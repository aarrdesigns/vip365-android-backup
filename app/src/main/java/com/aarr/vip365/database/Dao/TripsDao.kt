package com.aarr.vip365.database.Dao

import android.util.Log
import com.aarr.vip365.database.DBHelper
import com.aarr.vip365.database.Model.TripsModel
import com.aarr.vip365.database.Model.UsersModel
import com.j256.ormlite.table.TableUtils
import java.util.ArrayList

/**
 * Created by andresrodriguez on 9/11/17.
 */
class TripsDao {

    fun insert(dto: TripsModel): Int {
        val dao = DBHelper.helper.getRuntimeExceptionDao(TripsModel::class.java)
        return dao.create(dto)
    }

    fun update(dto: TripsModel): Int {
        val dao = DBHelper.helper.getRuntimeExceptionDao(TripsModel::class.java)
        return dao.update(dto)
    }

    fun findByPrimaryKey(id: Int): TripsModel? {
        var contactModel: TripsModel? = null
        try {
            var lst: List<TripsModel> = ArrayList<TripsModel>()
            val qB = TripsModel().getRepository().queryBuilder()
            qB.where().eq("id_payment", id)
            lst = qB.query()
            if (lst.size > 0)
                contactModel = lst[0]
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return contactModel
    }

    fun findByServerKey(id: Int): TripsModel? {
        var contactModel: TripsModel? = null
        try {
            var lst: List<TripsModel> = ArrayList<TripsModel>()
            val qB = TripsModel().getRepository().queryBuilder()
            qB.where().eq("id_payment_server", id)
            lst = qB.query()
            if (lst.size > 0)
                contactModel = lst[0]
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return contactModel
    }

    fun getCurrentUser(): TripsModel? {
        var contactModel: TripsModel? = null
        try {
            val qB = TripsModel().getRepository().queryBuilder()
            contactModel = qB.queryForFirst()
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return contactModel
    }

    fun clearUser(){
        TableUtils.clearTable(DBHelper.helper.connectionSource, TripsModel::class.java)
    }

    fun updateOrCreate(cat: TripsModel?):Int{
        if (cat!!.idMobile>0){
            TripsModel().getRepository().update(cat)
            return cat.idMobile
        }else{
            var i = TripsModel().getRepository().create(cat)
            Log.v("CreateCategory", "Id: " + i)
            return i
        }
    }
}