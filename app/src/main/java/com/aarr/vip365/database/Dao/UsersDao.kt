package com.aarr.vip365.database.Dao

import android.util.Log
import com.aarr.vip365.database.DBHelper
import com.aarr.vip365.database.Model.UsersModel
import com.j256.ormlite.table.TableUtils
import java.util.ArrayList

/**
 * Created by andresrodriguez on 9/11/17.
 */
class UsersDao {

    fun insert(dto: UsersModel): Int {
        val dao = DBHelper.helper.getRuntimeExceptionDao(UsersModel::class.java)
        return dao.create(dto)
    }

    fun update(dto: UsersModel): Int {
        val dao = DBHelper.helper.getRuntimeExceptionDao(UsersModel::class.java)
        return dao.update(dto)
    }

    fun findByPrimaryKey(id: Int): UsersModel? {
        var contactModel: UsersModel? = null
        try {
            var lst: List<UsersModel> = ArrayList<UsersModel>()
            val qB = UsersModel().getRepository().queryBuilder()
            qB.where().eq("id_payment", id)
            lst = qB.query()
            if (lst.size > 0)
                contactModel = lst[0]
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return contactModel
    }

    fun findByServerKey(id: Int): UsersModel? {
        var contactModel: UsersModel? = null
        try {
            var lst: List<UsersModel> = ArrayList<UsersModel>()
            val qB = UsersModel().getRepository().queryBuilder()
            qB.where().eq("id_payment_server", id)
            lst = qB.query()
            if (lst.size > 0)
                contactModel = lst[0]
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return contactModel
    }

    fun getCurrentUser(): UsersModel? {
        var contactModel: UsersModel? = null
        try {
            val qB = UsersModel().getRepository().queryBuilder()
            contactModel = qB.queryForFirst()
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return contactModel
    }

    fun clearUser(){
        TableUtils.clearTable(DBHelper.helper.connectionSource,UsersModel::class.java)
    }

    fun updateOrCreate(cat: UsersModel?):Int{
        if (cat!!.idMobile>0){
            UsersModel().getRepository().update(cat)
            return cat.idMobile
        }else{
            var i = UsersModel().getRepository().create(cat)
            Log.v("CreateCategory", "Id: " + i)
            return i
        }
    }
}