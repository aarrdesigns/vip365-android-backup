package com.aarr.vip365.database.Model

import com.aarr.vip365.database.DBHelper
import com.j256.ormlite.dao.RuntimeExceptionDao
import com.j256.ormlite.field.DatabaseField
import com.j256.ormlite.table.DatabaseTable
import java.io.Serializable
import java.util.*

/**
 * Created by andresrodriguez on 10/17/17.
 */
@DatabaseTable(tableName = "trips")
data class TripsModel(

        @DatabaseField(generatedId = true, columnName = "id_mobile")
        var idMobile:Int,

        @DatabaseField(columnName = "id_server")
        var idServer:Int,

        @DatabaseField(columnName = "user_client")
        var userClient:Int,

        @DatabaseField(columnName = "user_driver")
        var userDriver:Int,

        @DatabaseField(columnName = "request_date")
        var requestDate:Date,

        @DatabaseField(columnName = "trip_date")
        var tripDate:Date,

        @DatabaseField(columnName = "origin_location")
        var originLocation:String,

        @DatabaseField(columnName = "destiny_location")
        var destinyLocation:String,

        @DatabaseField(columnName = "origin_name")
        var originName:String,

        @DatabaseField(columnName = "destiny_name")
        var destinyName:String,

        @DatabaseField(columnName = "state")
        var state:Int,

        @DatabaseField(columnName = "path_taked")
        var pathTaked:String,

        @DatabaseField(columnName = "trip_cost")
        var tripCost:Double,

        @DatabaseField(columnName = "trip_type")
        var tripType:Int,

        @DatabaseField(columnName = "id_payment")
        var idPayment:Int,

        @DatabaseField(columnName = "is_paid")
        var isPaid:Boolean
): Serializable {
    constructor():this(
            -1,
            -1,
            -1,
            -1,
            Date(),
            Date(),
            "",
            "",
            "",
            "",
            -1,
            "",
            0.0,
            0,
            0,
            false
    )

    fun getRepository(): RuntimeExceptionDao<TripsModel, Int> {
        return DBHelper.DaoGet.get(TripsModel::class.java)
    }
}