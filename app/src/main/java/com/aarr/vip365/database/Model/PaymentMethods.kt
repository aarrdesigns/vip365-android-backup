package com.aarr.vip365.database.Model

import com.aarr.vip365.database.DBHelper
import com.j256.ormlite.dao.RuntimeExceptionDao
import com.j256.ormlite.field.DatabaseField
import com.j256.ormlite.table.DatabaseTable
import java.io.Serializable
import java.util.*

/**
 * Created by andresrodriguez on 9/11/17.
 */
@DatabaseTable(tableName = "payment_methods")
data class PaymentMethods(

        @DatabaseField(generatedId = true, columnName = "id_payment")
        var id:Int = -1,

        @DatabaseField(columnName = "id_payment_server")
        var idServer:Int = -1,

        @DatabaseField(columnName = "payment_type")
        var paymentType:Int = -1,

        @DatabaseField(columnName = "id_user")
        var idUser:Int = -1,

        @DatabaseField(columnName = "paypal_code")
        var paypalCode:String? = null,

        @DatabaseField(columnName = "card_type")
        var cardType:String? = null,

        @DatabaseField(columnName = "card_number")
        var cardNumber:String? = null,

        @DatabaseField(columnName = "card_security_number")
        var cardSecurityNumber:String? = null,

        @DatabaseField(columnName = "exp_date")
        var expDate:String? = null,

        @DatabaseField(columnName = "created_date")
        var createdDate:Date? = Date(),

        @DatabaseField(columnName = "is_default")
        var isDefault:Boolean? = false,

        @DatabaseField(columnName = "is_active")
        var isActive:Boolean? = false
        ):Serializable{

    constructor() : this(-1,
                            -1,
                            -1,
                            -1,
                            null,
                            null,
                            null,
                            null,
                            null,
                            Date(),
                            false,
                            false)

        fun getRepository(): RuntimeExceptionDao<PaymentMethods, Int> {
                return DBHelper.DaoGet.get(PaymentMethods::class.java)
        }

}