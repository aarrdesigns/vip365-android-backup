package com.aarr.vip365.database.Model

import com.aarr.vip365.database.DBHelper
import com.j256.ormlite.dao.RuntimeExceptionDao
import com.j256.ormlite.field.DatabaseField
import com.j256.ormlite.table.DatabaseTable
import java.io.Serializable

/**
 * Created by andresrodriguez on 10/9/17.
 */
@DatabaseTable(tableName = "users")
data class UsersModel(

                @DatabaseField(generatedId = true, columnName = "id_mobile")
                var idMobile:Int,

                @DatabaseField(columnName = "id_server")
                var idServer:Int,

                @DatabaseField(columnName = "name")
                var name:String,

                @DatabaseField(columnName = "last_name")
                var lastName:String,

                @DatabaseField(columnName = "email")
                var email:String,

                @DatabaseField(columnName = "phone_number")
                var phoneNumber:String,

                @DatabaseField(columnName = "password")
                var password:String,

                @DatabaseField(columnName = "profile_image")
                var profileImage:String?,

                @DatabaseField(columnName = "country_code")
                var countryCode:String,

                @DatabaseField(columnName = "type_user")
                var typeUser:Int
                      ):Serializable{
    constructor() : this(
                    -1,
                    -1,
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    -1)

    fun getRepository(): RuntimeExceptionDao<UsersModel, Int> {
        return DBHelper.DaoGet.get(UsersModel::class.java)
    }
}