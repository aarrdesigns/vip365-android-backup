package com.aarr.vip365.database.Dao

import android.util.Log
import com.aarr.vip365.database.DBHelper
import com.aarr.vip365.database.Model.PaymentMethods
import java.util.ArrayList

/**
 * Created by andresrodriguez on 9/11/17.
 */
class PaymentsMethodsDao {

    fun insert(dto: PaymentMethods): Int {
        val dao = DBHelper.Companion.helper.getRuntimeExceptionDao(PaymentMethods::class.java)
        return dao.create(dto)
    }

    fun update(dto: PaymentMethods): Int {
        val dao = DBHelper.Companion.helper.getRuntimeExceptionDao(PaymentMethods::class.java)
        return dao.update(dto)
    }

    fun findWhereCardNumberEq(card: String): PaymentMethods? {
        var lst: PaymentMethods? = null
        val qB = PaymentMethods().getRepository().queryBuilder()
        try {
            qB.where()
                    .eq("card_number", card)
            lst = qB.query()[0]
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return lst
    }

    fun findWherePayPalCode(code: String): PaymentMethods? {
        var lst: PaymentMethods? = null
        val qB = PaymentMethods().getRepository().queryBuilder()
        try {
            qB.where()
                    .eq("paypal_code", code)
            lst = qB.query()[0]
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return lst
    }

    fun findByType(type: Int): MutableList<PaymentMethods>? {
        var lst: MutableList<PaymentMethods>? = null
        val qB = PaymentMethods().getRepository().queryBuilder()
        try {
            qB.where()
                    .eq("payment_type", type)
            lst = qB.query()
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return lst
    }

    fun findAll(): MutableList<PaymentMethods> {
        var lst: MutableList<PaymentMethods> = ArrayList<PaymentMethods>()
        val qB = PaymentMethods().getRepository().queryBuilder()
        try {
            qB.orderBy("created_date",false)
            lst = qB.query()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return lst
    }

    fun findByPrimaryKey(id: Int): PaymentMethods? {
        var contactModel: PaymentMethods? = null
        try {
            var lst: List<PaymentMethods> = ArrayList<PaymentMethods>()
            val qB = PaymentMethods().getRepository().queryBuilder()
            qB.where().eq("id_payment", id)
            lst = qB.query()
            if (lst.size > 0)
                contactModel = lst[0]
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return contactModel
    }

    fun findByServerKey(id: Int): PaymentMethods? {
        var contactModel: PaymentMethods? = null
        try {
            var lst: List<PaymentMethods> = ArrayList<PaymentMethods>()
            val qB = PaymentMethods().getRepository().queryBuilder()
            qB.where().eq("id_payment_server", id)
            lst = qB.query()
            if (lst.size > 0)
                contactModel = lst[0]
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return contactModel
    }

    fun getCurrentPayment(): PaymentMethods? {
        var contactModel: PaymentMethods? = null
        try {
            var lst: List<PaymentMethods> = ArrayList<PaymentMethods>()
            val qB = PaymentMethods().getRepository().queryBuilder()
            qB.where().eq("is_default", true)
            lst = qB.query()
            if (lst.size > 0)
                contactModel = lst[0]
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return contactModel
    }

    fun cleanDefaultPayments(){
        var contactModel: PaymentMethods? = null
        try {
            var lst: List<PaymentMethods> = ArrayList<PaymentMethods>()
            val qB = PaymentMethods().getRepository().queryBuilder()
            qB.where().eq("is_default", true)
            lst = qB.query()
            if (lst.size > 0){
                for (i in lst){
                    i.isDefault = false
                    updateOrCreate(i)
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun updateOrCreate(cat:PaymentMethods?):Int{
        if (cat!!.id>0){
            PaymentMethods().getRepository().update(cat)
            return cat.id
        }else{
            var i = PaymentMethods().getRepository().create(cat)
            Log.v("CreateCategory","Id: "+i)
            return i
        }
    }
}