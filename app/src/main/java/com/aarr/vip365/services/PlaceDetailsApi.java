package com.aarr.vip365.services;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.aarr.vip365.services.PlaceDetails.PlaceDetails;
import com.aarr.vip365.services.PlaceDetails.PlaceDetailsByLocation;
import com.aarr.vip365.views.BuscarRutaActivity;
import com.aarr.vip365.views.MenuActivity;
import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by andresrodriguez on 1/19/17.
 */
public class PlaceDetailsApi {

    PlaceDetails placeDetails;
    PlaceDetailsByLocation placeDetailsByLocation;
    Context mContext;
    MenuActivity activity;
    BuscarRutaActivity activity2;
    final public static int ORIGEN = 1;
    final public static int DESTINO = 2;
    final public static int DIRECCIONES = 3;
    private int estado = 0;

    public PlaceDetailsApi(Context mContext, int estado){
        this.mContext = mContext;
        if (mContext instanceof MenuActivity){
            activity = (MenuActivity) mContext;
        }else if (mContext instanceof BuscarRutaActivity){
            activity2 = (BuscarRutaActivity) mContext;
        }
        this.estado = estado;
    }

    public void getPlaceDetails(final String placeId) {
        try{
            OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
            httpClient.addInterceptor(new Interceptor() {
                @Override
                public okhttp3.Response intercept(Chain chain) throws IOException {
                    Request original = chain.request();
                    // Customize the request
                    Request request = original.newBuilder()
                            .header("Accept", "application/json")
                            .header("Authorization", "auth-token")
                            .method(original.method(), original.body())
                            .build();
                    okhttp3.Response response = chain.proceed(request);
                    Log.v("Response",response.toString());
                    // Customize or return the response
                    return response;
                }
            });

            OkHttpClient client = httpClient.build();
            Retrofit retrofit = new Retrofit.Builder().baseUrl("https://maps.googleapis.com/maps/api/place/details/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client)
                    .build();
            MapsServices service = retrofit.create(MapsServices.class);

            Call<PlaceDetails> call = service.getPlaceDetails("json?" +
                    "place_id=" +
                    placeId +
                    "&sensor=" +
                    "true" +
                    "&key=" +
                    "AIzaSyB0Q57mi6LMSc5cF-A56iYTQHm5XnIVXCg");
            call.enqueue(new Callback<PlaceDetails>() {
                @Override
                public void onResponse(Call<PlaceDetails> call, Response<PlaceDetails> response) {
                    if (response.isSuccessful()) {
                        switch (estado){
                            case ORIGEN:
                                Log.v("PlaceDetailsApi","RESPONSE:SUCCESS");
                                placeDetails = response.body();
                                setPlaceDetails(placeDetails);
                                if (mContext instanceof MenuActivity){
                                    activity.setLocation(placeDetails.getResult(),ORIGEN);
                                }else if (mContext instanceof BuscarRutaActivity){
                                    activity2.setLocation(placeDetails.getResult(),ORIGEN);
                                }
                                break;
                            case DESTINO:
                                Log.v("PlaceDetailsApi","RESPONSE:SUCCESS");
                                placeDetails = response.body();
                                setPlaceDetails(placeDetails);
                                if (mContext instanceof MenuActivity){
                                    activity.setLocation(placeDetails.getResult(),ORIGEN);
                                }else if (mContext instanceof BuscarRutaActivity){
                                    activity2.setLocation(placeDetails.getResult(),DESTINO);
                                }
                                break;
                            case DIRECCIONES:
                                Log.v("PlaceDetailsDirecciones","RESPONSE:SUCCESS");
                                placeDetails = response.body();
                                Log.v("PlaceResult",placeDetails.getResult().toString());
//                            activity3.devolverDireccion(placeDetails.getResult(),mContext);
                                break;
                        }
                    } else {
                        Log.v("PlaceDetailsApi","RESPONSE:ERROR");
                        int statusCode = response.code();
                        ResponseBody errorBody = response.errorBody();
                        Log.v("PlaceDetailsApi",errorBody.toString());
                    }
                }

                @Override
                public void onFailure(Call<PlaceDetails> call, Throwable t) {
                    Log.e("PlaceDetailsApi","onFailure: ",t);
                }
            });
        }catch (Exception e){
            Toast.makeText(mContext,"Exception: "+e.toString(),Toast.LENGTH_LONG).show();
        }

    }

    public void getPlaceDetails(LatLng latLng) {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();
                // Customize the request
                Request request = original.newBuilder()
                        .header("Accept", "application/json")
                        .header("Authorization", "auth-token")
                        .method(original.method(), original.body())
                        .build();
                okhttp3.Response response = chain.proceed(request);
                Log.v("Response",response.toString());
                // Customize or return the response
                return response;
            }
        });

        OkHttpClient client = httpClient.build();
        Retrofit retrofit = new Retrofit.Builder().baseUrl("https://maps.googleapis.com/maps/api/place/nearbysearch/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();
        MapsServices service = retrofit.create(MapsServices.class);

        Call<PlaceDetailsByLocation> call = service.getPlaceDetailsByLocation("json?" +
                "location=" +
                String.valueOf(latLng.latitude) +","+
                String.valueOf(latLng.longitude) +
                "&sensor=" +
                "true" +
                "&key=" +
                "AIzaSyB0Q57mi6LMSc5cF-A56iYTQHm5XnIVXCg");
        call.enqueue(new Callback<PlaceDetailsByLocation>() {
            @Override
            public void onResponse(Call<PlaceDetailsByLocation> call, Response<PlaceDetailsByLocation> response) {
                if (response.isSuccessful()) {
                    if (activity!=null){
                        switch (estado){
                            case ORIGEN:
                                Log.v("PlaceDetailsApi","RESPONSE:SUCCESS");
                                placeDetailsByLocation = response.body();
//                            setPlaceDetails(placeDetails);
//                                activity.locationChoise(placeDetailsByLocation,ORIGEN);
                                break;
                            case DESTINO:
                                Log.v("PlaceDetailsApi","RESPONSE:SUCCESS");
                                placeDetailsByLocation = response.body();
//                            setPlaceDetails(placeDetails);
//                                activity.locationChoise(placeDetailsByLocation,DESTINO);
                                break;

                        }
                    }
                } else {
                    Log.v("PlaceDetailsApi","RESPONSE:ERROR");
                    int statusCode = response.code();
                    ResponseBody errorBody = response.errorBody();
                    Log.v("PlaceDetailsApi",errorBody.toString());
                }
            }

            @Override
            public void onFailure(Call<PlaceDetailsByLocation> call, Throwable t) {
                Log.e("PlaceDetailsApi","onFailure: ",t);
            }
        });
    }
    public PlaceDetails getPlaceDetails() {
        return placeDetails;
    }

    public void setPlaceDetails(PlaceDetails placeDetails) {
        this.placeDetails = placeDetails;
    }
}
