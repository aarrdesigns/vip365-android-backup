package com.aarr.vip365.services;

import android.content.Context;
import android.util.Log;

import com.aarr.vip365.services.PlaceDetails.PlaceDetails;
import com.aarr.vip365.services.PlaceDetails.PlaceDetailsByLocation;
import com.aarr.vip365.services.PlaceGeocode.PlaceGeocode;
import com.aarr.vip365.views.BuscarRutaActivity;
import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by andresrodriguez on 6/30/17.
 */

public class PlaceGeocodeApi {

    PlaceDetails placeDetails;
    PlaceDetailsByLocation placeDetailsByLocation;
    PlaceGeocode placeGeocode;
    Context mContext;
    BuscarRutaActivity activity;
    final public static int ORIGEN = 1;
    final public static int DESTINO = 2;
    final public static int DIRECCIONES = 3;
    private int estado = 0;

    public PlaceGeocodeApi(Context mContext, int estado){
        this.mContext = mContext;
        if (mContext instanceof BuscarRutaActivity){
            activity = (BuscarRutaActivity) mContext;
        }

        this.estado = estado;
    }

    public void getPlaceGeocode(final LatLng latLng) {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();
                // Customize the request
                Request request = original.newBuilder()
                        .header("Accept", "application/json")
                        .header("Authorization", "auth-token")
                        .method(original.method(), original.body())
                        .build();
                okhttp3.Response response = chain.proceed(request);
                Log.v("Response",response.toString());
                // Customize or return the response
                return response;
            }
        });

        OkHttpClient client = httpClient.build();
        Retrofit retrofit = new Retrofit.Builder().baseUrl("https://maps.googleapis.com/maps/api/geocode/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();
        MapsServices service = retrofit.create(MapsServices.class);


        Call<PlaceGeocode> call = service.getPlaceGeocode("json?" +
                "latlng=" +
                String.valueOf(latLng.latitude) +","+
                String.valueOf(latLng.longitude) +
                "&sensor=" +
                "true" +
                "&key=" +
                "AIzaSyB0Q57mi6LMSc5cF-A56iYTQHm5XnIVXCg");
        call.enqueue(new Callback<PlaceGeocode>() {
            @Override
            public void onResponse(Call<PlaceGeocode> call, Response<PlaceGeocode> response) {
                if (response.isSuccessful()) {
                    if (activity!=null) {
//                        activity.hideProgress();
                        placeGeocode = response.body();
                        if (estado == -1){
                            try{
                                activity.setMyLocationGeocode(placeGeocode.getResults().get(0).getFormattedAddress());
                            }catch(Exception e){
                                Log.e("MyLocation",e.toString());
                            }
                        }else{
                            try{
                                placeGeocode.getResults().get(0).getGeometry().getLocation().setLat(latLng.latitude);
                                placeGeocode.getResults().get(0).getGeometry().getLocation().setLng(latLng.longitude);
                                activity.setLocationDetails(placeGeocode.getResults().get(0), estado);
                            }catch(Exception e){
                                Log.w("PlaceDetailsException",e.toString());
                            }
                        }
                    }
                } else {
//                    if (activity3!=null) {
//                        activity3.hideProgress();
//                    }
                    Log.v("PlaceDetailsApi","RESPONSE:ERROR");
                    int statusCode = response.code();
                    ResponseBody errorBody = response.errorBody();
                    Log.v("PlaceDetailsApi",errorBody.toString());
                }
            }

            @Override
            public void onFailure(Call<PlaceGeocode> call, Throwable t) {
                Log.e("PlaceDetailsApi","onFailure: ",t);
//                if (activity3!=null) {
//                    activity3.hideProgress();
//                }
            }
        });
    }
}
