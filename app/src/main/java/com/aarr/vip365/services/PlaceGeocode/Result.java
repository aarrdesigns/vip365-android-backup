
package com.aarr.vip365.services.PlaceGeocode;

import java.io.Serializable;
import java.util.List;

public class Result implements Serializable {

    private List<AddressComponent> address_components = null;
    private String formatted_address;
    private Geometry geometry;
    private String place_id;
    private List<String> types = null;

    public List<AddressComponent> getAddressComponents() {
        return address_components;
    }

    public void setAddressComponents(List<AddressComponent> address_components) {
        this.address_components = address_components;
    }

    public String getFormattedAddress() {
        return formatted_address;
    }

    public void setFormattedAddress(String formatted_address) {
        this.formatted_address = formatted_address;
    }

    public String getPlaceId() {
        return place_id;
    }

    public void setPlaceId(String place_id) {
        this.place_id = place_id;
    }

    public Geometry getGeometry() {
        return geometry;
    }

    public void setGeometry(Geometry geometry) {
        this.geometry = geometry;
    }



    public List<String> getTypes() {
        return types;
    }

    public void setTypes(List<String> types) {
        this.types = types;
    }

}
