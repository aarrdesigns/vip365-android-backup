
package com.aarr.vip365.services.PlaceAutocomplete;

import java.io.Serializable;

public class Term implements Serializable
{

    private Integer offset;
    private String value;
    private final static long serialVersionUID = 4199464878541695364L;

    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
