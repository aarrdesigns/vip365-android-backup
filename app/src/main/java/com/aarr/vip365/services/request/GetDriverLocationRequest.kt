package com.aarr.vip365.services.request

import java.io.Serializable

/**
 * Created by andresrodriguez on 10/17/17.
 */
data class GetDriverLocationRequest(
        var user_driver:Int
): Serializable {
    constructor():this(
            -1
    )
}