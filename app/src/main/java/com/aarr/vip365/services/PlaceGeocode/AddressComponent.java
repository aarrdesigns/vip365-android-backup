
package com.aarr.vip365.services.PlaceGeocode;

import java.io.Serializable;
import java.util.List;

public class AddressComponent implements Serializable {

    private String long_name;
    private String short_name;
    private List<String> types = null;

    public String getLongName() {
        return long_name;
    }

    public void setLongName(String longName) {
        this.long_name = longName;
    }

    public String getShortName() {
        return short_name;
    }

    public void setShortName(String shortName) {
        this.short_name = shortName;
    }

    public List<String> getTypes() {
        return types;
    }

    public void setTypes(List<String> types) {
        this.types = types;
    }

}
