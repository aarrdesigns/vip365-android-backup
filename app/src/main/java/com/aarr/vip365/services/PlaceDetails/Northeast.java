
package com.aarr.vip365.services.PlaceDetails;

import java.io.Serializable;

public class Northeast implements Serializable
{

    private Float lat;
    private Float lng;
    private final static long serialVersionUID = 8416057806972872045L;

    public Float getLat() {
        return lat;
    }

    public void setLat(Float lat) {
        this.lat = lat;
    }

    public Float getLng() {
        return lng;
    }

    public void setLng(Float lng) {
        this.lng = lng;
    }

}
