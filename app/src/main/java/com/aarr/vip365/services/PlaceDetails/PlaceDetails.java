
package com.aarr.vip365.services.PlaceDetails;

import java.io.Serializable;
import java.util.List;

public class PlaceDetails implements Serializable
{

    private List<Object> htmlAttributions = null;
    private Result result;
    private String status;
    private final static long serialVersionUID = 4322931259782197354L;

    public List<Object> getHtmlAttributions() {
        return htmlAttributions;
    }

    public void setHtmlAttributions(List<Object> htmlAttributions) {
        this.htmlAttributions = htmlAttributions;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
