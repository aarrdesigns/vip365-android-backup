
package com.aarr.vip365.services.PlaceAutocomplete;

import java.io.Serializable;
import java.util.List;

public class StructuredFormatting implements Serializable
{

    private String main_text;
    private List<MainTextMatchedSubstring> mainTextMatchedSubstrings = null;
    private String secondary_text;
    private final static long serialVersionUID = -5926596328486000905L;

    public String getMainText() {
        return main_text;
    }

    public void setMainText(String mainText) {
        this.main_text = mainText;
    }

    public List<MainTextMatchedSubstring> getMainTextMatchedSubstrings() {
        return mainTextMatchedSubstrings;
    }

    public void setMainTextMatchedSubstrings(List<MainTextMatchedSubstring> mainTextMatchedSubstrings) {
        this.mainTextMatchedSubstrings = mainTextMatchedSubstrings;
    }

    public String getSecondaryText() {
        return secondary_text;
    }

    public void setSecondaryText(String secondaryText) {
        this.secondary_text = secondaryText;
    }

    @Override
    public String toString() {
        return "StructuredFormatting{" +
                "mainText='" + main_text + '\'' +
                ", mainTextMatchedSubstrings=" + mainTextMatchedSubstrings +
                ", secondaryText='" + secondary_text + '\'' +
                '}';
    }
}
