package com.aarr.vip365.services.request

import java.io.Serializable

/**
 * Created by andresrodriguez on 10/17/17.
 */
data class PaymentMethodsRequest(
        var id:Int,
        var user_id:Int,
        var card_type:String?,
        var card_number:String?,
        var card_security_number:String?,
        var card_holder_name:String?,
        var payment_type:Int,
        var paypal_code:String?,
        var exp_date:String?,
        var is_default:Int,
        var is_active:Int
): Serializable {
    constructor():this(
            -1,
            -1,
            "",
            "",
            "",
            "",
            -1,
            "",
            "",
            -1,
            -1
    )
}