package com.aarr.vip365.services.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


/**
 * Created by andresrodriguez on 10/17/17.
 */
data class LoginResponse(
        @SerializedName("Users")
        @Expose
        var user:User?,
        @SerializedName("isError")
        @Expose
        var isError:Boolean?,
        @SerializedName("msgError")
        @Expose
        var msgError:String?
):Serializable {

}