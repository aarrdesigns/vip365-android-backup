package com.aarr.vip365.services.request

import java.io.Serializable

/**
 * Created by andresrodriguez on 10/17/17.
 */
data class TripsRequest(
        var id:Int,
        var user_client:Int,
        var user_driver:Int,
        var request_date:String,
        var trip_date:String,
        var origin_location:String,
        var destiny_location:String,
        var origin_name:String,
        var destiny_name:String,
        var state:Int,
        var path_taked:String,
        var trip_cost:Double,
        var trip_type:Int,
        var id_payment:Int,
        var is_paid:Boolean
): Serializable {
    constructor():this(
            -1,
            -1,
            -1,
            "",
            "",
            "",
            "",
            "",
            "",
            -1,
            "",
            0.0,
            0,
            0,
            false
    )
}