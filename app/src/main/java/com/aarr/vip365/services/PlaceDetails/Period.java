
package com.aarr.vip365.services.PlaceDetails;

import java.io.Serializable;

public class Period implements Serializable
{

    private Close close;
    private Open open;
    private final static long serialVersionUID = -9103700589028103613L;

    public Close getClose() {
        return close;
    }

    public void setClose(Close close) {
        this.close = close;
    }

    public Open getOpen() {
        return open;
    }

    public void setOpen(Open open) {
        this.open = open;
    }

}
