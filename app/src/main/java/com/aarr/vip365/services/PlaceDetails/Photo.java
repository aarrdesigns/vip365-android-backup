
package com.aarr.vip365.services.PlaceDetails;

import java.io.Serializable;
import java.util.List;

public class Photo implements Serializable
{

    private Integer height;
    private List<String> htmlAttributions = null;
    private String photoReference;
    private Integer width;
    private final static long serialVersionUID = -2228839323741582321L;

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public List<String> getHtmlAttributions() {
        return htmlAttributions;
    }

    public void setHtmlAttributions(List<String> htmlAttributions) {
        this.htmlAttributions = htmlAttributions;
    }

    public String getPhotoReference() {
        return photoReference;
    }

    public void setPhotoReference(String photoReference) {
        this.photoReference = photoReference;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

}
