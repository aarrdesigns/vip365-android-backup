package com.aarr.vip365.services.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Created by andresrodriguez on 10/17/17.
 */
data class User(
        @SerializedName("id")
        @Expose
         var id:Int?,
        @SerializedName("type")
        @Expose
         var type:String?,
        @SerializedName("first_name")
        @Expose
         var firstName:String?,
        @SerializedName("last_name")
        @Expose
         var lastName:String?,
        @SerializedName("email")
        @Expose
         var email:String?,
        @SerializedName("user_type")
        @Expose
         var userType:String?,
        @SerializedName("status")
        @Expose
         var status:String?,
        @SerializedName("confirmation_code")
        @Expose
         var confirmationCode:Object?,
        @SerializedName("confirmed")
        @Expose
         var confirmed:String?,
        @SerializedName("phone_number")
        @Expose
         var phoneNumber:String?,
        @SerializedName("profile_image")
        @Expose
         var profileImage:String?,
        @SerializedName("country_code")
        @Expose
         var countryCode:String?,
        @SerializedName("created_at")
        @Expose
         var createdAt:String?,
        @SerializedName("updated_at")
        @Expose
         var updatedAt:String?,
        @SerializedName("deleted_at")
        @Expose
         var deletedAt:Object?
): Serializable {

}