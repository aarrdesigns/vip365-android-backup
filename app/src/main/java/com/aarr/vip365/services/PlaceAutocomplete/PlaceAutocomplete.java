
package com.aarr.vip365.services.PlaceAutocomplete;

import java.io.Serializable;
import java.util.List;

public class PlaceAutocomplete implements Serializable
{

    private List<Prediction> predictions = null;
    private String status;
    private final static long serialVersionUID = 2178194965990151489L;

    public List<Prediction> getPredictions() {
        return predictions;
    }

    public void setPredictions(List<Prediction> predictions) {
        this.predictions = predictions;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "PlaceAutocomplete{" +
                "predictions=" + predictions +
                ", status='" + status + '\'' +
                '}';
    }
}
