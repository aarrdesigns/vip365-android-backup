package com.aarr.vip365.services.request

import java.io.Serializable

/**
 * Created by andresrodriguez on 10/17/17.
 */
data class LoginRequest(
        var email:String,
        var password:String?,
        var user_type:Int
):Serializable{
    constructor():this(
            "",
            null,
            -1
    )
}