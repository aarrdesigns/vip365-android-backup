package com.aarr.vip365.services

import android.content.Context
import android.util.Log
import android.widget.Toast
import com.aarr.vip365.database.Dao.PaymentsMethodsDao
import com.aarr.vip365.database.Model.PaymentMethods
import com.aarr.vip365.database.Model.UsersModel
import com.aarr.vip365.services.request.LoginRequest
import com.aarr.vip365.services.request.PaymentMethodsRequest
import com.aarr.vip365.services.response.InsertResponse
import com.aarr.vip365.services.response.LoginResponse
import com.aarr.vip365.views.LoginActivity
import com.aarr.vip365.views.PagosActivity
import org.json.JSONException
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by andresrodriguez on 10/17/17.
 */
class RegisterPaymentService {
    private var context: Context? = null
    private var service: ApiRest? = null
    private var activity: PagosActivity? = null

    constructor(context: Context){
        this.context = context
        if (context is PagosActivity){
            activity = context as PagosActivity
        }
    }

    fun handlePaymentRegister(item:PaymentMethods){
//        val gson = GsonBuilder()
//                .setLenient()
//                .create()

        val retrofit = Retrofit.Builder()
                .baseUrl("http://vip365.000webhostapp.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        Log.v("Service", "Url:" + retrofit.baseUrl().toString())
        service = retrofit.create(ApiRest::class.java)

            var request = PaymentMethodsRequest(
                    item.idServer,
                    item.idUser,
                    item.cardType,
                    item.cardNumber,
                    item.cardSecurityNumber,
                    "Name",
                    item.paymentType,
                    item.paypalCode,
                    item.expDate,
                    item.isDefault!!.toInt(),
                    item.isActive!!.toInt()


            )

            try {
//                var response: Call<LoginResponse>? = service!!.login(request)
                var response: Call<InsertResponse>? = service!!.inserPaymentMethod(
//                        request.id,
                        request.user_id,
                        request.card_type,
                        request.card_number,
                        request.card_security_number,
                        request.card_holder_name,
                        request.payment_type,
                        request.exp_date,
                        request.paypal_code,
                        request.is_default,
                        request.is_active
                )
                Log.v("Service", "Request: "+request.toString())
                response!!.enqueue(object : Callback<InsertResponse> {

                    override fun onResponse(call: Call<InsertResponse>?, response: retrofit2.Response<InsertResponse>?) {
                        Log.v("Service", "Success")
                        Log.v("Service", "Response: " + response!!.raw().toString())
                        Log.v("Service", "Response: " + response!!.message())
                        for (i in response.headers().names()) {
                            Log.v("Service", "Header: " + i + ": " + response!!.headers().get(i))
//                            Log.v("Service","ResponseHeader: "+i)
                        }
//                        Log.v("Service","Data:"+response!!.body().toString())
                        var responseLogin = response!!.body()
                        if (responseLogin != null) {
                            Log.v("Service", "Data: " + responseLogin.toString())
                            if (responseLogin!!.id != null && responseLogin.id!!>0) {
                                Log.v("Service", "USER IS NOT NULL")
                                item.idServer = responseLogin.id!!
                                PaymentsMethodsDao().updateOrCreate(item)
//                                activity!!.goMenu(user)
                            } else {
                                Log.v("Service", "USER IS NULL")
//                                activity!!.dismissDialog()
                                if (responseLogin.msgError != null) {
                                    Toast.makeText(context, responseLogin.msgError, Toast.LENGTH_LONG).show()
                                } else {
                                    Toast.makeText(context, "Error al iniciar sesion", Toast.LENGTH_LONG).show()
                                }
                            }
                        }else{
//                            activity!!.dismissDialog()
                            Toast.makeText(context, "Error de servidor", Toast.LENGTH_LONG).show()
                        }
                    }
                    override fun onFailure(call: Call<InsertResponse>?, t: Throwable?) {
//                        activity!!.dismissDialog()
                        Log.v("Service", "Failed:" + t.toString())
                    }
                })

            } catch (e: JSONException) {
//                activity!!.dismissDialog()
                Log.v("Service", "Exception:" + e.toString())
            }
    }

    fun Boolean.toInt() = if (this) 1 else 0
}