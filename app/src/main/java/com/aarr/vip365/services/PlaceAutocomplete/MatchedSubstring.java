
package com.aarr.vip365.services.PlaceAutocomplete;

import java.io.Serializable;

public class MatchedSubstring implements Serializable
{

    private Integer length;
    private Integer offset;
    private final static long serialVersionUID = -7728381828272330077L;

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }

    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

}
