
package com.aarr.vip365.services.PlaceGeocode;


import java.io.Serializable;

public class Viewport implements Serializable {

    private Northeast_ northeast;
    private Southwest_ southwest;

    public Northeast_ getNortheast() {
        return northeast;
    }

    public void setNortheast(Northeast_ northeast) {
        this.northeast = northeast;
    }

    public Southwest_ getSouthwest() {
        return southwest;
    }

    public void setSouthwest(Southwest_ southwest) {
        this.southwest = southwest;
    }

}
