package com.aarr.vip365.services

import android.content.Context
import android.util.Log
import android.widget.Toast
import com.aarr.vip365.database.Dao.TripsDao
import com.aarr.vip365.database.Model.TripsModel
import com.aarr.vip365.database.Model.UsersModel
import com.aarr.vip365.services.request.LoginRequest
import com.aarr.vip365.services.request.TripsRequest
import com.aarr.vip365.services.response.InsertResponse
import com.aarr.vip365.services.response.LoginResponse
import com.aarr.vip365.util.DateManage
import com.aarr.vip365.views.LoginActivity
import com.aarr.vip365.views.MenuActivity
import org.json.JSONException
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by andresrodriguez on 10/17/17.
 */
class TripService {
    private var context: Context? = null
    private var service: ApiRest? = null
    private var activity: MenuActivity? = null

    constructor(context: Context){
        this.context = context
        if (context is LoginActivity){
            activity = context as MenuActivity
        }
    }

    fun registerTrip(trip:TripsModel){
//        val gson = GsonBuilder()
//                .setLenient()
//                .create()

        val retrofit = Retrofit.Builder()
                .baseUrl("http://vip365.000webhostapp.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        Log.v("Service", "Url:" + retrofit.baseUrl().toString())
        service = retrofit.create(ApiRest::class.java)

            var request = TripsRequest(
                    trip.idServer,
                    trip.userClient,
                    trip.userDriver,
                    DateManage.datetimeToWsString(trip.requestDate),
                    DateManage.datetimeToWsString(trip.tripDate),
                    trip.originLocation,
                    trip.destinyLocation,
                    trip.originName,
                    trip.destinyName,
                    trip.state,
                    trip.pathTaked,
                    trip.tripCost,
                    trip.tripType,
                    trip.idPayment,
                    trip.isPaid
            )

        Log.v("Service", "Data Request: "+request.toString())

            try {
                var response: Call<InsertResponse>? = service!!.insertTrip(
//                        request.id,
                        request.user_client,
                        request.user_driver,
                        request.request_date,
                        request.trip_date,
                        request.origin_location,
                        request.destiny_location,
                        request.origin_name,
                        request.destiny_name,
                        request.state,
                        request.path_taked,
                        request.trip_cost,
                        request.trip_type,
                        request.id_payment,
                        request.is_paid.toInt()
                )
//                var response: Call<LoginResponse>? = service!!.login(
//                        email,
//                        password,
//                        typeUser
//                )
                response!!.enqueue(object : Callback<InsertResponse> {

                    override fun onResponse(call: Call<InsertResponse>?, response: retrofit2.Response<InsertResponse>?) {
                        Log.v("Service", "Success")
                        Log.v("Service", "Response: " + response!!.raw().toString())
                        Log.v("Service", "Response: " + response!!.message())
                        for (i in response.headers().names()){
                            Log.v("Service", "Header: " + i + ": " + response!!.headers().get(i))
//                            Log.v("Service","ResponseHeader: "+i)
                        }
//                        Log.v("Service","Data:"+response!!.body().toString())
                        var responseTrip = response!!.body()
                        if (responseTrip!=null){
                            Log.v("Service", "Data: "+responseTrip.toString())
                            if (responseTrip!!.id!=null && responseTrip!!.id!!>0){
                                trip.idServer = responseTrip!!.id!!
                                TripsDao().updateOrCreate(trip)
                            }else{
                                if (responseTrip.msgError!=null){
                                    Toast.makeText(context, responseTrip.msgError, Toast.LENGTH_LONG).show()
                                }else{
                                    Toast.makeText(context, "Error al registrar el viaje", Toast.LENGTH_LONG).show()
                                }
                            }
//                            activity!!.goMenu(user)
                        }else{
//                            activity!!.dismissDialog()
                            Toast.makeText(context, "Error en servidor", Toast.LENGTH_LONG).show()

                        }

                    }
                    override fun onFailure(call: Call<InsertResponse>?, t: Throwable?) {
//                        activity!!.dismissDialog()
                        Log.v("Service", "Failed:" + t.toString())
                    }
                })

            } catch (e: JSONException) {
//                activity!!.dismissDialog()
                Log.v("Service", "Exception:" + e.toString())
            }
    }

    fun Boolean.toInt() = if (this) 1 else 0
}