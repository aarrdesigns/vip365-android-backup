
package com.aarr.vip365.services.PlaceDetails;

import java.io.Serializable;

public class Viewport implements Serializable
{

    private Northeast northeast;
    private Southwest southwest;
    private final static long serialVersionUID = 5781239608464826108L;

    public Northeast getNortheast() {
        return northeast;
    }

    public void setNortheast(Northeast northeast) {
        this.northeast = northeast;
    }

    public Southwest getSouthwest() {
        return southwest;
    }

    public void setSouthwest(Southwest southwest) {
        this.southwest = southwest;
    }

}
