
package com.aarr.vip365.services.PlaceAutocomplete;

import java.io.Serializable;
import java.util.List;

public class Prediction implements Serializable
{

    private String description;
    private String id;
    private List<MatchedSubstring> matched_substrings = null;
    private String place_id;
    private String reference;
    private StructuredFormatting structured_formatting = new StructuredFormatting();
    private List<Term> terms = null;
    private List<String> types = null;
    private final static long serialVersionUID = 8352041721860609990L;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<MatchedSubstring> getMatchedSubstrings() {
        return matched_substrings;
    }

    public void setMatchedSubstrings(List<MatchedSubstring> matched_substrings) {
        this.matched_substrings = matched_substrings;
    }

    public String getPlaceId() {
        return place_id;
    }

    public void setPlaceId(String place_id) {
        this.place_id = place_id;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public StructuredFormatting getStructuredFormatting() {
        return structured_formatting;
    }

    public void setStructuredFormatting(StructuredFormatting structured_formatting) {
        this.structured_formatting = structured_formatting;
    }

    public List<Term> getTerms() {
        return terms;
    }

    public void setTerms(List<Term> terms) {
        this.terms = terms;
    }

    public List<String> getTypes() {
        return types;
    }

    public void setTypes(List<String> types) {
        this.types = types;
    }

    @Override
    public String toString() {
        return "Prediction{" +
                "description='" + description + '\'' +
                ", id='" + id + '\'' +
                ", matched_substrings=" + matched_substrings +
                ", place_id='" + place_id + '\'' +
                ", reference='" + reference + '\'' +
                ", structured_formatting=" + structured_formatting +
                ", terms=" + terms +
                ", types=" + types +
                '}';
    }
}
