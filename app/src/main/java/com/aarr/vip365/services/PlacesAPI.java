package com.aarr.vip365.services;

import android.content.Context;
import android.location.Location;
import android.util.Log;
import android.widget.Toast;

import com.aarr.vip365.services.PlaceAutocomplete.PlaceAutocomplete;
import com.aarr.vip365.services.PlaceAutocomplete.Prediction;
import com.aarr.vip365.views.BuscarRutaActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by andresrodriguez on 1/18/17.
 */
public class PlacesAPI {

    private static final String TAG = PlacesAPI.class.getSimpleName();

    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place/autocomplete/";

    private static final String OUT_JSON = "json";

    private static final String API_KEY = "AIzaSyB0Q57mi6LMSc5cF-A56iYTQHm5XnIVXCg";

    private static final String RADIUS = "10";

    private BuscarRutaActivity activity;

    public List<Prediction> autocomplete (String input, Location currentLocation) {

        List<Prediction> resultList = null;

        HttpURLConnection conn = null;

        StringBuilder jsonResults = new StringBuilder();

        Log.v("LocationLatitude",String.valueOf(currentLocation.getLatitude()));
        Log.v("LocationLongitude",String.valueOf(currentLocation.getLongitude()));

        try {

            StringBuilder sb = new StringBuilder(PLACES_API_BASE + OUT_JSON);

            sb.append("?key=" + API_KEY);

            if (currentLocation!=null){
                sb.append("&location=" + String.valueOf(currentLocation.getLatitude())+","+String.valueOf(currentLocation.getLongitude()));
                sb.append("&radius=" + RADIUS);
            }

//            sb.append("&types=(cities)");

            sb.append("&input=" + URLEncoder.encode(input, "utf8"));

            Log.v("AutocompleteUrl",sb.toString());
            URL url = new URL(sb.toString());

            conn = (HttpURLConnection) url.openConnection();

            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            int read;

            char[] buff = new char[1024];

            while ((read = in.read(buff)) != -1) {

                jsonResults.append(buff, 0, read);

            }

        } catch (MalformedURLException e) {

            Log.e(TAG, "Error processing Places API URL", e);

            return resultList;

        } catch (IOException e) {

            Log.e(TAG, "Error connecting to Places API", e);

            return resultList;

        } finally {

            if (conn != null) {

                conn.disconnect();

            }

        }

        try {

            JSONObject jsonObj = new JSONObject(jsonResults.toString());

            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

            resultList = new ArrayList<Prediction>();

            for (int i = 0; i < predsJsonArray.length(); i++) {
                Prediction prediction = new Prediction();
                prediction.setId(String.valueOf(i));
                prediction.setDescription(predsJsonArray.getJSONObject(i).getString("description"));
                prediction.setPlaceId(predsJsonArray.getJSONObject(i).getString("place_id"));
                resultList.add(prediction);

            }

        } catch (JSONException e) {

            Log.e(TAG, "Cannot process JSON results", e);

        }

        return resultList;

    }

    public void autocompleteRetrofit(String input, Location currentLocation, final Context context,final int tipo){
        try{
            OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
            httpClient.addInterceptor(new Interceptor() {
                @Override
                public okhttp3.Response intercept(Chain chain) throws IOException {
                    Request original = chain.request();
                    // Customize the request
                    Request request = original.newBuilder()
                            .header("Accept", "application/json")
                            .header("Authorization", "auth-token")
                            .method(original.method(), original.body())
                            .build();
                    okhttp3.Response response = chain.proceed(request);
                    Log.v("Response",response.toString());
                    // Customize or return the response
                    return response;
                }
            });

            OkHttpClient client = httpClient.build();
            Retrofit retrofit = new Retrofit.Builder().baseUrl(PLACES_API_BASE)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client)
                    .build();
            MapsServices service = retrofit.create(MapsServices.class);

            String urlDetails = "json?";
            if (currentLocation!=null){
                urlDetails += "&location=" + String.valueOf(currentLocation.getLatitude())+","+String.valueOf(currentLocation.getLongitude());
                urlDetails += "&radius=" + RADIUS;
            }
            try {
                urlDetails += "&input=" + URLEncoder.encode(input, "utf8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                Log.v("UTF_Exception",e.toString());
            }
            urlDetails += "&key=" +
                    "AIzaSyB0Q57mi6LMSc5cF-A56iYTQHm5XnIVXCg";

            Log.v("FullUrl",PLACES_API_BASE+urlDetails);
            Call<PlaceAutocomplete> call = service.getPlacesAutocomplete(urlDetails);
            call.enqueue(new Callback<PlaceAutocomplete>() {
                @Override
                public void onResponse(Call<PlaceAutocomplete> call, Response<PlaceAutocomplete> response) {
                    if (response.isSuccessful()) {
                        Log.v("PlaceDetailsApi","RESPONSE:SUCCESS");
                        Log.v("PlaceDetailsApi","RESPONSE:"+response.body().toString());
                        final PlaceAutocomplete predicciones = response.body();
                        if (context instanceof BuscarRutaActivity){
                            activity = (BuscarRutaActivity) context;
                            if (predicciones!=null)
                                activity.retrieveAutocomplete(predicciones.getPredictions(),tipo);
                            else
                                Log.v("AutoComplete", "predicciones is null");
                        }

                    } else {
                        Log.v("PlaceDetailsApi","RESPONSE:ERROR");
                        int statusCode = response.code();
                        ResponseBody errorBody = response.errorBody();
                        Log.v("PlaceDetailsApi",errorBody.toString());
                    }
                }

                @Override
                public void onFailure(Call<PlaceAutocomplete> call, Throwable t) {
                    Log.e("PlaceDetailsApi","onFailure: ",t);
                }
            });
        }catch (Exception e){
            Toast.makeText(context,"Exception: "+e.toString(),Toast.LENGTH_LONG).show();
            Log.w("Exception",e.toString());
        }

    }

}