package com.aarr.vip365.services

import com.aarr.vip365.services.request.LoginRequest
import com.aarr.vip365.services.request.RegistroRequest
import com.aarr.vip365.services.response.GetDriverLocationResponse
import com.aarr.vip365.services.response.InsertResponse
import com.aarr.vip365.services.response.LoginResponse
import retrofit2.Call
import retrofit2.http.*

/**
 * Created by andresrodriguez on 10/17/17.
 */
interface ApiRest {

//    @Headers("Content-Type: application/json")
    @POST("api/loginClient")
    fun login(@Body body:LoginRequest): Call<LoginResponse>

//    @Headers("Content-Type: application/json")
//    @POST("api/loginClient")
//    fun login(@Query("email") email:String,
//              @Query("password") password:String?,
//              @Query("user_type") typeUser:Int
//              ): Call<LoginResponse>

    @FormUrlEncoded
    @POST("api/loginClient")
    fun login(@Field("email") email:String,
              @Field("password") password:String?,
              @Field("user_type") typeUser:Int
              ): Call<LoginResponse>

    @Headers("Content-Type: application/json")
    @POST("api/registerClient")
    fun register(@Body body:RegistroRequest): Call<Int>

    @FormUrlEncoded
    @POST("api/registerClient")
    fun register(
            //@Field("id") id:Int,
                 @Field("first_name") name:String,
                 @Field("last_name") lastName:String,
                 @Field("email") email:String,
                 @Field("phone_number") phoneNumber:String,
                 @Field("password") password:String,
                 @Field("profile_image") profileImage:String,
                 @Field("user_type") typeUser:Int,
                 @Field("country_code") countryCode:String
                 ): Call<InsertResponse>

    @FormUrlEncoded
    @POST("api/registerClient")
    fun updateClient(
            @Field("id") id:Int,
            @Field("first_name") name:String,
            @Field("last_name") lastName:String,
            //@Field("email") email:String,
            @Field("phone_number") phoneNumber:String,
            @Field("password") password:String,
            @Field("profile_image") profileImage:String,
            @Field("user_type") typeUser:Int,
            @Field("country_code") countryCode:String
    ): Call<InsertResponse>

    @FormUrlEncoded
    @POST("api/payments-methods")
    fun inserPaymentMethod(
//            @Field("id") id:Int,
                 @Field("user_id") idUser:Int,
                 @Field("card_type") cardType:String?,
                 @Field("card_number") cardNumber:String?,
                 @Field("card_security_number") cardSecurityNumber:String?,
                 @Field("card_holder_name") cardHolderName:String?,
                 @Field("payment_type") paymentType:Int,
                 @Field("exp_date") expDate:String?,
                 @Field("paypal_code") paypalCode:String?,
                 @Field("is_default") isDefault:Int,
                 @Field("is_active") isActive:Int
    ): Call<InsertResponse>

    @FormUrlEncoded
    @POST("api/create-trips")
    fun insertTrip(
//                @Field("id") id:Int,
                 @Field("user_client_id") userClient:Int,
                 @Field("user_drive_id") userDriver:Int,
                 @Field("request_date") requestDate:String,
                 @Field("trip_date") tripDate:String,
                 @Field("origin_location") originLocation:String,
                 @Field("destiny_location") destinyLocation:String,
                 @Field("origin_name") originName:String,
                 @Field("destiny_name") destinyName:String,
                 @Field("state") state:Int,
                 @Field("path_taked") pathTaked:String,
                 @Field("trip_cost") tripCost:Double,
                 @Field("trip_type") tripType:Int,
                 @Field("payment_id") idPayment:Int,
                 @Field("isPaid") isPaid:Int
    ): Call<InsertResponse>

    @FormUrlEncoded
    @POST("api/get-drivers")
    fun getDriverLocation(@Field("user_driver") id:Int): Call<GetDriverLocationResponse>
}