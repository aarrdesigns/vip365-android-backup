package com.aarr.vip365.services

import android.content.Context
import android.util.Log
import android.widget.Toast
import com.aarr.vip365.database.Model.UsersModel
import com.aarr.vip365.services.request.LoginRequest
import com.aarr.vip365.services.response.LoginResponse
import com.aarr.vip365.views.LoginActivity
import org.json.JSONException
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import com.google.gson.GsonBuilder
import com.google.gson.Gson

/**
 * Created by andresrodriguez on 10/17/17.
 */
class LoginService{
    private var context: Context? = null
    private var service: ApiRest? = null
    private var activity: LoginActivity? = null

    constructor(context: Context){
        this.context = context
        if (context is LoginActivity){
            activity = context as LoginActivity
        }
    }

    fun handleLogin(email:String,password:String,typeUser:Int){
//        val gson = GsonBuilder()
//                .setLenient()
//                .create()

        val retrofit = Retrofit.Builder()
                .baseUrl("http://vip365.000webhostapp.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        Log.v("Service","Url:"+retrofit.baseUrl().toString())
        service = retrofit.create(ApiRest::class.java)

            var request = LoginRequest(
                    email,
                    password,
                    typeUser
            )

            try {
//                var response: Call<LoginResponse>? = service!!.login(request)
                var response: Call<LoginResponse>? = service!!.login(
                        email,
                        password,
                        typeUser
                )
                response!!.enqueue(object : Callback<LoginResponse> {

                    override fun onResponse(call: Call<LoginResponse>?, response: retrofit2.Response<LoginResponse>?) {
                        Log.v("Service", "Success")
                        Log.v("Service", "Response: " + response!!.raw().toString())
                        Log.v("Service", "Response: " + response!!.message())
                        for (i in response.headers().names()) {
                            Log.v("Service", "Header: " + i + ": " + response!!.headers().get(i))
//                            Log.v("Service","ResponseHeader: "+i)
                        }
//                        Log.v("Service","Data:"+response!!.body().toString())
                        var responseLogin = response!!.body()
                        if (responseLogin != null) {
                            Log.v("Service","Data: "+responseLogin.toString())
                            if (responseLogin!!.user != null) {
                                Log.v("Service","USER IS NOT NULL")
                                var user = UsersModel(
                                        -1,
                                        responseLogin!!.user!!.id!!,
                                        responseLogin!!.user!!.firstName!!,
                                        responseLogin!!.user!!.lastName!!,
                                        responseLogin!!.user!!.email!!,
                                        responseLogin!!.user!!.phoneNumber!!,
                                        "",
                                        responseLogin!!.user!!.profileImage,
                                        responseLogin!!.user!!.countryCode!!,
                                        responseLogin!!.user!!.userType!!.toInt()
                                )
                                activity!!.goMenu(user)
                            } else {
                                Log.v("Service","USER IS NULL")
                                activity!!.dismissDialog()
                                if (responseLogin.msgError != null) {
                                    Toast.makeText(context, responseLogin.msgError, Toast.LENGTH_LONG).show()
                                } else {
                                    Toast.makeText(context, "Error al iniciar sesion", Toast.LENGTH_LONG).show()
                                }
                            }
                        }else{
                            activity!!.dismissDialog()
                            Toast.makeText(context, "Error de servidor", Toast.LENGTH_LONG).show()
                        }
                    }
                    override fun onFailure(call: Call<LoginResponse>?, t: Throwable?) {
                        activity!!.dismissDialog()
                        Log.v("Service","Failed:"+t.toString())
                    }
                })

            } catch (e: JSONException) {
                activity!!.dismissDialog()
                Log.v("Service","Exception:"+e.toString())
            }
    }
}