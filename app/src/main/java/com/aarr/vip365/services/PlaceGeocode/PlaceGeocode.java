
package com.aarr.vip365.services.PlaceGeocode;

import java.util.List;

public class PlaceGeocode {

    private List<Result> results = null;
    private String status;

    public List<Result> getResults() {
        return results;
    }

    public void setResults(List<Result> results) {
        this.results = results;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
