
package com.aarr.vip365.services.PlaceDetails;

import java.io.Serializable;

public class Geometry implements Serializable
{

    private Location location;
    private Viewport viewport;
    private final static long serialVersionUID = 4094325027898466022L;

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Viewport getViewport() {
        return viewport;
    }

    public void setViewport(Viewport viewport) {
        this.viewport = viewport;
    }

}
