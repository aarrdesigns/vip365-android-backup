
package com.aarr.vip365.services.PlaceGeocode;


import java.io.Serializable;

public class Northeast_ implements Serializable {

    private Double lat;
    private Double lng;

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

}
