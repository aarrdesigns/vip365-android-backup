
package com.aarr.vip365.services.PlaceDetails;

import java.io.Serializable;

public class Close implements Serializable
{

    private Integer day;
    private String time;
    private final static long serialVersionUID = 3004527778833411000L;

    public Integer getDay() {
        return day;
    }

    public void setDay(Integer day) {
        this.day = day;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

}
