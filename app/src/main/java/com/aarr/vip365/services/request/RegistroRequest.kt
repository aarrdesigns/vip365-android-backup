package com.aarr.vip365.services.request

import java.io.Serializable

/**
 * Created by andresrodriguez on 10/17/17.
 */
data class RegistroRequest(
        var first_name:String,
        var last_name:String,
        var email:String,
        var phone_number:String,
        var password:String,
        var profile_image:String,
        var type_user:Int,
        var country_code:String
):Serializable{
    constructor():this(
            "",
            "",
            "",
            "",
            "",
            "",
            -1,
            ""
    )
}