
package com.aarr.vip365.services.PlaceDetails;

import java.io.Serializable;

public class Aspect implements Serializable
{

    private Integer rating;
    private String type;
    private final static long serialVersionUID = 5885454288935953907L;

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
