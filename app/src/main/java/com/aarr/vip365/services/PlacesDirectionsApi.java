package com.aarr.vip365.services;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.aarr.vip365.services.PlaceDirections.PlacesDirections;
import com.aarr.vip365.views.BuscarRutaActivity;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by andresrodriguez on 1/20/17.
 */
public class PlacesDirectionsApi {
    Context mContext;
    PlacesDirections placeDirections;
    BuscarRutaActivity activity;
    String originLatLng = "";
    String destinLatLng = "";

    public PlacesDirectionsApi(Context mContext){
        this.mContext = mContext;
        if (mContext instanceof BuscarRutaActivity)
            activity = (BuscarRutaActivity) mContext;
    }

    public void getPlaceDirections(String origen, String destino) {
        try{

            OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
            httpClient.addInterceptor(new Interceptor() {
                @Override
                public okhttp3.Response intercept(Chain chain) throws IOException {
                    Request original = chain.request();
                    // Customize the request
                    Request request = original.newBuilder()
                            .header("Accept", "application/json")
                            .header("Authorization", "auth-token")
                            .method(original.method(), original.body())
                            .build();
                    okhttp3.Response response = chain.proceed(request);
                    Log.v("Response",response.toString());
                    // Customize or return the response
                    return response;
                }
            });

            OkHttpClient client = httpClient.build();
            Retrofit retrofit = new Retrofit.Builder().baseUrl("https://maps.googleapis.com/maps/api/directions/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client)
                    .build();
            MapsServices service = retrofit.create(MapsServices.class);

            Call<PlacesDirections> call = service.getPlaceDirections("json?" +
                    "origin=" +
                    origen +
                    "&destination=" +
                    destino +
                    "&mode="+
                    "driving"+
                    "&sensor=" +
                    "false"+
                    "&key=" +
                    "AIzaSyB0Q57mi6LMSc5cF-A56iYTQHm5XnIVXCg");
            call.enqueue(new Callback<PlacesDirections>() {
                @Override
                public void onResponse(Call<PlacesDirections> call, Response<PlacesDirections> response) {
                    if (response.isSuccessful()) {
                        Log.v("PlaceDirectionsApi","RESPONSE:SUCCESS");
                        placeDirections = response.body();
                        if (placeDirections!=null){
                            if (activity!=null){
                                activity.trazarRuta(placeDirections);
                            }
                        }
                    } else {
                        Log.v("PlaceDirectionsApi","RESPONSE:ERROR");
                        int statusCode = response.code();
                        ResponseBody errorBody = response.errorBody();
                        Log.v("PlaceDirectionsApi",errorBody.toString());
                    }
                }

                @Override
                public void onFailure(Call<PlacesDirections> call, Throwable t) {
                    Log.e("PlaceDirectionsApi","onFailure: ",t);
                }
            });
        }catch (Exception e){
            Toast.makeText(mContext,"Exception: "+e.toString(),Toast.LENGTH_LONG).show();
            Log.w("Exception",e.toString());
        }

    }


}
