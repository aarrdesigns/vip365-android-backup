
package com.aarr.vip365.services.PlaceDetails;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PlaceDetailsByLocation implements Serializable
{

    private List<Object> htmlAttributions = null;
    private List<Result> result;
    private String status;
    private final static long serialVersionUID = 4322931259782197354L;

    public List<Object> getHtmlAttributions() {
        return htmlAttributions;
    }

    public void setHtmlAttributions(List<Object> htmlAttributions) {
        this.htmlAttributions = htmlAttributions;
    }

    public List<Result> getResult() {
        if (result==null)
            result = new ArrayList<Result>();
        return result;
    }

    public void setResult(List<Result> result) {
        this.result = result;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "PlaceDetailsByLocation{" +
                "htmlAttributions=" + htmlAttributions +
                ", result=" + result +
                ", status='" + status + '\'' +
                '}';
    }
}
