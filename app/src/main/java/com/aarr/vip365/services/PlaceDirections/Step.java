
package com.aarr.vip365.services.PlaceDirections;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class Step implements Serializable
{

    private Distance_ distance;
    private Duration_ duration;
    private EndLocation_ end_location;
    private String html_instructions;
    private Polyline polyline;
    private StartLocation_ start_location;
    private String travel_mode;
    private String maneuver;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    private final static long serialVersionUID = 2537719110559636230L;

    public Distance_ getDistance() {
        return distance;
    }

    public void setDistance(Distance_ distance) {
        this.distance = distance;
    }

    public Duration_ getDuration() {
        return duration;
    }

    public void setDuration(Duration_ duration) {
        this.duration = duration;
    }

    public EndLocation_ getEndLocation() {
        return end_location;
    }

    public void setEndLocation(EndLocation_ end_location) {
        this.end_location = end_location;
    }

    public String getHtmlInstructions() {
        return html_instructions;
    }

    public void setHtmlInstructions(String html_instructions) {
        this.html_instructions = html_instructions;
    }

    public Polyline getPolyline() {
        return polyline;
    }

    public void setPolyline(Polyline polyline) {
        this.polyline = polyline;
    }

    public StartLocation_ getStartLocation() {
        return start_location;
    }

    public void setStartLocation(StartLocation_ start_location) {
        this.start_location = start_location;
    }

    public String getTravelMode() {
        return travel_mode;
    }

    public void setTravelMode(String travel_mode) {
        this.travel_mode = travel_mode;
    }

    public String getManeuver() {
        return maneuver;
    }

    public void setManeuver(String maneuver) {
        this.maneuver = maneuver;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
