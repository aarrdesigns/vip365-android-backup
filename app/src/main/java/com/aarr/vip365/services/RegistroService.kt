package com.aarr.vip365.services

import android.content.Context
import android.util.Log
import android.widget.Toast
import com.aarr.vip365.database.Model.UsersModel
import com.aarr.vip365.services.request.RegistroRequest
import com.aarr.vip365.services.response.InsertResponse
import com.aarr.vip365.views.RegistroActivity
import org.json.JSONException
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by andresrodriguez on 10/17/17.
 */
class RegistroService{
    private var context: Context? = null
    private var service: ApiRest? = null
    private var activity: RegistroActivity? = null

    constructor(context: Context){
        this.context = context
        if (context is RegistroActivity){
            activity = context as RegistroActivity
        }
    }

    fun handleRegister(user:UsersModel?){
        if (user!=null){

            val retrofit = Retrofit.Builder()
//                    .client(httpClient.build())
                    .baseUrl("http://vip365.000webhostapp.com/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
            Log.v("Service","Url:"+retrofit.baseUrl().toString())
            service = retrofit.create(ApiRest::class.java)

            var request = RegistroRequest(
                    user.name,
                    user.lastName,
                    user.email,
                    user.phoneNumber,
                    user.password,
                    user.profileImage!!,
                    user.typeUser,
                    user.countryCode
            )

            try {
//                var response: Call<Int>? = service!!.register(request)
                var response: Call<InsertResponse>? = service!!.register(
//                        user.idServer,
                        user.name,
                        user.lastName,
                        user.email,
                        user.phoneNumber,
                        user.password,
                        user.profileImage!!,
                        user.typeUser,
                        user.countryCode
                )
                response!!.enqueue(object : Callback<InsertResponse> {
                    override fun onResponse(call: Call<InsertResponse>?, response: retrofit2.Response<InsertResponse>?) {
                        Log.v("Service","Success")
                        Log.v("Service","Response: "+response!!.raw().toString())
                        Log.v("Service","Response: "+response!!.message())
                        for (i in response.headers().names()){
                            Log.v("Service","ResponseAllow: "+response!!.headers().get(i))
//                            Log.v("Service","ResponseHeader: "+i)
                        }
//                        Log.v("Service","Data:"+response!!.body().toString())
                        var serverResponse = response!!.body()
                        if (serverResponse!=null){
                            Log.v("Service","Data: "+serverResponse.toString())
                            if (serverResponse.id!=null && serverResponse.id!!>0){
                                user.idServer = serverResponse.id!!
                                activity!!.handleSuccessResponse(user)
                            }else{
                                activity!!.dismissDialog()
                                if (serverResponse.msgError!=null){
                                    Toast.makeText(context, serverResponse.msgError, Toast.LENGTH_LONG).show()
                                }else{
                                    Toast.makeText(context, "Error al registrar el viaje", Toast.LENGTH_LONG).show()
                                }
                            }
                        }else{
                            activity!!.dismissDialog()
                            Toast.makeText(context, "Error al servidor", Toast.LENGTH_LONG).show()
                        }


                    }
                    override fun onFailure(call: Call<InsertResponse>?, t: Throwable?) {
                        activity!!.dismissDialog()
                        Log.v("Service","Failed:"+t.toString())
                    }
                })

            } catch (e: JSONException) {
                activity!!.dismissDialog()
                Log.v("Service","Exception:"+e.toString())
            }
        }
    }
}