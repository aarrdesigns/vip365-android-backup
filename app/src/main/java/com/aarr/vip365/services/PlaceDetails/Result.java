
package com.aarr.vip365.services.PlaceDetails;

import java.io.Serializable;
import java.util.List;

public class Result implements Serializable
{

    private List<AddressComponent> addressComponents = null;
    private String adrAddress;
    private String formatted_address;
    private String formattedPhoneNumber;
    private Geometry geometry;
    private String icon;
    private String id;
    private String internationalPhoneNumber;
    private String name;
    private OpeningHours openingHours;
    private List<Photo> photos = null;
    private String placeId;
    private Integer priceLevel;
    private Float rating;
    private String reference;
    private List<Review> reviews = null;
    private String scope;
    private List<String> types = null;
    private String url;
    private Integer utcOffset;
    private String vicinity;
    private String website;
    private final static long serialVersionUID = 1640637202687758733L;

    public List<AddressComponent> getAddressComponents() {
        return addressComponents;
    }

    public void setAddressComponents(List<AddressComponent> addressComponents) {
        this.addressComponents = addressComponents;
    }

    public String getAdrAddress() {
        return adrAddress;
    }

    public void setAdrAddress(String adrAddress) {
        this.adrAddress = adrAddress;
    }

    public String getFormattedAddress() {
        return formatted_address;
    }

    public void setFormattedAddress(String formatted_address) {
        this.formatted_address = formatted_address;
    }

    public String getFormattedPhoneNumber() {
        return formattedPhoneNumber;
    }

    public void setFormattedPhoneNumber(String formattedPhoneNumber) {
        this.formattedPhoneNumber = formattedPhoneNumber;
    }

    public Geometry getGeometry() {
        return geometry;
    }

    public void setGeometry(Geometry geometry) {
        this.geometry = geometry;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getInternationalPhoneNumber() {
        return internationalPhoneNumber;
    }

    public void setInternationalPhoneNumber(String internationalPhoneNumber) {
        this.internationalPhoneNumber = internationalPhoneNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public OpeningHours getOpeningHours() {
        return openingHours;
    }

    public void setOpeningHours(OpeningHours openingHours) {
        this.openingHours = openingHours;
    }

    public List<Photo> getPhotos() {
        return photos;
    }

    public void setPhotos(List<Photo> photos) {
        this.photos = photos;
    }

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    public Integer getPriceLevel() {
        return priceLevel;
    }

    public void setPriceLevel(Integer priceLevel) {
        this.priceLevel = priceLevel;
    }

    public Float getRating() {
        return rating;
    }

    public void setRating(Float rating) {
        this.rating = rating;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public List<Review> getReviews() {
        return reviews;
    }

    public void setReviews(List<Review> reviews) {
        this.reviews = reviews;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public List<String> getTypes() {
        return types;
    }

    public void setTypes(List<String> types) {
        this.types = types;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getUtcOffset() {
        return utcOffset;
    }

    public void setUtcOffset(Integer utcOffset) {
        this.utcOffset = utcOffset;
    }

    public String getVicinity() {
        return vicinity;
    }

    public void setVicinity(String vicinity) {
        this.vicinity = vicinity;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    @Override
    public String toString() {
        return "Result{" +
                "website='" + website + '\'' +
                ", vicinity='" + vicinity + '\'' +
                ", utcOffset=" + utcOffset +
                ", url='" + url + '\'' +
                ", types=" + types +
                ", scope='" + scope + '\'' +
                ", reviews=" + reviews +
                ", reference='" + reference + '\'' +
                ", rating=" + rating +
                ", priceLevel=" + priceLevel +
                ", placeId='" + placeId + '\'' +
                ", photos=" + photos +
                ", openingHours=" + openingHours +
                ", name='" + name + '\'' +
                ", internationalPhoneNumber='" + internationalPhoneNumber + '\'' +
                ", id='" + id + '\'' +
                ", icon='" + icon + '\'' +
                ", geometry=" + geometry +
                ", formattedPhoneNumber='" + formattedPhoneNumber + '\'' +
                ", formatted_address='" + formatted_address + '\'' +
                ", adrAddress='" + adrAddress + '\'' +
                ", addressComponents=" + addressComponents +
                '}';
    }
}
