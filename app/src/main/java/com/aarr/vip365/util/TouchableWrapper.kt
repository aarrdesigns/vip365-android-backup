package com.aarr.vip365.util

import android.content.Context
import com.aarr.vip365.views.MainActivity
import android.view.MotionEvent
import android.widget.FrameLayout
import com.aarr.vip365.views.BuscarRutaActivity


/**
 * Created by andresrodriguez on 10/31/17.
 */
class TouchableWrapper(context: Context) : FrameLayout(context) {

    override fun dispatchTouchEvent(event: MotionEvent): Boolean {
        var activity: BuscarRutaActivity? = null
        if (context is BuscarRutaActivity){
            activity = context as BuscarRutaActivity
        }
        when (event.action) {

            MotionEvent.ACTION_DOWN -> {
                //MainActivity.mMapIsTouched = true
                if (activity!=null){
                    activity.movingMap()
                }
            }

            MotionEvent.ACTION_UP -> {
                //MainActivity.mMapIsTouched = false
                if (activity!=null){
                    activity.getLocationFromMap()
                }
            }
        }
        return super.dispatchTouchEvent(event)
    }
}