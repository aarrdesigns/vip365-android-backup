package com.aarr.vip365.util

/**
 * Created by andresrodriguez on 10/31/17.
 */
import com.google.android.gms.maps.SupportMapFragment
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

class MySupportMapFragment : SupportMapFragment() {
    var mOriginalContentView: View? = null
    var mTouchView: TouchableWrapper? = null

    override fun onCreateView(inflater: LayoutInflater?, parent: ViewGroup?, savedInstanceState: Bundle?): View? {
        mOriginalContentView = super.onCreateView(inflater, parent, savedInstanceState)
        mTouchView = TouchableWrapper(activity)
        mTouchView?.addView(mOriginalContentView)
        return mTouchView
    }

    override fun getView(): View? {
        return mOriginalContentView
    }
}