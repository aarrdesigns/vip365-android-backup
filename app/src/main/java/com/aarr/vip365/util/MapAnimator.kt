package com.aarr.vip365.util

import android.animation.*
import com.google.android.gms.internal.add
import com.google.android.gms.maps.model.LatLng
import android.animation.Animator.AnimatorListener
import android.view.animation.AccelerateDecelerateInterpolator
import android.view.animation.AccelerateInterpolator
import android.view.animation.DecelerateInterpolator
import com.google.android.gms.maps.model.PolylineOptions
import android.graphics.Color
import com.google.android.gms.maps.GoogleMap
import android.graphics.Color.parseColor
import com.aarr.vip365.R
import com.google.android.gms.maps.model.Polyline


/**
 * Created by andresrodriguez on 10/19/17.
 */
class MapAnimator{
    private var mapAnimator: MapAnimator? = null

    private var backgroundPolyline: Polyline? = null

    private var foregroundPolyline: Polyline? = null

    private var optionsForeground: PolylineOptions? = null

    private var firstRunAnimSet: AnimatorSet? = null

    private var secondLoopRunAnimSet: AnimatorSet? = null

    val GREY = R.color.colorPrimary
    val DARK = R.color.colorPrimaryDark

    constructor()

    fun getInstance(): MapAnimator {
        if (mapAnimator == null) mapAnimator = MapAnimator()
        return mapAnimator!!
    }


    fun animateRoute(googleMap: GoogleMap, bangaloreRoute: List<LatLng>) {
        var pointsArray = arrayOfNulls<LatLng>(bangaloreRoute.size)
        for (i in 0..bangaloreRoute.size-1){
            pointsArray[i] = bangaloreRoute[i]
        }
        if (firstRunAnimSet == null) {
            firstRunAnimSet = AnimatorSet()
        } else {
            firstRunAnimSet!!.removeAllListeners()
            firstRunAnimSet!!.end()
            firstRunAnimSet!!.cancel()

            firstRunAnimSet = AnimatorSet()
        }
        if (secondLoopRunAnimSet == null) {
            secondLoopRunAnimSet = AnimatorSet()
        } else {
            secondLoopRunAnimSet!!.removeAllListeners()
            secondLoopRunAnimSet!!.end()
            secondLoopRunAnimSet!!.cancel()

            secondLoopRunAnimSet = AnimatorSet()
        }
        //Reset the polylines
        if (foregroundPolyline != null) foregroundPolyline!!.remove()
        if (backgroundPolyline != null) backgroundPolyline!!.remove()


        val optionsBackground = PolylineOptions().add(bangaloreRoute[0]).color(GREY).width(5f)
        backgroundPolyline = googleMap.addPolyline(optionsBackground)

        optionsForeground = PolylineOptions().add(bangaloreRoute[0]).color(DARK).width(5f)
        foregroundPolyline = googleMap.addPolyline(optionsForeground)

        val percentageCompletion = ValueAnimator.ofInt(0, 100)
        percentageCompletion.duration = 2000
        percentageCompletion.interpolator = DecelerateInterpolator()
        percentageCompletion.addUpdateListener { animation ->
            val foregroundPoints = backgroundPolyline!!.getPoints()

            val percentageValue = animation.animatedValue as Int
            val pointcount = foregroundPoints.size
            val countTobeRemoved = (pointcount * (percentageValue / 100.0f)).toInt()
            val subListTobeRemoved = foregroundPoints.subList(0, countTobeRemoved)
            subListTobeRemoved.clear()

            foregroundPolyline!!.setPoints(foregroundPoints)
        }
        percentageCompletion.addListener(object : Animator.AnimatorListener {
            override fun onAnimationStart(animation: Animator) {

            }

            override fun onAnimationEnd(animation: Animator) {
                foregroundPolyline!!.setColor(GREY)
                foregroundPolyline!!.setPoints(backgroundPolyline!!.getPoints())
            }

            override fun onAnimationCancel(animation: Animator) {

            }

            override fun onAnimationRepeat(animation: Animator) {

            }
        })


        val colorAnimation = ValueAnimator.ofObject(ArgbEvaluator(), GREY, DARK)
        colorAnimation.interpolator = AccelerateInterpolator()
        colorAnimation.duration = 1200 // milliseconds

        colorAnimation.addUpdateListener { animator -> foregroundPolyline!!.setColor(animator.animatedValue as Int) }

        val foregroundRouteAnimator = ObjectAnimator.ofObject(this, "routeIncreaseForward", RouteEvaluator(), pointsArray)
        foregroundRouteAnimator.interpolator = AccelerateDecelerateInterpolator()
        foregroundRouteAnimator.addListener(object : Animator.AnimatorListener {
            override fun onAnimationStart(animation: Animator) {

            }

            override fun onAnimationEnd(animation: Animator) {
                backgroundPolyline!!.setPoints(foregroundPolyline!!.getPoints())
            }

            override fun onAnimationCancel(animation: Animator) {

            }

            override fun onAnimationRepeat(animation: Animator) {

            }
        })
        foregroundRouteAnimator.duration = 1600
        //        foregroundRouteAnimator.start();

        firstRunAnimSet!!.playSequentially(foregroundRouteAnimator,
                percentageCompletion)
        firstRunAnimSet!!.addListener(object : Animator.AnimatorListener {
            override fun onAnimationStart(animation: Animator) {

            }

            override fun onAnimationEnd(animation: Animator) {
                secondLoopRunAnimSet!!.start()
            }

            override fun onAnimationCancel(animation: Animator) {

            }

            override fun onAnimationRepeat(animation: Animator) {

            }
        })

        secondLoopRunAnimSet!!.playSequentially(colorAnimation,
                percentageCompletion)
        secondLoopRunAnimSet!!.startDelay = 200

        secondLoopRunAnimSet!!.addListener(object : Animator.AnimatorListener {
            override fun onAnimationStart(animation: Animator) {

            }

            override fun onAnimationEnd(animation: Animator) {
                secondLoopRunAnimSet!!.start()
            }

            override fun onAnimationCancel(animation: Animator) {

            }

            override fun onAnimationRepeat(animation: Animator) {

            }
        })

        firstRunAnimSet!!.start()
    }

    /**
     * This will be invoked by the ObjectAnimator multiple times. Mostly every 16ms.
     */
    fun setRouteIncreaseForward(endLatLng: LatLng) {
        val foregroundPoints = foregroundPolyline!!.getPoints()
        foregroundPoints.add(endLatLng)
        foregroundPolyline!!.setPoints(foregroundPoints)
    }
}