package com.aarr.vip365.util

import com.google.android.gms.maps.model.LatLng
import android.animation.TypeEvaluator



/**
 * Created by andresrodriguez on 10/19/17.
 */
class RouteEvaluator : TypeEvaluator<LatLng> {
    override fun evaluate(t: Float, startPoint: LatLng, endPoint: LatLng): LatLng {
        val lat = startPoint.latitude + t * (endPoint.latitude - startPoint.latitude)
        val lng = startPoint.longitude + t * (endPoint.longitude - startPoint.longitude)
        return LatLng(lat, lng)
    }
}