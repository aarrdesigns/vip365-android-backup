package com.aarr.vip365.util;

import android.util.Log;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;


public class DateManage {
	
	/**
     * Método para convertir Fecha de JAVA a
     * DateTime para SQL Formato yyyy-MM-dd HH:mm:ss
     */
    public static String dateToSQLDateTime(Date dateTime){
        if (dateTime==null)
            return null;
                    
        java.text.SimpleDateFormat sdf =  new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        return sdf.format(dateTime);
    }

    /**
     * Método para convertir Fecha de JAVA a
     * Time para SQL Formato HH:mm:ss
     */
    public static String timeToSQLDateTime(Date time){
        if (time==null)
            return null;

        java.text.SimpleDateFormat sdf =  new java.text.SimpleDateFormat("HH:mm:ss");

        return sdf.format(time);
    }

    /**
     * Método para expresar en String la fecha indicada
     * La cadena retornada tiene el formato para sentencias SQL: yyyy-MM-dd
     * @return String con la fecha con el formato SQL yyyy-MM-dd
     */
    public static String formatDateToSQL(Date dateTime) {
        if (dateTime==null)
            return null;
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return dateFormat.format(dateTime);
    }

    /**
     * Método para expresar en String la fecha y hora actual
     * La cadena retornada tiene el formato: ddMMyy
     * @return String con la fecha y hora con el formato yyyyMMddHHmmssSSS
     */
    public static String getDateHourStringTiny(){
        DateFormat dateFormat = new SimpleDateFormat("ddMMyy");
        Date date = new Date();
        return dateFormat.format(date);
    }

    /**
     * Método para convertir Fecha de JAVA a
     * DateTime para SQL Formato dd-MM-yyyy HH:mm
     */
    public static String dateTimeToHumanReadableString(Date dateTime){
        if (dateTime==null)
            return null;

        java.text.SimpleDateFormat sdf =  new java.text.SimpleDateFormat("dd-MM-yyyy HH:mm");

        return sdf.format(dateTime);
    }

    /**
     * Método para convertir Fecha de JAVA a
     * DateTime para SQL Formato dd-MM-yyyy HH:mm
     */
    public static String dateToHumanReadableString(Date dateTime){
        if (dateTime==null)
            return null;

        java.text.SimpleDateFormat sdf =  new java.text.SimpleDateFormat("dd-MM-yyyy");

        return sdf.format(dateTime);
    }

    /**
     * Recibe un objeto Date y regresa un String con el formato dd 'de' MMMM 'de' yyyy
     * por ejemplo: 12 de Junio de 2009
     * @param date Date a transformar
     * @return Cadena con formato dd 'de' MMMM 'de' yyyy
     */
    public static String dateToStringEspanol(Date date) {
        String fecha = "";
        SimpleDateFormat formateador = new SimpleDateFormat(
                "EEEE ', ' dd 'de' MMMM 'de' yyyy", new Locale("ES"));
        fecha = formateador.format(date);
        return fecha;
    }



    /**
     * Convierte de datos aislados (dia, mes, año) a una cadena con formato "dd/MM/yyyy"
     * @param day Dia del mes
     * @param month Mes del año
     * @param year Año a 4 posiciones
     * @return String
     */
    public static String partDateToString(int day, int month, int year){

    	String fecha = "";
    	fecha = rellenarEspaciosIzquierda(""+day, "0",2) + "/" + rellenarEspaciosIzquierda(""+month, "0",2) + "/" + rellenarEspaciosIzquierda(""+year, "0",4);

    	System.out.println("dia: " + day + " mes: " + month + " año: " +year + " --- Fecha compuesta: " + fecha);

    	return fecha;
    }

    public static String datetimeToWsString(Date fecha){
        if(fecha == null)
            return null;

        String parsedDate = "";
        try{
            SimpleDateFormat dateFormat = new SimpleDateFormat("MMM d, yyyy h:mm:ss", Locale.US);
            parsedDate = dateFormat.format(fecha);
        }catch (Exception e) {
            e.printStackTrace();
        }
        return parsedDate;
    }

    public static String dateToWsString(Date fecha){
        if(fecha == null)
            return null;

        String parsedDate = "";
        try{
            SimpleDateFormat dateFormat = new SimpleDateFormat("MMM d, yyyy", Locale.US);
            parsedDate = dateFormat.format(fecha);
        }catch (Exception e) {
            e.printStackTrace();
        }
        return parsedDate;
    }

    private static String rellenarEspaciosIzquierda(String dato, String completarCon, int totalEspacios){
    	String resultado = dato;

    	//int charCount=0;

    	for (int charCount = (dato.length()+1); charCount <= totalEspacios; charCount++){
    		resultado =  completarCon + resultado;
    	}

    	return resultado;
    }

    /**
     * Convierte una cadena en formato Date SQL (yyyy-MM-dd) a Date de JAVA
     * @param dateTimeStr
     * @return
     * @throws ParseException
     */
    public static Date parseDateSQL(String dateTimeStr) {
    	if (dateTimeStr==null)
    		return null;
    	if (dateTimeStr.trim().equals(""))
    		return null;

    	Date parsedDate = null;
    	try{
    		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    		parsedDate = dateFormat.parse(dateTimeStr);
    	}catch(Exception ex){
    		ex.printStackTrace();
    	}

    	return parsedDate;
    }

    /**
     * Convierte la fecha String que se envía de consola a DateTime
     * @param dateTimeStr la fecha en formato WS
     * @return Date con la fecha obtenida
     */
    public static Date parseDatetimeWs(String dateTimeStr){
        if(dateTimeStr == null)
            return null;

        if(dateTimeStr.trim().equals(""))
            return null;

        Date parsedDate = null;
        try{
            SimpleDateFormat dateFormat = new SimpleDateFormat("MMM d, yyyy h:mm:ss", Locale.US);
            parsedDate = dateFormat.parse(dateTimeStr);
        }catch (Exception e){
            e.printStackTrace();
        }
        return parsedDate;
    }

	/**
	 * Convierte la fecha String que se envía de consola a Date
	 * @param dateTimeStr la fecha en formato WS
	 * @return Date con la fecha obtenida
	 */
	public static Date parseDateWs(String dateTimeStr){
		if(dateTimeStr == null)
			return null;

		if(dateTimeStr.trim().equals(""))
			return null;

		Date parsedDate = null;
		try{
			SimpleDateFormat dateFormat = new SimpleDateFormat("MMM d, yyyy");
			parsedDate = dateFormat.parse(dateTimeStr);
		}catch (Exception e){
			e.printStackTrace();
		}
		return parsedDate;
	}

    /**
     * Convierte una cadena en formato DateTime SQL (yyyy-MM-dd HH:mm:ss) a Date de JAVA
     * @param dateTimeStr
     * @return
     * @throws ParseException
     */
    public static Date parseDateTimeSQL(String dateTimeStr) {
    	if (dateTimeStr==null)
    		return null;
    	if (dateTimeStr.trim().equals(""))
    		return null;

    	Date parsedDate = null;
    	try{
    		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    		parsedDate = dateFormat.parse(dateTimeStr);
    	}catch(Exception ex){
    		ex.printStackTrace();
    	}

    	return parsedDate;
    }

    /**
     * Convierte una cadena en formato Time SQL (HH:mm:ss) a Date de JAVA
     * @param timeStr
     */
    public static Date parseTimeSQL(String timeStr) {
    	if (timeStr==null)
    		return null;
    	if (timeStr.trim().equals(""))
    		return null;

    	Date parsedDate = null;
    	try{
            SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
            parsedDate = dateFormat.parse(timeStr);
    	}catch(Exception ex){
    		ex.printStackTrace();
    	}

    	return parsedDate;
    }

    /**
    * Convierte una cadena en formato DateTime SQL (dd-MM-yyyy) a Date de JAVA
    * @param dateTimeStr
    * @return
    * @throws ParseException
    */
   public static Date parseDate(String dateTimeStr) throws ParseException {
   	if (dateTimeStr==null)
   		return null;
   	if (dateTimeStr.trim().equals(""))
   		return null;

   	SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

   	return dateFormat.parse(dateTimeStr);
   }

   /**
   * Convierte una cadena en formato DateTime SQL (dd/MM/yyyy) a Date de JAVA
   * @param dateTimeStr
   * @return
   * @throws ParseException
   */
  public static Date parseDateMex(String dateTimeStr){
  	if (dateTimeStr==null)
  		return null;
  	if (dateTimeStr.trim().equals(""))
  		return null;

  	Date response = null;
  	try{
  		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
  		response = dateFormat.parse(dateTimeStr);
  	}catch(Exception ex){
  		ex.printStackTrace();
  	}

  	return response;
  }

  /**
   * Convierte una cadena en formato DateTime SQL (dd/MM/yyyy hh:mm:ss) a Date de JAVA
   * @param dateTimeStr
   * @return
   * @throws ParseException
   */
  public static Date parseDateTimeMex(String dateTimeStr){
  	if (dateTimeStr==null)
  		return null;
  	if (dateTimeStr.trim().equals(""))
  		return null;

  	Date response = null;
  	try{
  		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
  		response = dateFormat.parse(dateTimeStr);
  	}catch(Exception ex){
  		ex.printStackTrace();
  	}

  	return response;
  }

  public static String formatDateTimeToStringMex(Date dateTime){
	  if (dateTime==null)
          return null;

      java.text.SimpleDateFormat sdf =  new java.text.SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

      return sdf.format(dateTime);
  }

  public static String formatDateToStringMex(Date dateTime){
	  if (dateTime==null)
          return null;

      java.text.SimpleDateFormat sdf =  new java.text.SimpleDateFormat("dd/MM/yyyy");

      return sdf.format(dateTime);
  }

  public static String formatTimeToStringMex(Date dateTime){
	  if (dateTime==null)
          return null;

      java.text.SimpleDateFormat sdf =  new java.text.SimpleDateFormat("HH:mm:ss");

      return sdf.format(dateTime);
  }

  /**
   * Convierte una cadena en formato especificado a Date de JAVA
   * @param dateTimeStr Cadena de tiempo
   * @param format Formato de la cadena
   * @return Date
   * @throws ParseException
   */
  public static Date parseDateGeneric(String dateTimeStr, String format){
  	if (dateTimeStr==null)
  		return null;
  	if (dateTimeStr.trim().equals(""))
  		return null;

  	Date response = null;
  	try{
  		Locale spanish = new Locale("es", "ES");
  		DateFormat dateFormat = new SimpleDateFormat(format, spanish);
  		response = dateFormat.parse(dateTimeStr);
  	}catch(Exception ex){
  		ex.printStackTrace();
  	}

  	if (response == null){
  		try{
  			//Si no se pudo convertir con Locale ES, intentamos con EN
  			DateFormat dateFormatEn = new SimpleDateFormat(format, Locale.ENGLISH);
  			response = dateFormatEn.parse(dateTimeStr);
	  	}catch(Exception ex){
	  		ex.printStackTrace();
	  	}
  	}

  	return response;
  }

  /**
   * Convierte un Date a una cadena con formato especificado
   * @param dateTime Date
   * @param format formato a aplicar para crear Cadena
   * @return String
   */
  public static String formatDateGeneric(Date dateTime, String format){
	  if (dateTime==null)
          return null;

	  Locale spanish = new Locale("es", "ES");
      DateFormat sdf = new SimpleDateFormat(format, spanish);

      String formatDate = null;

      try {
    	  formatDate = sdf.format(dateTime);
      }catch(Exception ex){
    	  ex.printStackTrace();
      }

      if (formatDate==null){
    	  try {
	    	  //Si no se convirtio, intentamos con otro Locale (en ingles)
	    	  DateFormat sdfEn = new SimpleDateFormat(format, Locale.ENGLISH);
	    	  formatDate = sdfEn.format(dateTime);
	      }catch(Exception ex){
	    	  ex.printStackTrace();
	      }
      }

      return formatDate;
  }

  /**
   * Método para expresar en String la fecha y hora actual incluyendo hasta milisegundos
   * La cadena retornada tiene el formato: yyyyMMddHHmmssSSS
   * @return String con la fecha y hora con el formato yyyyMMddHHmmssSSS
   */
  public static String getDateHourString(){
      DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmssSSS");
      Date date = new Date();
      return dateFormat.format(date);
  }

  /**
   * Determina si una hora y fecha de un objeto Date estan dentro
   * del día de Hoy
   * @param date Fecha a comparar
   * @return true en caso de ser del día de hoy, false en caso contrario
   */
  public static boolean isDateInToday(Date date, int horaInicio){
	  boolean inToday = false;
	  Date fechaDesde = new Date();
	  Date fechaHasta = new Date();

	  try{
			Calendar calDesde = Calendar.getInstance();
			int horaActual = calDesde.get(Calendar.HOUR_OF_DAY);
			//calDesde.setTime(fechaCorte);
			calDesde.set(Calendar.HOUR_OF_DAY, 0);
			calDesde.set(Calendar.MINUTE, 0);
			if(horaInicio > 0){
				calDesde.set(Calendar.HOUR_OF_DAY, horaInicio);
			}


			if(horaActual < horaInicio)
				calDesde.add(Calendar.DAY_OF_YEAR, -1);

			fechaDesde.setTime(calDesde.getTimeInMillis());
	  }catch(Exception ex){
		  ex.printStackTrace();
	  }

	  try{
			Calendar calHasta = Calendar.getInstance();
			int horaActual = calHasta.get(Calendar.HOUR_OF_DAY);
			//calHasta.setTime(fechaCorte);
			calHasta.set(Calendar.HOUR_OF_DAY, 23);
			calHasta.set(Calendar.MINUTE, 59);

			if(horaInicio > 0){
				calHasta.set(Calendar.HOUR_OF_DAY, horaInicio - 1);
				if(horaActual > horaInicio)
					calHasta.add(Calendar.DAY_OF_YEAR, +1);
			}
			fechaHasta.setTime(calHasta.getTimeInMillis());
	  }catch(Exception ex){
		  ex.printStackTrace();
	  }

	  Log.d("DateManage", "Fecha Desde: " +fechaDesde  + " , Fecha Hasta: " + fechaHasta);

	  if ( date.after(fechaDesde) && date.before(fechaHasta) ){
		  inToday = true;
	  }

	  return inToday;
  }


  /**
   * Determina si una hora y fecha de un objeto Date estan dentro
   * del día de Hoy
   * @param dateBase Fecha a comparar
   * @return true en caso de ser del día de hoy, false en caso contrario
   */
  public static boolean isDateInDate(Date dateBase, Date dateCompare){
	  boolean inDate = false;
	  Date fechaDesde = dateBase;
	  Date fechaHasta = dateBase;

	  try{
			Calendar calDesde = Calendar.getInstance();

			//calDesde.setTime(fechaCorte);
			calDesde.set(Calendar.HOUR_OF_DAY, 0);
			calDesde.set(Calendar.MINUTE, 1);
			fechaDesde.setTime(calDesde.getTimeInMillis());
	  }catch(Exception ex){
		  ex.printStackTrace();
	  }

	  try{
			Calendar calHasta = Calendar.getInstance();
			//calHasta.setTime(fechaCorte);
			calHasta.set(Calendar.HOUR_OF_DAY, 23);
			calHasta.set(Calendar.MINUTE, 59);
			fechaHasta.setTime(calHasta.getTimeInMillis());
	  }catch(Exception ex){
		  ex.printStackTrace();
	  }

	  Log.d("DateManage", "Fecha Desde: " +fechaDesde  + " , Fecha Hasta: " + fechaHasta);

	  if ( dateCompare.after(fechaDesde) && dateCompare.before(fechaHasta) ){
		  inDate = true;
	  }

	  return inDate;
  }

  /**
   * Determina si una hora/minuto de un objeto Date estan dentro
   * de otro minuto
   * @param dateBase Fecha a comparar
   * @return true en caso de ser del día de hoy, false en caso contrario
   */
  public static boolean isTimeMinuteInTime(Date dateBase, Date dateCompare){
	  boolean inTime = false;
	  Date fechaDesde = new Date();
	  Date fechaHasta = new Date();

	  try{
			Calendar calDesde = Calendar.getInstance();
			calDesde.setTime(dateBase);
			calDesde.set(Calendar.SECOND, 0);
			calDesde.add(Calendar.SECOND, -1);//recorremos adicional un segundo atras, por que la rutina AFTER es como un > y no un >=
			fechaDesde.setTime(calDesde.getTimeInMillis());
	  }catch(Exception ex){
		  ex.printStackTrace();
	  }

	  try{
			Calendar calHasta = Calendar.getInstance();
			calHasta.setTime(dateBase);
			calHasta.set(Calendar.SECOND, 59);
			calHasta.add(Calendar.SECOND, 1);//recorremos adicional un segundo adelante, por que la rutina BEFORE es como un < y no un <=
			fechaHasta.setTime(calHasta.getTimeInMillis());
	  }catch(Exception ex){
		  ex.printStackTrace();
	  }

	  //Log.d("DateManage", "Fecha Desde: " +fechaDesde  + " , Fecha Hasta: " + fechaHasta + ", Comparar: " + dateCompare);

	  if ( dateCompare.after(fechaDesde) && dateCompare.before(fechaHasta) ){
		  inTime = true;
	  }

	  return inTime;
  }

  public static Date asignarDiaMesAnio(Date timeOnly, int day, int month, int year){
	  if (timeOnly==null)
		  return null;

	  Date result = null;
	  Calendar calResult = Calendar.getInstance();
	  calResult.setTime(timeOnly);
	  calResult.set(year, month, day);
	  result = calResult.getTime();

	  return result;
  }

  public static boolean minutoEsMultiplo(Date tiempoBaseInicio, Date tiempoBaseFinal, int minutosMultiplo, Date tiempoCompara){
	  boolean esMultiplo = false;

	  try{
		  Calendar calAux = Calendar.getInstance();

		  //Primero validamos que tiempoBaseFinal sea posterior a tiempoBaseInicio
		  if (!tiempoBaseFinal.after(tiempoBaseInicio))
			  return esMultiplo;//no es un rango valido (el final no puede ser antes del inicio)

		  tiempoBaseInicio = DateManage.asignarDiaMesAnio(tiempoBaseInicio, 1, 1, 2000);
		  tiempoBaseFinal = DateManage.asignarDiaMesAnio(tiempoBaseFinal, 1, 1, 2000);
		  tiempoCompara = DateManage.asignarDiaMesAnio(tiempoCompara, 1, 1, 2000);

		  //Primero calcularemos todos los instantes posibles entre tiempoInicio y tiempoFinal
		  // con intervalos basados en "minutosMultiplo"
		  List<Long> instantesEnRango = new ArrayList<Long>();
		  Date tiempo = (Date)tiempoBaseInicio.clone(); //Iniciamos en la BaseInicio
		  while ( (tiempo.after(tiempoBaseInicio) || isTimeMinuteInTime(tiempoBaseInicio, tiempo))
				  && (tiempo.before(tiempoBaseFinal) || isTimeMinuteInTime(tiempoBaseFinal, tiempo)) ){
			  //si el tiempo calculado es (Mayor o Igual) a BaseInicio y (Menor o Igual) a BaseFinal
			  //lo agregamos a lista de instantes
			  instantesEnRango.add(tiempo.getTime());

			  //Agregamos N minutos a tiempo calculado
			  calAux.setTime(tiempo);
			  calAux.add(Calendar.MINUTE, minutosMultiplo);
			  tiempo = calAux.getTime();
		  }

		  //Ahora recorremos toda la lista, comparando cada item, contra fechaCompara (es la que intentamos buscar)
		  // y ver si existe una coincidencia
		  for (Long instante : instantesEnRango){
			  if (isTimeMinuteInTime( new Date(instante.longValue()) , tiempoCompara)){
				  esMultiplo = true;
				  break; //si encontramos al menos una coincidencia ya no requerimos recorrer lo que resta
			  }
		  }
	  }catch(Exception ex){
		  Log.d("DateManage", "Error al calcular multiplos de tiempo." + ex.toString());
	  }

	  return esMultiplo;
  }

  public static long diasDiferenciaEntreFechas(Date fechaInicio, Date fechaFin){
	  long startTime = fechaInicio.getTime();
	  long endTime = fechaFin.getTime();

	  long diffTime = endTime - startTime;
	  long diffDays = diffTime / (1000 * 60 * 60 * 24);

	  return diffDays;
  }

  /*
   * Retorna explícitamente la hora de un Date, en formato:
   * h:mm:ss a
   *
   * usando exclusivamente en mayusculas los identificadores AM y PM.
   */
  public static String formatHourQPay(Date date){
	  String str = formatDateGeneric(date, "h:mm:ss a");

	  if (str!=null){
		  str = str.toUpperCase(Locale.ENGLISH);// am --> AM , pm --> PM, a.m. --> A.M., p.m. --> P.M.
		  str = str.replace("A.M.", "AM"); // A.M. --> AM
		  str = str.replace("P.M.", "PM");// P.M. --> PM
	  }

	  return str;
  }

	public static String dateToHumanReadableSlashString(Date dateTime){
		if (dateTime==null)
			return null;

		java.text.SimpleDateFormat sdf =  new java.text.SimpleDateFormat("dd/MM/yyyy");

		return sdf.format(dateTime);
	}

	public static String dateToSQLDate(Date dateTime){
		if (dateTime==null)
			return null;

		java.text.SimpleDateFormat sdf =  new java.text.SimpleDateFormat("yyyy-MM-dd");

		return sdf.format(dateTime);
	}
  
}
