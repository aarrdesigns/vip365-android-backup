package com.aarr.vip365.util

import java.io.Serializable

/**
 * Created by andresrodriguez on 10/12/17.
 */
data class CountryData(
        var name: String,
        var extention: String,
        var code: String,
        var imgFlag: Int
):Serializable{
    constructor():this("","","",-1)
}