package com.aarr.vip365.mapas;

import android.content.Context;
import android.location.Location;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;


import com.aarr.vip365.services.PlaceAutocomplete.Prediction;
import com.aarr.vip365.services.PlacesAPI;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by andresrodriguez on 1/18/17.
 */
public class PlacesAutoCompleteAdapter extends ArrayAdapter<String> implements Filterable {

    List<Prediction> resultList;

    Context mContext;
    int mResource;
//    DireccionesActivity activity;
    List<Prediction> predictions;
    Location currentLocation;

    PlacesAPI mPlaceAPI = new PlacesAPI();

    public PlacesAutoCompleteAdapter(Context context, int resource, Location currentLocation) {
        super(context, resource);
        mContext = context;
        mResource = resource;
//        activity = (DireccionesActivity) mContext;
        this.currentLocation = currentLocation;
        if (currentLocation!=null){
            Log.v("LocationLatitude",String.valueOf(currentLocation.getLatitude()));
            Log.v("LocationLongitude",String.valueOf(currentLocation.getLongitude()));
            Log.v("LocationLatitudeLocal",String.valueOf(this.currentLocation.getLatitude()));
            Log.v("LocationLongitudeLocal",String.valueOf(this.currentLocation.getLongitude()));
        }
    }

    @Override
    public int getCount() {
        // Last item will be the footer
        return resultList.size();
    }

    @Override
    public String getItem(int position) {
        return resultList.get(position).getDescription();
    }

    public Prediction getPredictionItem(int position){ return resultList.get(position);}

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                if (constraint != null) {
                    resultList = mPlaceAPI.autocomplete(constraint.toString(),currentLocation);
                    List<String> descriptions = new ArrayList<String>();
                    if (resultList.size()>0){
                        setPredictions(resultList);
                        for (Prediction item: resultList){
                            descriptions.add(item.getDescription());
                        }
                    }
                    filterResults.values = resultList;
                    filterResults.count = resultList.size();
                }

                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null && results.count > 0) {
                    notifyDataSetChanged();
                }
                else {
                    notifyDataSetInvalidated();
                }
            }
        };

        return filter;
    }

    public List<Prediction> getPredictions() {
        if (predictions!=null)
            predictions = new ArrayList<Prediction>();
        return predictions;
    }

    public void setPredictions(List<Prediction> predictions) {
        this.predictions = predictions;
    }
}