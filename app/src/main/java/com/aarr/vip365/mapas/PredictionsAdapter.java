package com.aarr.vip365.mapas;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aarr.vip365.R;
import com.aarr.vip365.services.PlaceAutocomplete.Prediction;
import com.aarr.vip365.views.MenuActivity;

import java.util.List;

/**
 * Created by andresrodriguez on 3/2/17.
 */
public class PredictionsAdapter extends RecyclerView.Adapter<PredictionsAdapter.PredictionsViewHolder> {

    private Context adapterContext;
    private List<Prediction> prediction = null;
    private Dialog dialog = null;
    private MenuActivity activity;

    public PredictionsAdapter(List<Prediction> items, Context context){
        setPredictions(items);
        adapterContext = context;
        if (adapterContext instanceof MenuActivity){
            activity = (MenuActivity) adapterContext;
        }
    }

    public List<Prediction> getPredictions() {
        return prediction;
    }

    public void setPredictions(List<Prediction> prediction) {
        this.prediction = prediction;
    }

    public static class PredictionsViewHolder extends RecyclerView.ViewHolder {
        public TextView direccion;
        public TextView distancia;
        public RelativeLayout baseLayout;

        public PredictionsViewHolder(View v){
            super(v);
            direccion = (TextView) v.findViewById(R.id.direccion);
//            distancia = (TextView) v.findViewById(R.id.distancia);
            baseLayout = (RelativeLayout) v.findViewById(R.id.baseLayout);
        }
    }

    @Override
    public PredictionsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.prediction_search_item, parent, false);
        return new PredictionsViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final PredictionsViewHolder holder, final int position) {
        if(getPredictions() != null){
            if(getPredictions().size() > 0){
                holder.direccion.setText(getPredictions().get(position).getDescription());
                holder.baseLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (activity!=null){
                            Log.v("Prediction",getPredictions().get(position).toString());
                            activity.getDetails(getPredictions().get(position).getPlaceId());
                        }
                    }
                });
            }
        }
    }

    @Override
    public int getItemCount() {
        return prediction.size();
    }
}
