package com.aarr.vip365

import android.app.Application
import android.content.Context
//import android.support.multidex.MultiDex
import com.facebook.FacebookSdk.getApplicationContext
//import android.support.multidex.MultiDexApplication


//import android.support.multidex.MultiDex
//import android.support.multidex.MultiDexApplication


//class MainApplication : MultiDexApplication() {
//    init {
//        enableMultiDexApp = this
//    }
//
//    override fun onCreate() {
//        super.onCreate()
//        context = applicationContext
//
//    }
//
//    companion object {
//        var enableMultiDexApp: MainApplication? = null
//        var context: Context? = null
//    }
//}

class MainApplication : Application() {

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
//        MultiDex.install(this)
    }

    override fun onCreate() {
        super.onCreate()
        MainApplication.appContext = applicationContext
    }

    companion object {
        var appContext: Context? = null
            private set
    }

}
