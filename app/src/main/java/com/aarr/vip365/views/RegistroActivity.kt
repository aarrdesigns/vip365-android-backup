package com.aarr.vip365.views


//import `in`.championswimmer.libsocialbuttons.buttons.BtnFacebook
//import `in`.championswimmer.libsocialbuttons.buttons.BtnGoogleplus
import android.app.Activity
import android.app.Dialog
import android.app.ProgressDialog
import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.View
import com.aarr.vip365.R
//import com.jaychang.sa.SocialUser
//import com.jaychang.sa.AuthCallback
//import com.jaychang.sa.SimpleAuth
import java.util.*
import com.aarr.vip365.database.Dao.UsersDao
import com.aarr.vip365.database.Model.UsersModel
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.ConnectionResult
import android.content.Intent
import android.media.Image
import android.telephony.TelephonyManager
import android.view.Window
import android.widget.*
import com.aarr.vip365.services.RegistroService
import com.aarr.vip365.util.CountryData
import com.aarr.vip365.util.CountryUtils
import com.facebook.*
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignInResult
import org.json.JSONObject

class RegistroActivity : AppCompatActivity() {

    private var toolbar: Toolbar? = null
    val FACEBOOK = "FACEBOOK"
    val GOOGLE = "GOOGLE"
    val TWITTER = "TWITTER"
    val INSTAGRAM = "INSTAGRAM"
    var btnGoogle: RelativeLayout? = null
    var btnFacebook: RelativeLayout? = null
    var txtName: EditText? = null
    var txtLastName: EditText? = null
    var txtEmail: EditText? = null
    var txtPhoneNumber: EditText? = null
    var txtPassword: EditText? = null
    var btnSave: Button? = null
    var mGoogleApiClient: GoogleApiClient? = null
    var countryFlag: ImageView? = null
    var countryNumberCode: TextView? = null
    var countryDataContainer: LinearLayout? = null
    var countryCode: String? = null
    var REQUEST_COUNTRY = 1000
    var REQUEST_PHONE = 1002
    private val RC_SIGN_IN = 9001
    private var dialog: ProgressDialog? = null
    var mCallbackManager: CallbackManager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registro)

        toolbar = findViewById<Toolbar>(R.id.toolbar) as Toolbar
        toolbar!!.setTitle("Registrate")
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        toolbar!!.setNavigationOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                onBackPressed()
            }
        })

        countryFlag = findViewById<ImageView>(R.id.countryFlag) as ImageView
        countryDataContainer = findViewById<LinearLayout>(R.id.countryDataContainer) as LinearLayout
        countryDataContainer!!.setOnClickListener {
            var intent = Intent(this,CountryListActivity::class.java)
            startActivityForResult(intent,REQUEST_COUNTRY)
        }
        countryNumberCode = findViewById<TextView>(R.id.countryNumberCode) as TextView

        txtName = findViewById<EditText>(R.id.txtName) as EditText
        txtLastName = findViewById<EditText>(R.id.txtLastName) as EditText
        txtEmail = findViewById<EditText>(R.id.txtEmail) as EditText
        txtPhoneNumber = findViewById<EditText>(R.id.txtPhoneNumber) as EditText
        txtPassword = findViewById<EditText>(R.id.txtPassword) as EditText
        btnSave = findViewById<Button>(R.id.btnSave) as Button
        btnSave!!.setOnClickListener {
            val name = txtName!!.text.toString()
            val lastName = txtLastName!!.text.toString()
            val email = txtEmail!!.text.toString()
            val phoneNumber = countryNumberCode!!.text.toString()+txtPhoneNumber!!.text.toString()
            val password = txtPassword!!.text.toString()
            val user = UsersModel(-1,-1,name,lastName,email,phoneNumber,password,"",countryCode!!,1)

            saveUser(user)
        }

        initGoogleClient()
        btnGoogle = findViewById<RelativeLayout>(R.id.btnGoogle) as RelativeLayout
        btnGoogle!!.setOnClickListener {
            googleSignIn()
        }

        btnFacebook = findViewById<RelativeLayout>(R.id.btnFacebook) as RelativeLayout
        btnFacebook!!.setOnClickListener {
//            connectFacebook()
            LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "user_friends"));
        }

        var tm = getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
        countryCode = tm.simCountryIso
        var country = CountryUtils().getCountryByCode(countryCode!!.replace(" ",""))
        if (country!=null){
            countryFlag!!.setImageResource(country!!.imgFlag)
            countryNumberCode!!.setText(country!!.extention)
        }else{
            Log.v("CountryCode","Country data is null")
        }
        Log.v("CountryCode",countryCode)

        initFacebookClient()

    }

    fun initFacebookClient(){
        FacebookSdk.sdkInitialize(this.getApplicationContext());

        mCallbackManager = CallbackManager.Factory.create();

        LoginManager.getInstance().registerCallback(mCallbackManager,object: FacebookCallback<LoginResult>{
            override fun onSuccess(result: LoginResult?) {
                Log.v("RegistroActivity", "Login")
                var request = GraphRequest
                        .newMeRequest(
                                result!!.accessToken,
                                object: GraphRequest.GraphJSONObjectCallback {
                                    override fun onCompleted(jsonObject: JSONObject?, response: GraphResponse?) {
                                        Log.v("RegistroActivity", response.toString())
                                        Log.v("RegistroActivity", jsonObject.toString())
                                        var idFacebook = jsonObject!!.get("id")
                                        var name = jsonObject.get("name") as String
                                        var email = jsonObject.get("email") as String
                                        var profilePicture = "https://graph.facebook.com/" + idFacebook + "/picture?type=large"
                                        var newUser = UsersModel(
                                                -1,
                                                -1,
                                                name,
                                                "",
                                                email,
                                                "",
                                                "",
                                                profilePicture,
                                                "",
                                                3
                                        )
                                        askPhoneNumber(newUser)
//                                        showProgressDialog()
//                                        RegistroService(this@RegistroActivity).handleRegister(newUser)
//                                        saveUserSocial(newUser,3)
                                        Log.v("RegistroActivity", jsonObject.toString())
                                    }
                                })
                val parameters = Bundle()
                parameters.putString("fields", "id,name,email,gender, birthday")
                request.parameters = parameters
                request.executeAsync()

    //            Log.v("FacebookConnect","Email: "+socialUser.email)
    //            Log.v("FacebookConnect","Full name:"+socialUser.fullName)
    //            Log.v("FacebookConnect","Profile Picture: "+socialUser.profilePictureUrl)
    //            saveUser(socialUser,3)
            }

            override fun onCancel() {
                Toast.makeText(this@RegistroActivity, "Login Cancel", Toast.LENGTH_LONG).show()
            }

            override fun onError(error: FacebookException?) {
                Toast.makeText(this@RegistroActivity, error!!.message, Toast.LENGTH_LONG).show()
            }

        })
    }

    fun initGoogleClient(){
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build()

        mGoogleApiClient = GoogleApiClient.Builder(this)
                .enableAutoManage(this, object: GoogleApiClient.OnConnectionFailedListener{
                    override fun onConnectionFailed(p0: ConnectionResult) {
                        Log.e("GoogleError",p0.errorMessage)
                    }
                } /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build()
    }

    fun googleSignIn(){
        val signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient)
        startActivityForResult(signInIntent, RC_SIGN_IN)
    }

    fun connectFacebook() {
        val scopes = Arrays.asList("user_birthday", "user_friends")
//
//        SimpleAuth.getInstance().connectFacebook(scopes, object : AuthCallback {
//            override fun onSuccess(socialUser: SocialUser) {
//                Log.v("FacebookConnect","Email: "+socialUser.email)
//                Log.v("FacebookConnect","Full name:"+socialUser.fullName)
//                Log.v("FacebookConnect","Profile Picture: "+socialUser.profilePictureUrl)
//                saveUser(socialUser,3)
//            }
//
//            override fun onError(error: Throwable) {
//                toast(error.message!!)
//            }
//
//            override fun onCancel() {
//                toast("Canceled")
//            }
//        })
    }

    fun askPhoneNumber(user:UsersModel?){
        if (user!=null){
            var intent = Intent(this,RequestPhoneActivity::class.java)
            intent.putExtra("User",user)
            startActivityForResult(intent,REQUEST_PHONE)
        }
//        var dialog = Dialog(this)
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
//        dialog.setContentView(R.layout.phone_number_dialog)
//
//        val phoneNumber = dialog.findViewById(R.id.txtPhoneNumber) as EditText
//        val btnSave = dialog.findViewById(R.id.btnSave) as Button
//        btnSave.setOnClickListener {
//            if (phoneNumber.text.toString()!=null && !phoneNumber.text.toString().equals("")){
//                user!!.phoneNumber = phoneNumber!!.text.toString()
////                showProgressDialog()
////                RegistroService(this).handleRegister(user)
////                val insert = UsersDao().updateOrCreate(user)
////                if (insert>0){
////                    var returnIntent = Intent()
////                    setResult(Activity.RESULT_OK,returnIntent)
////                    finish()
//////                    when (user.typeUser){
//////                        2 -> MenuActivity().start(this@RegistroActivity, FACEBOOK, user)
//////                        3 -> MenuActivity().start(this@RegistroActivity, GOOGLE, user)
//////                    }
////                }
//            }
//        }
//        dialog.show()
    }

    fun connectGoogle() {
        val scopes = Arrays.asList(
                "https://www.googleapis.com/auth/youtube",
                "https://www.googleapis.com/auth/youtube.upload"
        )
//        SimpleAuth.getInstance().connectGoogle(scopes, object : AuthCallback {
//            override fun onSuccess(socialUser: SocialUser) {
//                Log.v("GoogleConnect","Email: "+socialUser.email)
//                Log.v("GoogleConnect","Full name:"+socialUser.fullName)
//                Log.v("GoogleConnect","Profile Picture: "+socialUser.profilePictureUrl)
//                saveUser(socialUser,2)
//            }
//
//            override fun onError(error: Throwable) {
//                toast(error.message!!)
//            }
//
//            override fun onCancel() {
//                toast("Canceled")
//            }
//        })
    }

    private fun toast(msg: String) {
        Toast.makeText(this@RegistroActivity, msg, Toast.LENGTH_LONG).show()
    }

    fun saveUserSocial(user:UsersModel?,social:Int){
        if (user!=null){
            askPhoneNumber(user)
        }else{
            when (social){
                2 -> toast("Error iniciando con Google")
                3 -> toast("Error iniciando con Facebook")
            }
        }
    }

    fun saveUser(user:UsersModel?){
        if (user!=null){
            var isError = false
            if (user!!.password==null || user!!.password.equals("")){
                isError = true
                txtPassword!!.setError("Este campo es requerido")
            }
            if (user!!.phoneNumber==null || user!!.phoneNumber.equals("")){
                isError = true
                txtPhoneNumber!!.setError("Este campo es requerido")
            }
            if (user!!.email==null || user!!.email.equals("")){
                isError = true
                txtEmail!!.setError("Este campo es requerido")
            }
            if (user!!.lastName==null || user!!.lastName.equals("")){
                isError = true
                txtLastName!!.setError("Este campo es requerido")
            }
            if (user!!.name==null || user!!.name.equals("")){
                isError = true
                txtName!!.setError("Este campo es requerido")
            }

            if (!isError){
                Log.v("NormalConnect","User Data: "+user!!.toString())
                showProgressDialog()
                RegistroService(this).handleRegister(user)
            }
        }else{

        }
    }

    fun dismissDialog(){
        if (dialog!=null){
            dialog!!.dismiss()
        }
    }

    fun handleSuccessResponse(user:UsersModel){
        dismissDialog()
        val insert = UsersDao().updateOrCreate(user)
        if (insert>0){
            var returnIntent = Intent()
            setResult(Activity.RESULT_OK,returnIntent)
            finish()
        }
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            val result = Auth.GoogleSignInApi.getSignInResultFromIntent(data)
            handleSignInResult(result)
        }else if (requestCode == REQUEST_COUNTRY){
            if (resultCode == Activity.RESULT_OK){
                val result = data!!.getSerializableExtra("CountryData") as CountryData
                if (result!=null){
                    countryCode = result!!.code
                    countryFlag!!.setImageResource(result!!.imgFlag)
                    countryNumberCode!!.setText(result!!.extention)
                }
            }
        }else if(mCallbackManager!!.onActivityResult(requestCode, resultCode, data)) {
            return
        }else if (requestCode == REQUEST_PHONE){
            if (resultCode == Activity.RESULT_OK){
                val user = data!!.getSerializableExtra("User") as UsersModel
                if (user!=null){
                    showProgressDialog()
                    RegistroService(this).handleRegister(user)
                }
            }
        }
    }

    private fun handleSignInResult(result: GoogleSignInResult) {
        Log.d("GoogleConnect", "handleSignInResult:" + result.isSuccess)
        Log.d("GoogleConnect", "handleSignInResult:" + result.status.statusCode)
        Log.d("GoogleConnect", "handleSignInResult:" + result.status.statusMessage)
        if (result.isSuccess) {
            // Signed in successfully, show authenticated UI.
            val acct = result.signInAccount
            Log.v("GoogleConnect", "Full Name:" + acct!!.displayName)
            Log.v("GoogleConnect", "Name:" + acct!!.givenName)
            Log.v("GoogleConnect", "LastName:" + acct!!.familyName)
            Log.v("GoogleConnect", "Email:" + acct!!.email)
            Log.v("GoogleConnect", "Photo:" + acct!!.photoUrl)
            var name = acct!!.givenName
            var lastName = acct!!.familyName
            var email = acct!!.email
            var profilePicture = acct!!.photoUrl.toString()
            var newUser = UsersModel(
                    -1,
                    -1,
                    name!!,
                    lastName!!,
                    email!!,
                    "",
                    "",
                    profilePicture!!,
                    "",
                    2
            )
            askPhoneNumber(newUser)
//            showProgressDialog()
//            RegistroService(this).handleRegister(newUser)
//            saveUserSocial(newUser,2)
        } else {

        }
    }

    fun showProgressDialog(){
        dialog = ProgressDialog(this)
        dialog!!.setMessage("Porfavor espere...")
        dialog!!.show()
    }
}
