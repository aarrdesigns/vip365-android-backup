package com.aarr.vip365.views

import android.Manifest
import android.animation.Animator
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.graphics.Color
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.location.Location
import android.location.LocationListener
import android.media.ExifInterface
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.SystemClock
import android.support.design.widget.FloatingActionButton
import android.support.design.widget.Snackbar
import android.support.design.widget.NavigationView
import android.support.v4.app.ActivityCompat
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.*
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.widget.*
import com.aarr.vip365.database.Dao.UsersDao
import com.aarr.vip365.database.Model.UsersModel
import com.aarr.vip365.mapas.PlacesAutoCompleteAdapter
import com.aarr.vip365.R
import com.aarr.vip365.database.Model.PaymentMethods
import com.aarr.vip365.database.Model.TripsModel
import com.aarr.vip365.services.PlaceDetails.Result
import com.aarr.vip365.services.PlaceDetailsApi
import com.aarr.vip365.services.RegisterPaymentService
import com.aarr.vip365.services.TripService
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.google.android.gms.location.LocationServices
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
//import com.jaychang.sa.SocialUser
import java.io.File
import java.util.*

class MenuActivity : AppCompatActivity(), SensorEventListener, NavigationView.OnNavigationItemSelectedListener, OnMapReadyCallback, LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private var mMap: GoogleMap? = null
    private var mGoogleApiClient: GoogleApiClient? = null
    private var locationSearch: AutoCompleteTextView? = null
    private var reciclador: RecyclerView? = null
    private var currentLocation: Location? = null
    private var placeId: String? = null
    private var currentResult: Result? = null
    private val EXTRA_USER = "EXTRA_USER"
    private val EXTRA_TYPE = "EXTRA_TYPE"
    private var myMarker:Marker? = null
    private var isMarkerRotating = false
    private var sensorMan: SensorManager? = null
    var mGravity: FloatArray? = null
    var mMagnetic: FloatArray? = null
    var compass_last_measured_bearing: Float = 0F
    var currentUser: UsersModel? = null
    var txtNombre: TextView? = null
    var txtCalificacion: TextView? = null
    var imgProfile: ImageView? = null
    var navigationView: NavigationView? = null
    var revealContainer: RelativeLayout? = null
    private var accelerometer: Sensor? = null
    private var mAccel: Float = 0.toFloat()
    private var mAccelCurrent: Float = 0.toFloat()
    private var mAccelLast: Float = 0.toFloat()
    var REQUEST_CODE = 100
    var mapFragment: SupportMapFragment ? = null
    var btnEmergency: FloatingActionButton? = null
    var btnAlert: FloatingActionButton? = null

    private val type: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)

        checkPaymentMethods()

        val toolbar = findViewById<Toolbar>(R.id.toolbar) as Toolbar
        toolbar!!.setTitle(" ")
        setSupportActionBar(toolbar)

        btnEmergency = findViewById<FloatingActionButton>(R.id.btnEmergency) as FloatingActionButton
        btnEmergency!!.setOnClickListener {
            var user = UsersDao().getCurrentUser()
            var myLocation = currentLocation!!.latitude.toString()+","+currentLocation!!.longitude
            var trip = TripsModel(
                    -1,
                    -1,
                    user!!.idServer,
                    -1,
                    Date(),
                    Date(),
                    myLocation,
                    myLocation,
                    "Mi Ubicacion",
                    "Ubicacion de destino",
                    1,
                    myLocation,
                    0.0,
                    2,
                    -1,
                    false
            )
            Log.e("Service","Request: "+trip.toString())
            TripService(this).registerTrip(trip)
        }
        btnAlert = findViewById<FloatingActionButton>(R.id.btnAlert) as FloatingActionButton
        btnAlert!!.setOnClickListener {
            var user = UsersDao().getCurrentUser()
            var myLocation = currentLocation!!.latitude.toString()+","+currentLocation!!.longitude
            var trip = TripsModel(
                    -1,
                    -1,
                    user!!.idServer,
                    -1,
                    Date(),
                    Date(),
                    myLocation,
                    myLocation,
                    "Mi Ubicacion",
                    "Ubicacion de destino",
                    1,
                    myLocation,
                    0.0,
                    3,
                    -1,
                    false
            )
            TripService(this).registerTrip(trip)
        }

        locationSearch = findViewById<AutoCompleteTextView>(R.id.locationSearch) as AutoCompleteTextView
        locationSearch!!.setOnClickListener {
            var intent = Intent(this,BuscarRutaActivity::class.java)
            startActivity(intent)
        }

        revealContainer = findViewById<RelativeLayout>(R.id.revealContainer) as RelativeLayout


        val fab = findViewById<FloatingActionButton>(R.id.fab) as FloatingActionButton
        fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
        }

        val drawer = findViewById<DrawerLayout>(R.id.drawer_layout) as DrawerLayout
        val toggle = ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer.setDrawerListener(toggle)
        toggle.syncState()

        navigationView = findViewById<NavigationView>(R.id.nav_view) as NavigationView
        navigationView!!.setNavigationItemSelectedListener(this)
        setUserData()

        // Create an instance of GoogleAPIClient.
        if (mGoogleApiClient == null) {
            mGoogleApiClient = GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build()
            mGoogleApiClient!!.connect()
        }

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        try{
            mapFragment = supportFragmentManager!!
                    .findFragmentById(R.id.mapView) as SupportMapFragment
            mapFragment!!.getMapAsync(this)

        }catch (e:Exception){
            Log.w("Exception",e.toString())
        }

        initSensors()

    }

    fun setUserData(){
        var header = navigationView!!.getHeaderView(0)
        txtNombre = header.findViewById<TextView>(R.id.txtNombre) as TextView
        txtCalificacion = header.findViewById<TextView>(R.id.txtCalificacion) as TextView
        imgProfile = header.findViewById<ImageView>(R.id.imgProfile) as ImageView

        currentUser = UsersDao().getCurrentUser()
        if (currentUser!=null){
            txtNombre!!.setText(currentUser!!.name+" "+currentUser!!.lastName)
//            txtCalificacion!!.setText(currentUser!!.name+" "+currentUser!!.lastName)
            if (currentUser!!.profileImage!=null && !currentUser!!.profileImage.equals("")){

                var imageUri = Uri.parse(File(currentUser!!.profileImage).toString())

                val exif = ExifInterface(imageUri.getPath())
                val rotation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL)
                Log.e("ROTATIONVALUE",rotation.toString())

                val options = RequestOptions()
                options.diskCacheStrategy(DiskCacheStrategy.NONE)
                options.skipMemoryCache(true)
                options.fitCenter()
                options.dontTransform()
                options.dontAnimate()
                Glide
                        .with(this)
                        .load(currentUser!!.profileImage)
                        .apply(options)
                        .into(imgProfile)
            }
        }
    }

    fun initSensors() {
        sensorMan = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        accelerometer = sensorMan!!.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)
        mAccel = 0.00f
        mAccelCurrent = SensorManager.GRAVITY_EARTH
        mAccelLast = SensorManager.GRAVITY_EARTH
    }

    override fun onBackPressed() {
        val drawer = findViewById<DrawerLayout>(R.id.drawer_layout) as DrawerLayout
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        val id = item.itemId

        if (id == R.id.pago_item) {
            showPagos()
        } else if (id == R.id.viajes_item) {

        } else if (id == R.id.ayuda_item) {

        } else if (id == R.id.config_item) {
            showConfiguration()
        }
//        else if (id == R.id.nav_share) {
//
//        } else if (id == R.id.nav_send) {
//
//        }

        val drawer = findViewById<DrawerLayout>(R.id.drawer_layout) as DrawerLayout
        drawer.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onStart() {
        super.onStart()
//        if (mGoogleApiClient!=null){
//            mGoogleApiClient!!.connect()
//        }
    }

    override fun onStop() {
        super.onStop()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (mGoogleApiClient!=null){
            mGoogleApiClient!!.disconnect()
        }
    }

    @SuppressLint("MissingPermission")
    override fun onMapReady(map: GoogleMap?) {
        mMap = map
        mMap!!.uiSettings.isCompassEnabled = false
        mMap!!.isMyLocationEnabled = true
        var locationButton = ( mapFragment!!.view!!.findViewById<View>(Integer.parseInt("1")).getParent() as View).findViewById<ImageView>(Integer.parseInt("2")) as ImageView
//        locationButton.setBackgroundResource(R.drawable.circle_bg_black_border)
//        locationButton.setPadding(3,3,3,3)
        var rlp =  locationButton!!.getLayoutParams() as RelativeLayout.LayoutParams

        // position on right bottom
        rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0)
        rlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE)
        rlp.setMargins(0, 0, 30, 30)
        mMap!!.setOnMyLocationButtonClickListener(object: GoogleMap.OnMyLocationButtonClickListener{
            override fun onMyLocationButtonClick(): Boolean {

                return false
            }
        })
    }

    fun getLocation(init:Boolean):LatLng? {
        var newLocation: LatLng? = null
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return null
        }
        val mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient)
        if (mLastLocation != null) {
            currentLocation = mLastLocation
            Log.v("GooglePlacesApi", mLastLocation.toString())
            newLocation = LatLng(mLastLocation.latitude,mLastLocation.longitude)
            val icon = BitmapFactory.decodeResource(this@MenuActivity.getResources(),
                    R.mipmap.pin_user_24)
//            if (myMarker==null){
//                myMarker = mMap!!.addMarker(MarkerOptions()
//                        .position(newLocation!!)
//                        .icon(BitmapDescriptorFactory.fromBitmap(icon))
//                        .flat(true))
//            }else{
//                animateMarker(myMarker!!,newLocation,false,true)
//            }
//            mMap!!.addMarker(MarkerOptions().position(newLocation!!))
            if (!init){
                mMap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(newLocation, 16.0f))
            }else{
                mMap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(newLocation, 22.0f))
            }

        }else{
            Log.v("GooglePlacesApi", "Location Is Null")
        }
        return newLocation
    }



    fun showPagos(){
        var intent = Intent(this,PagosActivity::class.java)
        startActivity(intent)
    }

    fun showConfiguration(){
        var intent = Intent(this,ConfiguracionActivity::class.java)
        startActivityForResult(intent,REQUEST_CODE)
    }

//    fun fillAndResetRecycler(lista: List<Prediction>?) {
//        if (lista != null) {
//            reciclador.setAdapter(null)
//            val adapter = PredictionsAdapter(lista, this)
//            reciclador.setLayoutManager(LinearLayoutManager(this))
//            reciclador.setAdapter(adapter)
//        } else {
//            Toast.makeText(this, "Error al recuperar resultados, intente de nuevo", Toast.LENGTH_LONG).show()
//        }
//    }

    fun initBuscador() {
        val adapter = PlacesAutoCompleteAdapter(this, R.layout.autocomplete_list_item, currentLocation)
        locationSearch!!.setAdapter(adapter)
        locationSearch!!.setOnItemClickListener(AdapterView.OnItemClickListener { parent, view, position, id ->
            val description = adapter.getPredictionItem(position)
            //                Toast.makeText(RastreoPedidoActivity.this, description.getPlaceId(), Toast.LENGTH_SHORT).show();
            Log.v("PlaceId", description.getPlaceId())
            val placeDetailsApi = PlaceDetailsApi(this@MenuActivity, PlaceDetailsApi.ORIGEN)
            placeDetailsApi.getPlaceDetails(description.getPlaceId())
        })
    }

    fun animateMarker(marker: Marker, toPosition: LatLng,
                      hideMarker: Boolean, isMyLocation:Boolean) {
        val handler = Handler()
        val start = SystemClock.uptimeMillis()
        val proj = mMap!!.getProjection()
        val startPoint = proj.toScreenLocation(marker.position)
        val startLatLng = proj.fromScreenLocation(startPoint)
        val duration: Long = 500


        val interpolator = LinearInterpolator()

        handler.post(object : Runnable {
            override fun run() {
                val elapsed = SystemClock.uptimeMillis() - start
                val t = interpolator.getInterpolation(elapsed.toFloat() / duration)
                val lng = t * toPosition.longitude + (1 - t) * startLatLng.longitude
                val lat = t * toPosition.latitude + (1 - t) * startLatLng.latitude
                val newPosition = LatLng(lat, lng)
                marker.setPosition(newPosition)
//                if (!isMyLocation){
//                    polyline.add(newPosition)
//                    polyline.width(10f)
//                    polyline.color(Color.BLUE)
//                    mMap.addPolyline(polyline)
//                }

                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16)
                } else {
                    if (hideMarker) {
                        marker.isVisible = false
                    } else {
                        marker.isVisible = true
                    }
                }
            }
        })
    }

    fun getDetails(placeId: String) {
        if (placeId != "") {
            this.placeId = placeId
            Log.v("PlaceId", placeId)
            val placeDetailsApi = PlaceDetailsApi(this, PlaceDetailsApi.DIRECCIONES)
            placeDetailsApi.getPlaceDetails(placeId)
        }
    }

    fun setLocation(result: Result?, estado: Int) {
        val direccion = ""
        when (estado) {
            PlaceDetailsApi.ORIGEN -> if (result != null) {
                currentResult = result
                Log.v("MapPlaceLat", result!!.getGeometry().getLocation().getLat().toString())
                Log.v("MapPlaceLng", result!!.getGeometry().getLocation().getLng().toString())
                val lat = result!!.getGeometry().getLocation().getLat()
                val lng = result!!.getGeometry().getLocation().getLng()

                val location = Location(android.location.LocationManager.GPS_PROVIDER)
                location.latitude = lat.toDouble()
                location.longitude = lng.toDouble()
                Log.v("CurrentLocation", lat.toString() + "," + lng.toString())
                if (location != null) {
                    currentLocation = location
                }
                val newPosition = LatLng(lat.toDouble(), lng.toDouble())
//                origen = lat.toString() + "," + lng.toString()
//                mapPin.setVisibility(View.VISIBLE)
                mMap!!.clear()
                mMap!!.addMarker(MarkerOptions().position(newPosition).title(direccion))
                mMap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(newPosition, 16.0f))
            }
        }
    }

    override fun onLocationChanged(location: Location?) {

    }

    override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {

    }

    override fun onProviderEnabled(provider: String?) {

    }

    override fun onProviderDisabled(provider: String?) {

    }

    override fun onPause() {
        super.onPause()
        sensorMan!!.unregisterListener(this)
    }

    override fun onConnected(p0: Bundle?) {
        Log.v("GooglePlacesApi", "Connected")
        locationSearch!!.setEnabled(true)
        locationSearch!!.requestFocus()
        val newLoc = getLocation(true)
        try{
            revealMap(newLoc!!)
        }catch (e:Exception){
            Toast.makeText(this,"Reveal: "+e.toString(),Toast.LENGTH_LONG).show()
            Log.v("RevealException", e.toString())
        }
//        initBuscador()
    }

    override fun onConnectionSuspended(p0: Int) {

    }

    override fun onConnectionFailed(p0: ConnectionResult) {

    }

    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {

    }

    override fun onSensorChanged(event: SensorEvent) {
        if (event.sensor.type == Sensor.TYPE_ACCELEROMETER) {
            mGravity = event.values.clone()
            // Shake detection
            val x = mGravity!![0]
            val y = mGravity!![1]
            val z = mGravity!![2]
            mAccelLast = mAccelCurrent
            mAccelCurrent = Math.sqrt((x * x + y * y + z * z).toDouble()).toFloat()
            val delta = mAccelCurrent - mAccelLast
            mAccel = mAccel * 0.9f + delta
            // Make this higher or lower according to how much
            // motion you want to detect
            if (mAccel > 10) {
                Log.v("Location", "Acceleration: " + mAccel + "")
//                Toast.makeText(this, "Moving...", Toast.LENGTH_SHORT).show()
//                getLocation(false)
            }
        }

    }

    private fun rotateMarker(marker: Marker, toRotation: Float) {
        if (!isMarkerRotating) {
            val handler = Handler()
            val start = SystemClock.uptimeMillis()
            val startRotation = marker.rotation
            val duration: Long = 2000

            val interpolator = LinearInterpolator()

            handler.post(object : Runnable {
                override fun run() {
                    isMarkerRotating = true

                    val elapsed = SystemClock.uptimeMillis() - start
                    val t = interpolator.getInterpolation(elapsed.toFloat() / duration)

                    val rot = t * toRotation + (1 - t) * startRotation

                    val bearing = if (-rot > 180) rot / 2 else rot

                    marker.rotation = bearing

                    if (t < 1.0) {
                        // Post again 16ms later.
                        handler.postDelayed(this, 16)
                    } else {
                        isMarkerRotating = false
                    }
                }
            })
        }
    }

//    fun start(context: Context, type:String, socialUser: SocialUser){
//        var intent = Intent(context, MenuActivity::class.java)
//        intent.putExtra(EXTRA_USER, socialUser)
//        intent.putExtra(EXTRA_TYPE, type)
//        context.startActivity(intent)
//    }

    override fun onResume() {
        super.onResume()
        setUserData()
        sensorMan!!.registerListener(this, accelerometer,
                SensorManager.SENSOR_DELAY_UI)
    }

    fun revealMap(newLocation:LatLng){
        var x = revealContainer!!.getRight()
        var y = revealContainer!!.getBottom()

//        var startRadius = 0
//        var endRadius =  Math.hypot(revealContainer!!.getWidth().toDouble(), revealContainer!!.getHeight().toDouble()).toInt()

        var startRadius = Math.max(revealContainer!!.getWidth().toDouble(), revealContainer!!.getHeight().toDouble()).toInt()
        var endRadius =  0

        // get the center for the clipping circle
        var cx = revealContainer!!.getMeasuredWidth() / 2;
        var cy = revealContainer!!.getMeasuredHeight() / 2;

        // get the initial radius for the clipping circle
        var initialRadius = revealContainer!!.getWidth() / 2;

        var anim = ViewAnimationUtils.createCircularReveal(revealContainer, cx, cy, initialRadius.toFloat(),0F)
        anim.duration = 1000
//        anim.startDelay = 1500

        anim.addListener(object : Animator.AnimatorListener {
            override fun onAnimationRepeat(animation: Animator?) {
            }

            override fun onAnimationEnd(animation: Animator?) {
                Log.e("Animator","Animation Ends")
                mMap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(newLocation, 16.0f))
                revealContainer!!.setVisibility(View.GONE)
            }

            override fun onAnimationCancel(animation: Animator?) {
            }

            override fun onAnimationStart(animation: Animator?) {
                Log.e("Animator","Animation Starts")
            }
        })
//        revealContainer!!.setVisibility(View.GONE)
        anim.start()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE){
            if (resultCode == Activity.RESULT_OK){
                finish()
                var intent = Intent(this,MainActivity::class.java)
                startActivity(intent)
            }
        }
    }

    fun checkPaymentMethods(){
        var lst = PaymentMethods().getRepository().queryForAll()
        if (lst.isEmpty()){
            var user = UsersDao().getCurrentUser()
            var cash = PaymentMethods(
                    -1,
                    -1,
                    3,
                    user!!.idServer,
                    null,
                    null,
                    null,
                    null,
                    null,
                    Date(),
                    false,
                    false
            )
            RegisterPaymentService(this).handlePaymentRegister(cash)
        }
    }



}
