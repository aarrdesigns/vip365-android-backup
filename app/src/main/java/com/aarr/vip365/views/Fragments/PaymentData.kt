package com.aarr.vip365.views.Fragments


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import com.aarr.vip365.adapters.PaymentAdapter
import com.aarr.vip365.database.Dao.PaymentsMethodsDao

import com.aarr.vip365.R
import com.aarr.vip365.views.PagosActivity
import xyz.belvi.luhn.cardValidator.models.LuhnCard


/**
 * A simple [Fragment] subclass.
 */
class PaymentData : Fragment() {

    var recycler: RecyclerView? = null
    var addPayment: RelativeLayout? = null
    var currentCard: LuhnCard? = null
    var rootView: View? = null
    var parentActivity: PagosActivity? = null


    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        rootView = inflater!!.inflate(R.layout.fragment_payment_data, container, false)

        parentActivity = activity as PagosActivity

        recycler = rootView!!.findViewById<RecyclerView>(R.id.recycler) as RecyclerView
        recycler!!.isNestedScrollingEnabled = false

        addPayment = rootView!!.findViewById<RelativeLayout>(R.id.addPayment) as RelativeLayout
        addPayment!!.setOnClickListener {
            //            initCardCreation()
            parentActivity!!.loadNewFragment()
        }

        fillAndResetRecycler()

        return rootView
    }

    fun fillAndResetRecycler(){
        recycler!!.adapter = null
        var adapter = PaymentAdapter(PaymentsMethodsDao().findAll(),activity)
        val lManager : LinearLayoutManager? = LinearLayoutManager(activity)
        recycler!!.layoutManager = lManager
        val dividerItemDecoration = DividerItemDecoration(recycler!!.getContext(),
                lManager!!.getOrientation())
        recycler!!.addItemDecoration(dividerItemDecoration)
        recycler!!.adapter = adapter
    }

}// Required empty public constructor
