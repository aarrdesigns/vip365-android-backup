package com.aarr.vip365.views

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.*
import android.location.Location
import android.location.LocationListener
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.SystemClock
import android.support.v4.app.ActivityCompat
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.Transformation
import android.view.inputmethod.InputMethodManager
import android.widget.*
import com.aarr.vip365.adapters.AutocompleteAdapter

import com.aarr.vip365.R
import com.aarr.vip365.adapters.PaymentAdapter
import com.aarr.vip365.config.Configuration
import com.aarr.vip365.database.Dao.PaymentsMethodsDao
import com.aarr.vip365.database.Dao.UsersDao
import com.aarr.vip365.database.Model.PaymentMethods
import com.aarr.vip365.database.Model.TripsModel
import com.aarr.vip365.services.*
import com.aarr.vip365.services.PlaceAutocomplete.Prediction
import com.aarr.vip365.services.PlaceDetails.Result
import com.aarr.vip365.services.PlaceDirections.Leg
import com.aarr.vip365.services.PlaceDirections.PlacesDirections
import com.aarr.vip365.services.PlaceDirections.Route
import com.aarr.vip365.services.PlaceDirections.Step
import com.aarr.vip365.util.MapAnimator
//import com.braintreepayments.api.dropin.DropInActivity
//import com.braintreepayments.api.dropin.DropInRequest
//import com.braintreepayments.api.dropin.DropInResult
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.GoogleMap.OnCameraMoveStartedListener.*
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import java.util.*
import kotlin.collections.ArrayList

class BuscarRutaActivity : AppCompatActivity(), OnMapReadyCallback, LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, View.OnTouchListener {

    private var mMap: GoogleMap? = null
    private var origen: String? = null
    private var destino: String? = null
    private var polyline = PolylineOptions()
    private var marker: Marker? = null
    private var myLocationMarker: Marker? = null
    private var mGoogleApiClient: GoogleApiClient? = null
    var mPlaceAPI = PlacesAPI()
    private var myLocation: Location? = null
    private var currentLocation: Location? = null
    private var destinyLocation: Location? = null
    private var txtOrigen: EditText? = null
    private var txtDestino: EditText? = null
    private var recycler: RecyclerView? = null
    var btnClose: ImageButton? = null
    var mapContainer: LinearLayout? = null
    var predictionContainer: LinearLayout? = null
    val ORIGEN = 1
    val DESTINO = 2
    val DIRECCIONES = 3
    var isPredictionContainerExpanded = false
    var isLocationContainerExpanded = false
    var isConfirmServiceExpanded = false
    var selectLocation: ImageView? = null
    var isCustomLocation = false
    var currentZoom = 0F
    var btnFinish: Button? = null
    var selectLocationsContainer: RelativeLayout? = null
    var confirmServiceContainer: RelativeLayout? = null
    var origenText = "Mi ubicacion"
    var destinoText = ""
    var mapFragment : SupportMapFragment? = null
    var btnConfirmar: Button? = null
    var touchState: Int? = -1
    val PRESSED: Int? = 100
    val RELEASED: Int? = 200
    var paymentMethod:RelativeLayout? = null
    var tipoPago: ImageView? = null
    var nombrePago: TextView? = null
    var dialog: Dialog? = null
    var currentPaymentMethod : PaymentMethods? = null
    var currentType: Int = -1
    var isMapSelection = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_buscar_ruta)

        recycler = findViewById<RecyclerView>(R.id.recycler) as RecyclerView

        btnClose = findViewById<ImageButton>(R.id.btnClose) as ImageButton
        btnClose!!.setOnClickListener {
            finish()
        }

        paymentMethod = findViewById<RelativeLayout>(R.id.paymentMethod) as RelativeLayout
        paymentMethod!!.setOnClickListener {
            showPaymentsDialog()
        }
        tipoPago = findViewById<ImageView>(R.id.tipoPago) as ImageView
        nombrePago = findViewById<TextView>(R.id.nombrePago) as TextView

        btnConfirmar = findViewById<Button>(R.id.btnConfirmar) as Button
        btnConfirmar!!.setOnClickListener {
            /*var user = UsersDao().getCurrentUser()
            var trip = TripsModel(
                    -1,
                    -1,
                    user!!.idServer,
                    -1,
                    Date(),
                    Date(),
                    origen!!,
                    destino!!,
                    origenText,
                    destinoText,
                    1,
                    "none",
                    0.0,
                    1,
                    currentPaymentMethod!!.idServer,
                    false
            )
            Log.e("Service","Request: "+trip.toString())
            TripService(this).registerTrip(trip)*/
//            var dropInRequest = DropInRequest()
//                    .clientToken("eyJ2ZXJzaW9uIjoyLCJhdXRob3JpemF0aW9uRmluZ2VycHJpbnQiOiIyZjM0NWUzZDhkMGFlMjE2OGFmZGQyMjJmMTU5NDBhMzAyZmUxMDYyMzY1NDQ2ZTk4ZDZjOTFiNGQ0ZTQ3NmIyfGNyZWF0ZWRfYXQ9MjAxNy0xMC0yOVQxNjo0ODoxNi42NzExODE5NTUrMDAwMFx1MDAyNm1lcmNoYW50X2lkPTM0OHBrOWNnZjNiZ3l3MmJcdTAwMjZwdWJsaWNfa2V5PTJuMjQ3ZHY4OWJxOXZtcHIiLCJjb25maWdVcmwiOiJodHRwczovL2FwaS5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tOjQ0My9tZXJjaGFudHMvMzQ4cGs5Y2dmM2JneXcyYi9jbGllbnRfYXBpL3YxL2NvbmZpZ3VyYXRpb24iLCJjaGFsbGVuZ2VzIjpbXSwiZW52aXJvbm1lbnQiOiJzYW5kYm94IiwiY2xpZW50QXBpVXJsIjoiaHR0cHM6Ly9hcGkuc2FuZGJveC5icmFpbnRyZWVnYXRld2F5LmNvbTo0NDMvbWVyY2hhbnRzLzM0OHBrOWNnZjNiZ3l3MmIvY2xpZW50X2FwaSIsImFzc2V0c1VybCI6Imh0dHBzOi8vYXNzZXRzLmJyYWludHJlZWdhdGV3YXkuY29tIiwiYXV0aFVybCI6Imh0dHBzOi8vYXV0aC52ZW5tby5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tIiwiYW5hbHl0aWNzIjp7InVybCI6Imh0dHBzOi8vY2xpZW50LWFuYWx5dGljcy5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tLzM0OHBrOWNnZjNiZ3l3MmIifSwidGhyZWVEU2VjdXJlRW5hYmxlZCI6dHJ1ZSwicGF5cGFsRW5hYmxlZCI6dHJ1ZSwicGF5cGFsIjp7ImRpc3BsYXlOYW1lIjoiQWNtZSBXaWRnZXRzLCBMdGQuIChTYW5kYm94KSIsImNsaWVudElkIjpudWxsLCJwcml2YWN5VXJsIjoiaHR0cDovL2V4YW1wbGUuY29tL3BwIiwidXNlckFncmVlbWVudFVybCI6Imh0dHA6Ly9leGFtcGxlLmNvbS90b3MiLCJiYXNlVXJsIjoiaHR0cHM6Ly9hc3NldHMuYnJhaW50cmVlZ2F0ZXdheS5jb20iLCJhc3NldHNVcmwiOiJodHRwczovL2NoZWNrb3V0LnBheXBhbC5jb20iLCJkaXJlY3RCYXNlVXJsIjpudWxsLCJhbGxvd0h0dHAiOnRydWUsImVudmlyb25tZW50Tm9OZXR3b3JrIjp0cnVlLCJlbnZpcm9ubWVudCI6Im9mZmxpbmUiLCJ1bnZldHRlZE1lcmNoYW50IjpmYWxzZSwiYnJhaW50cmVlQ2xpZW50SWQiOiJtYXN0ZXJjbGllbnQzIiwiYmlsbGluZ0FncmVlbWVudHNFbmFibGVkIjp0cnVlLCJtZXJjaGFudEFjY291bnRJZCI6ImFjbWV3aWRnZXRzbHRkc2FuZGJveCIsImN1cnJlbmN5SXNvQ29kZSI6IlVTRCJ9LCJtZXJjaGFudElkIjoiMzQ4cGs5Y2dmM2JneXcyYiIsInZlbm1vIjoib2ZmIn0=");
//            startActivityForResult(dropInRequest.getIntent(this), 10001)
        }

        selectLocationsContainer = findViewById<RelativeLayout>(R.id.selectLocationsContainer) as RelativeLayout
        confirmServiceContainer = findViewById<RelativeLayout>(R.id.confirmServiceContainer) as RelativeLayout

        selectLocation = findViewById<ImageView>(R.id.selectLocation) as ImageView

        mapContainer = findViewById<LinearLayout>(R.id.mapContainer) as LinearLayout
        predictionContainer = findViewById<LinearLayout>(R.id.predictionsContainer) as LinearLayout

        txtOrigen = findViewById<EditText>(R.id.txtOrigen) as EditText
        txtOrigen!!.addTextChangedListener(object: TextWatcher{
            override fun afterTextChanged(s: Editable?) {
                try{
                    Log.v("TextOrigen", "Query Origen:"+s.toString())
                    autocompleteDirections(s!!.toString(),ORIGEN)
                }catch (e:Exception){
                    Toast.makeText(this@BuscarRutaActivity,"Exception: "+e.toString(),Toast.LENGTH_LONG).show()
                    Log.w("Exception",e.toString())
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        })
        txtOrigen!!.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus){
                txtOrigen!!.setText("")
                fillAndResetAutocomplete(ArrayList<Prediction>(),ORIGEN)
                mapContainer!!.visibility = View.GONE
                expand(predictionContainer!!)
//                predictionContainer!!.visibility = View.VISIBLE
            }
        }
        txtOrigen!!.setOnClickListener {
            mapContainer!!.visibility = View.GONE
            expand(predictionContainer!!)
            if (isCustomLocation){
                isCustomLocation = false
                txtDestino!!.setText("")
            }
            if (mMap!=null){
                mMap!!.setOnCameraIdleListener(null)
                isMapSelection = false
            }
            selectLocation!!.visibility = View.GONE
//            predictionContainer!!.visibility = View.VISIBLE
        }

        txtDestino = findViewById<EditText>(R.id.txtDestino) as EditText
        txtDestino!!.addTextChangedListener(object: TextWatcher{
            override fun afterTextChanged(s: Editable?) {
                try{
                    Log.v("TextDestino", "Query Destino:"+s.toString())
                    autocompleteDirections(s!!.toString(),DESTINO)
                }catch (e:Exception){
                    Toast.makeText(this@BuscarRutaActivity,"Exception: "+e.toString(),Toast.LENGTH_LONG).show()
                    Log.w("Exception",e.toString())
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        })
        txtDestino!!.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus){
                txtDestino!!.setText("")
                fillAndResetAutocomplete(ArrayList<Prediction>(),DESTINO)
                mapContainer!!.visibility = View.GONE
                expand(predictionContainer!!)
//                predictionContainer!!.visibility = View.VISIBLE
            }
        }
        txtDestino!!.setOnClickListener {
            mapContainer!!.visibility = View.GONE
            expand(predictionContainer!!)
            if (isCustomLocation){
                isCustomLocation = false
                txtDestino!!.setText("")
            }
            if (mMap!=null){
                mMap!!.setOnCameraIdleListener(null)
                isMapSelection = false
            }
            selectLocation!!.visibility = View.GONE
//            predictionContainer!!.visibility = View.VISIBLE
        }

        btnFinish = findViewById<Button>(R.id.btnFinish) as Button
        btnFinish!!.setOnClickListener {
            mMap!!.clear()
            myLocationMarker = null
//            reAddMyLocationMarker()
            val placesDirectionsApi = PlacesDirectionsApi(this@BuscarRutaActivity)
            placesDirectionsApi.getPlaceDirections(origen, destino)
        }

        if (mGoogleApiClient == null) {
            mGoogleApiClient = GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build()
            mGoogleApiClient!!.connect()
        }

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        try{
            mapFragment = supportFragmentManager!!
                    .findFragmentById(R.id.mapView) as SupportMapFragment
            mapFragment!!.getMapAsync(this)
        }catch (e:Exception){
            Toast.makeText(this@BuscarRutaActivity,"Exception: "+e.toString(),Toast.LENGTH_LONG).show()
            Log.w("Exception",e.toString())
        }
    }

    fun animateMarker(marker: Marker, toPosition: LatLng,
                      hideMarker: Boolean) {
        val handler = Handler()
        val start = SystemClock.uptimeMillis()
        val proj = mMap!!.getProjection()
        val startPoint = proj.toScreenLocation(marker.position)
        val startLatLng = proj.fromScreenLocation(startPoint)
        val duration: Long = 500


        val interpolator = LinearInterpolator()

        handler.post(object : Runnable {
            override fun run() {
                val elapsed = SystemClock.uptimeMillis() - start
                val t = interpolator.getInterpolation(elapsed.toFloat() / duration)
                val lng = t * toPosition.longitude + (1 - t) * startLatLng.longitude
                val lat = t * toPosition.latitude + (1 - t) * startLatLng.latitude
                val newPosition = LatLng(lat, lng)
                marker.setPosition(newPosition)
                polyline.add(newPosition)
                polyline.width(10f)
                polyline.color(Color.BLUE)
                mMap!!.addPolyline(polyline)

                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16)
                } else {
                    if (hideMarker) {
                        marker.isVisible = false
                    } else {
                        marker.isVisible = true
                    }
                }
            }
        })
    }

    fun trazarRuta(directions: PlacesDirections) {
        try{
            mMap!!.setOnCameraIdleListener(null)
            isMapSelection = false
            Log.v("MapReady", "Trazando Ruta")
            Log.v("MapReady", "Cambiando Vistas")
            collapse(selectLocationsContainer!!)
            collapse(predictionContainer!!)
            selectLocation!!.visibility = View.GONE
            btnFinish!!.visibility = View.GONE
//            predictionContainer!!.visibility = View.GONE
            mapContainer!!.visibility = View.VISIBLE
            var duration = -1


            expand(confirmServiceContainer!!)
            val points = ArrayList<LatLng>()
            var lineOptions: PolylineOptions? = null
            val markerOptions = MarkerOptions()
            var routes: List<Route> = ArrayList<Route>()
            var legs: List<Leg> = ArrayList<Leg>()
            var steps: List<Step> = ArrayList<Step>()

            if (directions.getRoutes().isNotEmpty()) {
                routes = directions.getRoutes()
                if (routes[0].getLegs() != null && routes[0].getLegs().isNotEmpty()) {
                    legs = routes[0].getLegs()
                    for (item in legs) {
                        try{
                            var getDuration = item!!.duration.text.split(" ")
                            duration = getDuration[0].toInt()
                        }catch (e:Exception){
                            Log.e("DurationException",e.toString())
                        }

                        if (item.getSteps() != null && item.getSteps().isNotEmpty()) {
                            steps = item.getSteps()
                            for (item2 in steps) {
                                Log.v("DirectionPolyline", item2.getPolyline().getPoints())
                                lineOptions = PolylineOptions()
                                val latInfo = item2.getStartLocation().getLat().split(",")
                                val lat = java.lang.Double.parseDouble(latInfo[0])
                                val lng = java.lang.Double.parseDouble(item2.getStartLocation().getLng())
                                val position = LatLng(lat, lng)
                                val paths = decodePoly(item2.getPolyline().getPoints())
                                points.addAll(paths)
                            }
                        }
                    }
                }
                // Adding all the points in the route to LineOptions
                lineOptions!!.addAll(points)
                lineOptions.width(10f)
                lineOptions.color(R.color.colorPrimary)

                // Drawing polyline in the Google Map for the i-th route
                var destinyPosition: List<String>? = null
                if (destino != "")
                    destinyPosition = destino!!.split(",")
                if (destinyPosition != null) {
//                    val destinoLatLng = LatLng(java.lang.Double.parseDouble(destinyPosition[0]), java.lang.Double.parseDouble(destinyPosition[1]))
                    val origenLatLng = LatLng(points[0].latitude, points[0].longitude)
                    val destinoLatLng = LatLng(points[points.size-1].latitude, points[points.size-1].longitude)

                    val iconOrigen = BitmapFactory.decodeResource(this@BuscarRutaActivity.getResources(),
                            R.mipmap.origen_24)
                    val iconDestino = BitmapFactory.decodeResource(this@BuscarRutaActivity.getResources(),
                            R.mipmap.destino_32)

                    mMap!!.addPolyline(lineOptions)
                    marker = mMap!!.addMarker(MarkerOptions()
                            .position(destinoLatLng)
                            .icon(BitmapDescriptorFactory.fromBitmap(iconDestino))
                            .flat(true))

                    mMap!!.addMarker(MarkerOptions()
                            .position(origenLatLng)
                            .icon(BitmapDescriptorFactory.fromBitmap(iconOrigen))
                            .infoWindowAnchor(0F,0F)
                            .flat(true)).showInfoWindow()
                    val builder = LatLngBounds.Builder()
                    for (latLng in points) {
                        builder.include(latLng)
                    }
                    val bounds = builder.build()
                    //BOUND_PADDING is an int to specify padding of bound.. try 100.
                    val width = getResources().getDisplayMetrics().widthPixels
                    val height = (getResources().getDisplayMetrics().heightPixels * 0.75).toInt()
                    val minMetric = Math.min(width, height)
                    val padding = (minMetric * 0.30).toInt()
                    Log.v("BoundPadding","Width: "+width.toString())
                    Log.v("BoundPadding","Height: "+height.toString())
                    Log.v("BoundPadding","MinMetric: "+padding.toString())
                    Log.v("BoundPadding","Padding: "+padding.toString())
                    val cu = CameraUpdateFactory.newLatLngBounds(bounds,width,height, padding)
                    mMap!!.animateCamera(cu)
                }

            }
        }catch (e:Exception){
            Toast.makeText(this@BuscarRutaActivity,"Exception: "+e.toString(),Toast.LENGTH_LONG).show()
        }
    }

    private fun decodePoly(encoded: String): List<LatLng> {

        val poly = ArrayList<LatLng>()
        var index = 0
        val len = encoded.length
        var lat = 0
        var lng = 0

        while (index < len) {
            var b: Int
            var shift = 0
            var result = 0
            do {
                b = encoded[index++].toInt() - 63
                result = result or (b and 0x1f shl shift)
                shift += 5
            } while (b >= 0x20)
            val dlat = if (result and 1 != 0) (result shr 1).inv() else result shr 1
            lat += dlat

            shift = 0
            result = 0
            do {
                b = encoded[index++].toInt() - 63
                result = result or (b and 0x1f shl shift)
                shift += 5
            } while (b >= 0x20)
            val dlng = if (result and 1 != 0) (result shr 1).inv() else result shr 1
            lng += dlng

            val p = LatLng(lat.toDouble() / 1E5,
                    lng.toDouble() / 1E5)
            poly.add(p)
        }
        return poly
    }

    private fun getDistance(lat1: Double, lon1: Double, lat2: Double, lon2: Double): Double {
        //double earthRadius = 3958.75;//miles
        val earthRadius = 6371.0//kilometers
        //        double earthRadius = 6371000;//metros
        val dLat = Math.toRadians(lat2 - lat1)
        val dLng = Math.toRadians(lon2 - lon1)
        val sindLat = Math.sin(dLat / 2)
        val sindLng = Math.sin(dLng / 2)
        val a = Math.pow(sindLat, 2.0) + Math.pow(sindLng, 2.0) *
                Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
        val c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))
        return earthRadius * c
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onStop() {
        super.onStop()
    }

    override fun onDestroy() {
        super.onDestroy()
        try{
            mGoogleApiClient!!.disconnect()
        }catch (e:Exception){
            Toast.makeText(this@BuscarRutaActivity,"Exception: "+e.toString(),Toast.LENGTH_LONG).show()
        }
    }

    @SuppressLint("MissingPermission")
    override fun onMapReady(map: GoogleMap?) {
        try{
            Log.v("MapReady","Map is ready")
            mMap = map
            mMap!!.uiSettings.isCompassEnabled = false
            mMap!!.isMyLocationEnabled = true
            var locationButton = ( mapFragment!!.view!!.findViewById<View>(Integer.parseInt("1")).getParent() as View).findViewById<ImageView>(Integer.parseInt("2"))
//        locationButton.setBackgroundResource(R.drawable.circle_bg_black_border)
//        locationButton.setPadding(3,3,3,3)
            var rlp =  locationButton!!.getLayoutParams() as RelativeLayout.LayoutParams

            // position on right bottom
            rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0)
            rlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE)
            rlp.setMargins(0, 0, 30, 30)
            mMap!!.setOnMyLocationButtonClickListener(object: GoogleMap.OnMyLocationButtonClickListener{
                override fun onMyLocationButtonClick(): Boolean {

                    return false
                }
            })
        }catch (e:Exception){
            Toast.makeText(this@BuscarRutaActivity,"Exception: "+e.toString(),Toast.LENGTH_LONG).show()
        }

    }

    fun getLocation() {
        try{
            txtOrigen!!.setText("Ubicacion actual")
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return
            }
            val mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                    mGoogleApiClient)
            if (mLastLocation != null) {
                val newLocation: LatLng? = LatLng(mLastLocation.latitude,mLastLocation.longitude)
                val placeDetailsApi = PlaceGeocodeApi(this@BuscarRutaActivity, -1)
                placeDetailsApi.getPlaceGeocode(newLocation)
                myLocation = mLastLocation
                currentLocation = mLastLocation
                origen = currentLocation!!.latitude.toString()+","+currentLocation!!.longitude.toString()
                Log.v("GooglePlacesApi", mLastLocation.toString())

                val icon = BitmapFactory.decodeResource(this@BuscarRutaActivity.getResources(),
                        R.mipmap.pin_user_24)
//                myLocationMarker = mMap!!.addMarker(MarkerOptions()
//                            .position(newLocation!!)
//                            .icon(BitmapDescriptorFactory.fromBitmap(icon))
//                            .flat(true))
                mMap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(newLocation, 16.0f))
            }else{
                Log.v("GooglePlacesApi", "Location Is Null")
            }
        }catch (e:Exception){
            Toast.makeText(this@BuscarRutaActivity,"Exception: "+e.toString(),Toast.LENGTH_LONG).show()
        }
    }

    fun setMyLocationGeocode(locationText:String?){
        if (locationText!=null){
            origenText = locationText
        }
    }

    fun reAddMyLocationMarker(){
        if (myLocationMarker==null){
            val newLocation: LatLng? = LatLng(myLocation!!.latitude,myLocation!!.longitude)
            val icon = BitmapFactory.decodeResource(this@BuscarRutaActivity.getResources(),
                    R.mipmap.pin_user_24)
            myLocationMarker = mMap!!.addMarker(MarkerOptions()
                    .position(newLocation!!)
                    .icon(BitmapDescriptorFactory.fromBitmap(icon))
                    .flat(true))
        }
    }

    override fun onLocationChanged(location: Location?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onProviderEnabled(provider: String?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onProviderDisabled(provider: String?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onConnected(p0: Bundle?) {
        Log.v("GooglePlacesApi", "Connected")
        try {
            getLocation()
        }catch (e:Exception){
            Toast.makeText(this@BuscarRutaActivity,"Exception: "+e.toString(),Toast.LENGTH_LONG).show()
        }
    }

    override fun onConnectionSuspended(p0: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onConnectionFailed(p0: ConnectionResult) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    fun fillAndResetAutocomplete(predictions:MutableList<Prediction>,tipo:Int){
        try{
            var selecteLoc = Prediction()

            selecteLoc.description = "SelectLocation"
            selecteLoc.id = "-100"
            selecteLoc.structuredFormatting.mainText = "Seleccionar ubicacion"
            predictions.add(selecteLoc)

            recycler!!.adapter = null
            var adapter = AutocompleteAdapter(predictions,this,tipo)
            val lManager : LinearLayoutManager? = LinearLayoutManager(this)
            recycler!!.layoutManager = lManager
//        val dividerItemDecoration = DividerItemDecoration(recycler!!.getContext(),
//                lManager!!.getOrientation())
//        recycler!!.addItemDecoration(dividerItemDecoration)
            recycler!!.adapter = adapter
            Log.v("AutoComplete", "RecyclerView  re-inited")
        }catch (e:Exception){
            Toast.makeText(this@BuscarRutaActivity,"Exception: "+e.toString(),Toast.LENGTH_LONG).show()
            Log.w("Exception",e.toString())
        }
        Log.v("AutoComplete", "Re-init RecyclerView")

    }

    fun autocompleteDirections(txt:String?,tipo:Int){
        try{
            if (currentLocation!=null){
                Log.v("AutoComplete", "Autocomplete init: "+txt+", Location: "+currentLocation.toString())
                mPlaceAPI.autocompleteRetrofit(txt, currentLocation,this,tipo)
            }else{
                Log.v("AutoComplete", "Current location is null")
            }
        }catch (e:Exception){
            Toast.makeText(this@BuscarRutaActivity,"Exception: "+e.toString(),Toast.LENGTH_LONG).show()
            Log.w("Exception",e.toString())
        }

    }

    fun retrieveAutocomplete(lstPredictions:MutableList<Prediction>,tipo:Int){
        try{
            if (lstPredictions.size > 0) {
                fillAndResetAutocomplete(lstPredictions,tipo)
            }else{
                Log.v("AutoComplete", "Predictions vacias")
            }
        }catch (e:Exception){
            Toast.makeText(this@BuscarRutaActivity,"Exception: "+e.toString(),Toast.LENGTH_LONG).show()
            Log.w("Exception",e.toString())
        }

    }

    fun getLocationData(tipo:Int,prediction:Prediction){
        try{
            when (tipo){
                ORIGEN -> {
                    txtOrigen!!.setText(prediction.structuredFormatting.mainText)
                    origenText = prediction.structuredFormatting.mainText
                    hideKeyboard(txtOrigen!!)
                }
                DESTINO -> {
                    txtDestino!!.setText(prediction.structuredFormatting.mainText)
                    destinoText = prediction.structuredFormatting.mainText
                    hideKeyboard(txtDestino!!)
                }
            }
            Log.v("PlaceId", prediction.getPlaceId())
            Log.v("PlaceName", prediction.structuredFormatting.mainText)
            val placeDetailsApi = PlaceDetailsApi(this@BuscarRutaActivity, tipo)
            placeDetailsApi.getPlaceDetails(prediction.getPlaceId())
        }catch (e:Exception){
            Toast.makeText(this@BuscarRutaActivity,"Exception: "+e.toString(),Toast.LENGTH_LONG).show()
        }

    }

    fun setLocation(result: Result?, estado: Int) {
        try{
            val direccion = ""
            when (estado) {
                ORIGEN -> if (result != null) {
                    Log.v("MapPlaceLat", result!!.getGeometry().getLocation().getLat().toString())
                    Log.v("MapPlaceLng", result!!.getGeometry().getLocation().getLng().toString())
                    val lat = result!!.getGeometry().getLocation().getLat()
                    val lng = result!!.getGeometry().getLocation().getLng()

                    val location = Location(android.location.LocationManager.GPS_PROVIDER)
                    location.latitude = lat.toDouble()
                    location.longitude = lng.toDouble()
                    Log.v("CurrentLocation", lat.toString() + "," + lng.toString())
                    if (location != null) {
                        currentLocation = location
                        origen = location.latitude.toString()+","+location.longitude.toString()
                    }
                    val newPosition = LatLng(lat.toDouble(), lng.toDouble())
//                origen = lat.toString() + "," + lng.toString()
//                mapPin.setVisibility(View.VISIBLE)
                    if (destino!=null){
                        mMap!!.clear()
                        myLocationMarker = null
//                        reAddMyLocationMarker()
                        val placesDirectionsApi = PlacesDirectionsApi(this@BuscarRutaActivity)
                        placesDirectionsApi.getPlaceDirections(origen, destino)
                    }else{
                        mMap!!.clear()
                        myLocationMarker = null
//                        reAddMyLocationMarker()
                        mMap!!.addMarker(MarkerOptions().position(newPosition).title(direccion))
                        mMap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(newPosition, 16.0f))
                    }
                }
                DESTINO -> if (result!=null){
                    Log.v("MapPlaceLat", result!!.getGeometry().getLocation().getLat().toString())
                    Log.v("MapPlaceLng", result!!.getGeometry().getLocation().getLng().toString())
                    val lat = result!!.getGeometry().getLocation().getLat()
                    val lng = result!!.getGeometry().getLocation().getLng()

                    val location = Location(android.location.LocationManager.GPS_PROVIDER)
                    location.latitude = lat.toDouble()
                    location.longitude = lng.toDouble()
                    Log.v("CurrentLocation", lat.toString() + "," + lng.toString())
                    if (location != null) {
                        destinyLocation = location
                        destino = location.latitude.toString()+","+location.longitude.toString()
                    }
                    val newPosition = LatLng(lat.toDouble(), lng.toDouble())
//                origen = lat.toString() + "," + lng.toString()
//                mapPin.setVisibility(View.VISIBLE)
                    if (origen!=null){
                        mMap!!.clear()
                        myLocationMarker = null
//                        reAddMyLocationMarker()
                        val placesDirectionsApi = PlacesDirectionsApi(this@BuscarRutaActivity)
                        placesDirectionsApi.getPlaceDirections(origen, destino)
                    }else{
                        mMap!!.clear()
                        myLocationMarker = null
//                        reAddMyLocationMarker()
                        mMap!!.addMarker(MarkerOptions().position(newPosition).title(direccion))
                        mMap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(newPosition, 16.0f))
                    }

                }
            }
        }catch (e:Exception){
            Toast.makeText(this@BuscarRutaActivity,"Exception: "+e.toString(),Toast.LENGTH_LONG).show()
        }
    }

    fun expand(v: View) {
        when (v.id){
            R.id.confirmServiceContainer -> {
//                val display = getWindowManager().getDefaultDisplay()
//                val size =  Point()
//                display.getSize(size);
//                int width = size.x;
//                int height = size.y;
                isConfirmServiceExpanded = true
                v.measure(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                val targetHeight = v.height/2

                // Older versions of android (pre API 21) cancel animations for views with a height of 0.
                v.layoutParams.height = 1
                v.visibility = View.VISIBLE
                val a = object : Animation() {
                    override fun applyTransformation(interpolatedTime: Float, t: Transformation) {
                        v.layoutParams.height = if (interpolatedTime == 1f)
                            LinearLayout.LayoutParams.WRAP_CONTENT
                        else
                            (targetHeight * interpolatedTime).toInt()
                        v.requestLayout()
                    }

                    override fun willChangeBounds(): Boolean {
                        return true
                    }
                }

                // 1dp/ms
                a.duration = (targetHeight / v.context.resources.displayMetrics.density).toInt().toLong()
                v.startAnimation(a)
            }
            R.id.selectLocationsContainer -> {
                isLocationContainerExpanded = true
                v.measure(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
                val targetHeight = v.measuredHeight

                // Older versions of android (pre API 21) cancel animations for views with a height of 0.
                v.layoutParams.height = 1
                v.visibility = View.VISIBLE
                val a = object : Animation() {
                    override fun applyTransformation(interpolatedTime: Float, t: Transformation) {
                        v.layoutParams.height = if (interpolatedTime == 1f)
                            LinearLayout.LayoutParams.WRAP_CONTENT
                        else
                            (targetHeight * interpolatedTime).toInt()
                        v.requestLayout()
                    }

                    override fun willChangeBounds(): Boolean {
                        return true
                    }
                }

                // 1dp/ms
                a.duration = (targetHeight / v.context.resources.displayMetrics.density).toInt().toLong()
                v.startAnimation(a)
            }
            R.id.predictionsContainer -> {
                isPredictionContainerExpanded = true
                v.measure(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
                val targetHeight = v.measuredHeight

                // Older versions of android (pre API 21) cancel animations for views with a height of 0.
                v.layoutParams.height = 1
                v.visibility = View.VISIBLE
                val a = object : Animation() {
                    override fun applyTransformation(interpolatedTime: Float, t: Transformation) {
                        v.layoutParams.height = if (interpolatedTime == 1f)
                            LinearLayout.LayoutParams.MATCH_PARENT
                        else
                            (targetHeight * interpolatedTime).toInt()
                        v.requestLayout()
                    }

                    override fun willChangeBounds(): Boolean {
                        return true
                    }
                }

                // 1dp/ms
                a.duration = (targetHeight / v.context.resources.displayMetrics.density).toInt().toLong()
                v.startAnimation(a)
            }
        }
    }

    fun collapse(v: View) {
        when (v.id){
            R.id.confirmServiceContainer -> {
                isConfirmServiceExpanded = false
            }
            R.id.selectLocationsContainer -> {
                isLocationContainerExpanded = false
            }
            R.id.predictionsContainer -> {
                isPredictionContainerExpanded = false
            }
        }
        val initialHeight = v.measuredHeight

        val a = object : Animation() {
            override fun applyTransformation(interpolatedTime: Float, t: Transformation) {
                if (interpolatedTime == 1f) {
                    v.visibility = View.GONE
                } else {
                    v.layoutParams.height = initialHeight - (initialHeight * interpolatedTime).toInt()
                    v.requestLayout()
                }
            }

            override fun willChangeBounds(): Boolean {
                return true
            }
        }

        // 1dp/ms
        a.duration = (initialHeight / v.context.resources.displayMetrics.density).toInt().toLong()
        v.startAnimation(a)
    }

    fun getPXFromPD(DP: Int): Int {
        val scale = resources.displayMetrics.density
        return (DP * scale + 0.5f).toInt()
    }

    fun getDPFromPX(PX: Int): Int {
        val scale = resources.displayMetrics.density
        return (PX * scale).toInt()
    }

    fun hideKeyboard(view:View){
        view!!.clearFocus()
        val imm = this.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0)
    }

    fun selectLocationInMap(type:Int){
        if (mMap!=null){
            isMapSelection = true
            currentType = type
            mMap!!.clear()
            myLocationMarker = null
//            reAddMyLocationMarker()
            mapContainer!!.visibility = View.VISIBLE
            collapse(predictionContainer!!)
            selectLocation!!.visibility = View.VISIBLE
            when(type){
                ORIGEN -> {
                    hideKeyboard(txtOrigen!!)
                    selectLocation!!.setImageResource(R.mipmap.pin_origen_128)
                    if (currentLocation!=null){
                        var latLng = LatLng(currentLocation!!.latitude,currentLocation!!.longitude)
                        mMap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16.0f))
                    }
                }
                DESTINO -> {
                    hideKeyboard(txtDestino!!)
                    selectLocation!!.setImageResource(R.mipmap.pin_desitno_128)
                    var latLng = LatLng(currentLocation!!.latitude,currentLocation!!.longitude)
                    mMap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16.0f))
                }
            }
        }
    }

    fun movingMap(){
        btnFinish!!.visibility = View.GONE
    }

    fun getLocationFromMap(){
        if (isMapSelection){
            btnFinish!!.visibility = View.GONE
            currentZoom = mMap!!.getCameraPosition().zoom
            isCustomLocation = true
            val midLatLng = mMap!!.getCameraPosition().target
            when(currentType){
                ORIGEN -> {
                    currentLocation = null
                    txtOrigen!!.setText("Cargando...")
                    val placeDetailsApi = PlaceGeocodeApi(this@BuscarRutaActivity, ORIGEN)
                    placeDetailsApi.getPlaceGeocode(midLatLng)
                }
                DESTINO -> {
                    destinyLocation = null
                    txtDestino!!.setText("Cargando...")
                    val placeDetailsApi = PlaceGeocodeApi(this@BuscarRutaActivity, DESTINO)
                    placeDetailsApi.getPlaceGeocode(midLatLng)
                }
            }
        }
    }

    fun setLocationDetails(result: com.aarr.vip365.services.PlaceGeocode.Result, estado: Int) {
        var direccion: String? = ""
        when (estado) {
            ORIGEN ->
                if (result != null) {
                    isCustomLocation = true
                    Log.v("MapPlaceLat", result!!.getGeometry().getLocation().getLat().toString())
                    Log.v("MapPlaceLng", result!!.getGeometry().getLocation().getLng().toString())
                    val lat = result!!.getGeometry().getLocation().getLat()
                    val lng = result!!.getGeometry().getLocation().getLng()
                    direccion = result!!.getFormattedAddress()
                    try{
                        for (item in result!!.addressComponents){
                            for (item2 in item.types){
                                if (item2.equals("route")){
                                    direccion = item.longName
                                    Log.v("Direction","Long: "+item.longName)
                                    Log.v("Direction","Short: "+item.shortName)
                                }
                            }
                        }
                    }catch (e:Exception){
                        Log.w("DirectionException",e.toString())
                    }

                    if (direccion != null){
                        txtOrigen!!.setText(direccion)
                        origenText = direccion
                    }
                    val location = Location(android.location.LocationManager.GPS_PROVIDER)
                    location.latitude = lat.toDouble()
                    location.longitude = lng.toDouble()
                    Log.v("CurrentLocation", lat.toString() + "," + lng.toString())
                    if (location != null) {
                        currentLocation = location
                        origen = currentLocation!!.latitude.toString()+","+currentLocation!!.longitude.toString()
                    }
                    if (currentLocation!=null && destinyLocation!=null){
                        btnFinish!!.visibility = View.VISIBLE
                    }
                }
            DESTINO ->
                if (result != null) {
                    isCustomLocation = true
                    Log.v("MapPlaceLat", result!!.getGeometry().getLocation().getLat().toString())
                    Log.v("MapPlaceLng", result!!.getGeometry().getLocation().getLng().toString())
                    val lat = result!!.getGeometry().getLocation().getLat()
                    val lng = result!!.getGeometry().getLocation().getLng()
                    direccion = result!!.getFormattedAddress()
                    try{
                        for (item in result!!.addressComponents){
                            for (item2 in item.types){
                                if (item2.equals("route")){
                                    direccion = item.longName
                                    Log.v("Direction","Long: "+item.longName)
                                    Log.v("Direction","Short: "+item.shortName)
                                }
                            }
                        }
                    }catch (e:Exception){
                        Log.w("DirectionException",e.toString())
                    }
                    if (direccion != null){
                        txtDestino!!.setText(direccion)
                        destinoText = direccion
                    }
                    val location = Location(android.location.LocationManager.GPS_PROVIDER)
                    location.latitude = lat.toDouble()
                    location.longitude = lng.toDouble()
                    Log.v("CurrentLocation", lat.toString() + "," + lng.toString())
                    if (location != null) {
                        destinyLocation = location
                        destino = location.latitude.toString()+","+location.longitude.toString()
                    }

                    if (currentLocation!=null && destinyLocation!=null){
                        btnFinish!!.visibility = View.VISIBLE
                    }
                }
        }
    }

    fun roundBit(bm:Bitmap): Bitmap {

        var circleBitmap = Bitmap.createBitmap(bm.getWidth(),
                            bm.getHeight(), Bitmap.Config.ARGB_8888)

        var shader = BitmapShader(bm, Shader.TileMode.CLAMP,
                Shader.TileMode.CLAMP)
        var paint =  Paint()
        paint.setShader(shader)
        paint.setAntiAlias(true)
        var c = Canvas(circleBitmap)
        c.drawCircle((bm.getWidth() / 2).toFloat(), (bm.getHeight() / 2).toFloat(), (bm.getWidth() / 2).toFloat(),
                paint)

        return circleBitmap
    }

    fun startRouteAnim(route:List<LatLng>){
        if (mMap!=null){
            MapAnimator().getInstance().animateRoute(mMap!!,route)
        }else{
            Toast.makeText(getApplicationContext(), "El mapa no esta listo", Toast.LENGTH_LONG).show();
        }
    }

    override fun onTouch(v: View?, event: MotionEvent?): Boolean {
        when(event!!.action){
            MotionEvent.ACTION_UP -> {
                touchState = RELEASED
            }
            MotionEvent.ACTION_DOWN -> {
                touchState = PRESSED
            }
        }
        return true
    }

    fun showPaymentsDialog(){
        dialog = Dialog(this)
        dialog!!.setContentView(R.layout.payment_methods_list)

        var recyclerDialog = dialog!!.findViewById<RecyclerView>(R.id.recycler)

        var adapter = PaymentAdapter(PaymentsMethodsDao().findAll(),this)
        val lManager : LinearLayoutManager? = LinearLayoutManager(this)
        recyclerDialog.layoutManager = lManager
        val dividerItemDecoration = DividerItemDecoration(recycler!!.getContext(),
                lManager!!.getOrientation())
        recyclerDialog.addItemDecoration(dividerItemDecoration)
        recyclerDialog.adapter = adapter

        dialog!!.show()
    }

    fun selectPaymentMethod(item:PaymentMethods){
        if (item!=null){
            currentPaymentMethod = item
            when(currentPaymentMethod!!.paymentType){
                Configuration().PAYMENT_CREDIT_DEBIT_CARD ->{
                    nombrePago!!.setText(getMaskedNumber(currentPaymentMethod!!.cardNumber!!))
                    when (currentPaymentMethod!!.cardType) {
                        "Visa" -> tipoPago!!.setImageResource(R.mipmap.visa_64)
                        "American Express" -> tipoPago!!.setImageResource(R.mipmap.amex_64)
                        "Master" -> tipoPago!!.setImageResource(R.mipmap.mastercard_64)
                        "Discover" -> tipoPago!!.setImageResource(R.mipmap.discover_64)
                        "JCB" -> tipoPago!!.setImageResource(R.mipmap.jcb_64)
                        "Maestro" -> tipoPago!!.setImageResource(R.mipmap.maestro_64)
                    }
                }
                Configuration().PAYMENT_PAYPAL ->{
                    nombrePago!!.setText("PayPal")
                    tipoPago!!.setImageResource(R.mipmap.paypal_64)
                }
                Configuration().PAYMENT_CASH ->{
                    nombrePago!!.setText("Efectivo")
                    tipoPago!!.setImageResource(R.mipmap.cash_64)
                }
            }
        }
        if (dialog!=null){
            dialog!!.dismiss()
        }
    }

    fun getMaskedNumber(cardNumber:String):String{
        var lastFour = cardNumber.substring(cardNumber.length-4,cardNumber.length)
        val cardValue = "**** "+lastFour
        return cardValue
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
//        if (requestCode == 10001) {
//            if (resultCode == Activity.RESULT_OK) {
//              var result = data?.getParcelableExtra<DropInResult>(DropInResult.EXTRA_DROP_IN_RESULT)
//                Log.v("PaymentResult",result.toString())
//              // use the result to update your UI and send the payment method nonce to your server
//            } else if (resultCode == Activity.RESULT_CANCELED) {
//              // the user canceled
//            } else {
//              // handle errors here, an exception may be available in
//              var error = data?.getSerializableExtra(DropInActivity.EXTRA_ERROR) as Exception
//            }
//          }
    }

}
