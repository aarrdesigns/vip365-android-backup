package com.aarr.vip365.views

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.ImageButton
import android.widget.LinearLayout
import android.widget.Toast
import com.aarr.vip365.config.Configuration
import com.aarr.vip365.database.Dao.PaymentsMethodsDao
import com.aarr.vip365.database.Model.PaymentMethods

import com.aarr.vip365.R
import com.aarr.vip365.services.RegisterPaymentService
import com.aarr.vip365.views.Fragments.NewPayment
import com.aarr.vip365.views.Fragments.PaymentData
import com.paypal.android.sdk.payments.*
import xyz.belvi.luhn.interfaces.LuhnCardVerifier
import xyz.belvi.luhn.cardValidator.models.LuhnCard
import xyz.belvi.luhn.interfaces.LuhnCallback
import xyz.belvi.luhn.Luhn
import io.card.payment.CardIOActivity
import java.util.*
import org.json.JSONException


class PagosActivity : AppCompatActivity() {

    var currentCard: LuhnCard? = null
    var btnClose: ImageButton? = null
    var CONFIG_CLIENT_ID = "Ael-JLdC_7OVcuvv2qPCn6cJbgUIERNPiHIfXO2pnINEGsPqMlpfgWrKmlS94cdFMfemBNR6bY1psRmO"
    var CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_SANDBOX
    val REQUEST_CODE_PAYMENT = 1
    val REQUEST_CODE_FUTURE_PAYMENT = 2
    var container : LinearLayout? = null
    var currentFragment = -1
    val FRAGMENT_DATA = 1
    val FRAGMENT_NEW = 2

    var config = PayPalConfiguration()
                        .environment(CONFIG_ENVIRONMENT)
                        .clientId(CONFIG_CLIENT_ID)
                        // the following are only used in PayPalFuturePaymentActivity.
                        .merchantName("Hipster Store")
                        .merchantPrivacyPolicyUri(
                        Uri.parse("https://www.example.com/privacy"))
                        .merchantUserAgreementUri(
                        Uri.parse("https://www.example.com/legal"))

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pagos)

        container = findViewById<LinearLayout>(R.id.container) as LinearLayout

        btnClose = findViewById<ImageButton>(R.id.btnClose) as ImageButton
        btnClose!!.setOnClickListener {
            when (currentFragment){
                FRAGMENT_DATA -> {
                    finish()
                }
                FRAGMENT_NEW -> {
                    returnToMainFragment()
                }
            }
        }

        var intent = Intent(this, PayPalService::class.java)
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config)
        startService(intent)

        loadDataFragment()

    }

    fun returnToMainFragment(){
        btnClose!!.setImageResource(android.R.drawable.ic_menu_close_clear_cancel)
        supportFragmentManager!!.popBackStackImmediate()
        currentFragment = FRAGMENT_DATA
    }

    fun loadDataFragment(){
        val fragment: android.support.v4.app.Fragment
        fragment = PaymentData()

        currentFragment = FRAGMENT_DATA

        val fragmentTransaction = supportFragmentManager!!.beginTransaction()

        fragmentTransaction.replace(R.id.container, fragment)
        fragmentTransaction.addToBackStack("FragmentData")
        fragmentTransaction.commit()
    }

    fun loadNewFragment(){
        val fragment: android.support.v4.app.Fragment
        fragment = NewPayment()
        btnClose!!.setImageResource(R.drawable.abc_ic_ab_back_material)
        currentFragment = FRAGMENT_NEW

        val fragmentTransaction = supportFragmentManager!!.beginTransaction()

        fragmentTransaction.replace(R.id.container, fragment)
        fragmentTransaction.addToBackStack("NewPayment")
        fragmentTransaction.commit()
    }

    fun initCardCreation(){
        val cardIoBundle = Bundle()
        cardIoBundle.putBoolean(CardIOActivity.EXTRA_REQUIRE_EXPIRY, true) // default: false
        cardIoBundle.putBoolean(CardIOActivity.EXTRA_SCAN_EXPIRY, false) // default: false
        cardIoBundle.putBoolean(CardIOActivity.EXTRA_REQUIRE_CVV, true) // default: false
        cardIoBundle.putBoolean(CardIOActivity.EXTRA_REQUIRE_POSTAL_CODE, false) // default: false
        cardIoBundle.putBoolean(CardIOActivity.EXTRA_REQUIRE_CARDHOLDER_NAME, true) // default: false
        cardIoBundle.putBoolean(CardIOActivity.EXTRA_HIDE_CARDIO_LOGO, true) // default: false
        cardIoBundle.putBoolean(CardIOActivity.EXTRA_SUPPRESS_MANUAL_ENTRY, true) // default: false
        cardIoBundle.putBoolean(CardIOActivity.EXTRA_SUPPRESS_SCAN, true) // default: false

        Luhn.startLuhn(this, object : LuhnCallback {
            override fun cardDetailsRetrieved(luhnContext: Context, creditCard: LuhnCard, cardVerifier: LuhnCardVerifier) {
                Log.v("CreditCard","Name: "+creditCard.cardName)
                Log.v("CreditCard","CVV: "+creditCard.cvv)
                Log.v("CreditCard","Exp date: "+creditCard.expDate)
                Log.v("CreditCard","Card number:"+creditCard.pan)
                currentCard = creditCard
                cardVerifier.startProgress()
//                Handler().postDelayed(Runnable { cardVerifier.requestOTP(4) }, 2500)
                Handler().postDelayed(Runnable { cardVerifier.onCardVerified(true, getString(R.string.verification_error), getString(R.string.verification_error)) }, 2500)
            }

            override fun otpRetrieved(luhnContext: Context, cardVerifier: LuhnCardVerifier, otp: String) {
                cardVerifier.startProgress()
                Log.v("CreditCardOTP","OTP Retrieve")
                Handler().postDelayed(Runnable { cardVerifier.onCardVerified(false, getString(R.string.verification_error), getString(R.string.verification_error)) }, 2500)
            }

            override fun onFinished(isVerified: Boolean) {
                Log.v("CreditCard","Finish: "+isVerified.toString())
                saveCreditCard()
            }

        },cardIoBundle, R.style.LuhnStyle)
    }

    fun saveCreditCard(){
        if (currentCard!=null){
            PaymentsMethodsDao().cleanDefaultPayments()
            var newCard = PaymentMethods(-1,-1,Configuration().PAYMENT_CREDIT_DEBIT_CARD,-1,null,currentCard!!.cardName,currentCard!!.pan,currentCard!!.cvv,currentCard!!.expDate, Date(),true,true)
//            PaymentsMethodsDao().updateOrCreate(newCard)
            RegisterPaymentService(this).handlePaymentRegister(newCard)
        }
    }

    fun savePaypal(paypalCode:String){
        PaymentsMethodsDao().cleanDefaultPayments()
        var newCard = PaymentMethods(-1,-1,Configuration().PAYMENT_PAYPAL,-1,paypalCode,null,null,null,null, Date(),true,true)
//        PaymentsMethodsDao().updateOrCreate(newCard)
        RegisterPaymentService(this).handlePaymentRegister(newCard)
        try{
            returnToMainFragment()
        }catch (e:Exception){
            Log.w("Exception",e.toString())
        }

    }

    fun onFuturePaymentPressed() {
        var intent = Intent(this,
                PayPalFuturePaymentActivity::class.java)

        startActivityForResult(intent, REQUEST_CODE_FUTURE_PAYMENT)
    }

    override fun onActivityResult(requestCode:Int, resultCode:Int, data:Intent) {
        if (requestCode == REQUEST_CODE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                var confirm:PaymentConfirmation = data
                        .getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION)
                if (confirm != null) {
                    try {
                        System.out.println(confirm.toJSONObject().toString(4));
                        System.out.println(confirm.getPayment().toJSONObject()
                                .toString(4))

                        Toast.makeText(getApplicationContext(), "Order placed",
                                Toast.LENGTH_LONG).show()

                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                System.out.println("The user canceled.")
            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                System.out
                        .println("An invalid Payment or PayPalConfiguration was submitted. Please see the docs.")
            }
        } else if (requestCode == REQUEST_CODE_FUTURE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                var auth:PayPalAuthorization = data
                        .getParcelableExtra(PayPalFuturePaymentActivity.EXTRA_RESULT_AUTHORIZATION)
                if (auth != null) {
                    try {
                        Log.i("FuturePaymentExample", auth.toJSONObject()
                                .toString(4))

                        var authorization_code = auth.getAuthorizationCode()
                        Log.i("FuturePaymentExample", authorization_code)

                        sendAuthorizationToServer(auth)
                        savePaypal(authorization_code)
//                        Toast.makeText(getApplicationContext(),
//                                "Future Payment code received from PayPal",
//                                Toast.LENGTH_LONG).show()

                    } catch ( e:JSONException) {
                        Log.e("FuturePaymentExample",
                                "an extremely unlikely failure occurred: ", e)
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i("FuturePaymentExample", "The user canceled.")
            } else if (resultCode == PayPalFuturePaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i("FuturePaymentExample",
                        "Probably the attempt to previously start the PayPalService had an invalid PayPalConfiguration. Please see the docs.")
            }
        }
    }

    fun sendAuthorizationToServer(authorization:PayPalAuthorization) {

    }

    fun onFuturePaymentPurchasePressed(pressed:View) {
        // Get the Application Correlation ID from the SDK
        var correlationId = PayPalConfiguration
                .getApplicationCorrelationId(this)

        Log.i("FuturePaymentExample", "Application Correlation ID: "
                + correlationId);

        // TODO: Send correlationId and transaction details to your server for
        // processing with
        // PayPal...
        Toast.makeText(getApplicationContext(),
                "App Correlation ID received from SDK", Toast.LENGTH_LONG)
                .show();
    }



}
