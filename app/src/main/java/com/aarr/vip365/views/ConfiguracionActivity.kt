package com.aarr.vip365.views

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import com.aarr.vip365.database.Dao.UsersDao
import com.aarr.vip365.database.Model.UsersModel

import com.aarr.vip365.R
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import java.io.File
import android.media.ExifInterface
import android.util.Log
import com.facebook.login.LoginManager
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient


class ConfiguracionActivity : AppCompatActivity() {

    var currentUser: UsersModel? = null
    var txtNombre: TextView? = null
    var txtPhoneNumber: TextView? = null
    var txtEmail: TextView? = null
    var imgProfile: ImageView? = null
    var btnClose: ImageView? = null
    var btnSignOut: RelativeLayout? = null
    var profileCard: RelativeLayout? = null
    var mGoogleApiClient: GoogleApiClient? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_configuracion)

        initGoogleClient()
        txtNombre = findViewById<TextView>(R.id.txtNombre) as TextView
        txtPhoneNumber = findViewById<TextView>(R.id.txtPhoneNumber) as TextView
        txtEmail = findViewById<TextView>(R.id.txtEmail) as TextView
        imgProfile = findViewById<ImageView>(R.id.imgProfile) as ImageView
        btnClose = findViewById<ImageView>(R.id.btnClose) as ImageView
        btnClose!!.setOnClickListener {
            finish()
        }
        btnSignOut = findViewById<RelativeLayout>(R.id.btnSignOut) as RelativeLayout
        btnSignOut!!.setOnClickListener {
            var currentUser = UsersDao().getCurrentUser()
            if (currentUser!=null){
                try{
                    when (currentUser.typeUser){
                        1 -> {
                            Log.e("CleanSesion","SignOut Normal User")
                            UsersDao().clearUser()
                        }
                        2 -> {
                            Log.e("CleanSesion","SignOut Google User")
                            UsersDao().clearUser()
                            Auth.GoogleSignInApi.signOut(mGoogleApiClient)
                        }
                        3 -> {
                            Log.e("CleanSesion","SignOut Facebook User")
                            UsersDao().clearUser()
                            LoginManager.getInstance().logOut()
                        }
                    }
                }catch (e:Exception){
                    Log.e("CleanSesionException",e.toString())
                }
                var returnIntent = Intent()
                setResult(Activity.RESULT_OK,returnIntent)
                finish()
            }
        }
        setUserData()
        profileCard = findViewById<RelativeLayout>(R.id.profileCard) as RelativeLayout
        profileCard!!.setOnClickListener {
            var intent = Intent(this,EditUserActivity::class.java)
            startActivity(intent)
        }

    }

    fun initGoogleClient(){
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build()

        mGoogleApiClient = GoogleApiClient.Builder(this)
                .enableAutoManage(this, object: GoogleApiClient.OnConnectionFailedListener{
                    override fun onConnectionFailed(p0: ConnectionResult) {
                        Log.e("GoogleError",p0.errorMessage)
                    }
                } /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build()
    }

    fun setUserData(){
        currentUser = UsersDao().getCurrentUser()
        if (currentUser!=null){
            txtNombre!!.setText(currentUser!!.name+" "+currentUser!!.lastName)
            txtPhoneNumber!!.setText(currentUser!!.phoneNumber)
            txtEmail!!.setText(currentUser!!.email)
            if (currentUser!!.profileImage!=null && !currentUser!!.profileImage.equals("")){

                var imageUri = Uri.parse(File(currentUser!!.profileImage).toString())

                val exif = ExifInterface(imageUri.getPath())
                val rotation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL)
                Log.e("ROTATIONVALUE",rotation.toString())
                val rotationInDegrees = exifToDegrees(rotation)

                val options = RequestOptions()
                options.diskCacheStrategy(DiskCacheStrategy.NONE)
                options.skipMemoryCache(true)
                options.fitCenter()
                options.dontTransform()
                options.dontAnimate()
                Glide
                        .with(this)
                        .load(currentUser!!.profileImage)
                        .apply(options)
                        .into(imgProfile)
            }
        }
    }

    private fun exifToDegrees(exifOrientation: Int): Int {
        if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_90) {
            return 90
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_180) {
            return 180
        } else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_270) {
            return 270
        }
        return 0
    }

    override fun onResume() {
        super.onResume()
        setUserData()
    }
}
