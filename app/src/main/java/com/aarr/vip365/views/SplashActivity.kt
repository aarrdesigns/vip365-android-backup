package com.aarr.vip365.views

import android.support.v7.app.AppCompatActivity
import android.os.Bundle

import android.transition.Explode
import android.os.Build
import android.content.Intent
import android.view.Window
import android.content.Context
import android.content.pm.PackageManager
import android.os.CountDownTimer
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.widget.Toast
import android.net.ConnectivityManager
import android.content.DialogInterface
import android.location.LocationManager
import android.support.v7.app.AlertDialog
import android.util.Log
import com.aarr.vip365.database.Dao.UsersDao
import com.facebook.login.LoginManager
import com.google.android.gms.auth.api.Auth


class SplashActivity : AppCompatActivity() {

//    var intentGPService: Intent? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        window.requestFeature(Window.FEATURE_ACTIVITY_TRANSITIONS)
        super.onCreate(savedInstanceState)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val exitTrans = Explode()
            window.exitTransition = exitTrans
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            permisoUbicacion()
        }else{
            exitSplash()
        }

    }

    fun exitSplash(){
        val manager =  getSystemService( Context.LOCATION_SERVICE ) as LocationManager
        if (!isOnline()){
            buildAlertMessageNoInternet()
        }else if ( !manager.isProviderEnabled( LocationManager.GPS_PROVIDER ) ) {
            buildAlertMessageNoGps()
        }else{
            object: CountDownTimer(2000,1000){
                override fun onTick(millisUntilFinished: Long) {

                }

                override fun onFinish() {
                    if (UsersDao().getCurrentUser()!=null){
                        var intent = Intent(this@SplashActivity,MenuActivity::class.java)
                        startActivity(intent)
                        finish()
                    }else{
                        val intent = Intent(this@SplashActivity, MainActivity::class.java)
                        startActivity(intent)
                        finish()
                    }

                }
            }.start()
        }
    }

    private val MY_PERMISSIONS_REQUEST_FINE_LOCATION = 10000
    private fun permisoUbicacion() {

        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION), MY_PERMISSIONS_REQUEST_FINE_LOCATION)
        } else {
            permisoAlmacenamiento()
        }
    }

//    private val MY_PERMISSIONS_REQUEST_SMS = 10001
//    private fun permisoSMS() {
//        // Iniciamos servicio envío coordenadas GPS, Se activa aqui, para estar seguros de tener el permiso de la ubicación antes de lanzarlo
//        intentGPService = Intent(this, GPSActualizacionUbicacionService::class.java)
//        startService(intentGPService)
//        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED) {
//            ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.SEND_SMS), MY_PERMISSIONS_REQUEST_SMS)
//        } else {
//            permisoTelefono()
//        }
//    }

    private val MY_PERMISSIONS_REQUEST_PHONE = 10002
    private fun permisoTelefono() {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.READ_PHONE_STATE), MY_PERMISSIONS_REQUEST_PHONE)
        } else {
            permisoAudio()
        }
    }

    private val MY_PERMISSIONS_REQUEST_AUDIO = 10003
    private fun permisoAudio() {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.RECORD_AUDIO), MY_PERMISSIONS_REQUEST_AUDIO)
        } else {
            permisoAlmacenamiento()
        }
    }

    private val MY_PERMISSIONS_REQUEST_STORAGE = 10004
    private fun permisoAlmacenamiento() {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.READ_EXTERNAL_STORAGE), MY_PERMISSIONS_REQUEST_STORAGE)
        }else{
            exitSplash()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            MY_PERMISSIONS_REQUEST_FINE_LOCATION -> if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //Se acepto el permiso, se verifica el siguiente
                permisoAlmacenamiento()
                Log.v("Permiso:", "Ubicacion")
            } else {
                Toast.makeText(this@SplashActivity, "No se activó el permiso, la aplicación no podrá ejecutarse", Toast.LENGTH_SHORT).show()
                finish()
            }
//            MY_PERMISSIONS_REQUEST_SMS -> if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                //Se acepto el permiso, se verifica el siguiente
//                permisoTelefono()
//            }
//            else {
//                Toast.makeText(this@SplashActivity, "No se activó el permiso, la aplicación no podrá ejecutarse", Toast.LENGTH_SHORT).show()
//                finish()
//            }
//            MY_PERMISSIONS_REQUEST_PHONE -> if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                //Se acepto el permiso, se verifica el siguiente
//                permisoAudio()
//            } else {
//                Log.v("Permiso:", "TElefono")
//                Toast.makeText(this@SplashActivity, "No se activó el permiso, la aplicación no podrá ejecutarse", Toast.LENGTH_SHORT).show()
//                finish()
//            }
//            MY_PERMISSIONS_REQUEST_AUDIO -> if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                //Se acepto el permiso, se verifica el siguiente
//                permisoAlmacenamiento()
//            } else {
//                Log.v("Permiso:", "Audio")
//                Toast.makeText(this@SplashActivity, "No se activó el permiso, la aplicación no podrá ejecutarse", Toast.LENGTH_SHORT).show()
//                finish()
//            }
            MY_PERMISSIONS_REQUEST_STORAGE -> if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                exitSplash()
            } else {
                Toast.makeText(this@SplashActivity, "No se activó el permiso, la aplicación no podrá ejecutarse", Toast.LENGTH_SHORT).show()
                Log.v("Permiso:", "Almacenamiento")
                finish()
            }
        }
    }

    fun isOnline(): Boolean {
        val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val netInfo = cm.activeNetworkInfo
        return netInfo != null && netInfo.isConnectedOrConnecting
    }

    private fun buildAlertMessageNoGps() {
        val builder = AlertDialog.Builder(this)
        builder.setMessage("GPS desactivado, desea activarlo?")
                .setCancelable(false)
                .setPositiveButton("Si", DialogInterface.OnClickListener { dialog, id ->
                    startActivity(Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS))
                    finish()
                })
                .setNegativeButton("No", DialogInterface.OnClickListener { dialog, id ->
                    dialog.cancel()
                    finish()
                })
        val alert = builder.create()
        alert.show()
    }

    private fun buildAlertMessageNoInternet() {
        val builder = AlertDialog.Builder(this)
        builder.setMessage("Revisa tu conexion a internet")
                .setCancelable(false)
                .setNeutralButton("Salir", DialogInterface.OnClickListener { dialog, id -> finish() })
//                .setPositiveButton("Si", DialogInterface.OnClickListener { dialog, id -> startActivity(Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS)) })
//                .setNegativeButton("No", DialogInterface.OnClickListener { dialog, id ->
//                    dialog.cancel()
//                    finish()
//                })
        val alert = builder.create()
        alert.show()
    }
}
