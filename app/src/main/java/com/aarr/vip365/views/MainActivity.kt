package com.aarr.vip365.views

import android.app.Activity
import android.content.Intent
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.widget.Button
import com.aarr.vip365.R
import android.view.animation.AnticipateOvershootInterpolator
import android.transition.Explode
import android.view.View


class MainActivity : AppCompatActivity() {

    var btnIniciar: Button? = null
    var btnRegistrar: Button? = null
    var REQUEST_CODE = 100

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnIniciar = findViewById<Button>(R.id.btnIniciar) as Button
        btnIniciar!!.setOnClickListener {
            goIniciar()
        }
        btnRegistrar = findViewById<Button>(R.id.btnRegistrar) as Button
        btnRegistrar!!.setOnClickListener {
            goRegistro()
        }

    }

    fun goIniciar(){
        var intent: Intent? = Intent(this, LoginActivity::class.java)
        startActivityForResult(intent,REQUEST_CODE)
    }

    fun goRegistro(){
        var intent: Intent? = Intent(this, RegistroActivity::class.java)
        startActivityForResult(intent,REQUEST_CODE)
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun makeExplodeTransition(): Explode {
        val explode = Explode()
        explode.duration = 3000
        explode.interpolator = AnticipateOvershootInterpolator()
        return explode
    }

    // Custom method to toggle visibility of views
    private fun toggleVisibility(vararg views: View) {
        // Loop through the views
        for (v in views) {
            if (v.getVisibility() === View.VISIBLE) {
                v.setVisibility(View.INVISIBLE)
            } else {
                v.setVisibility(View.VISIBLE)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == REQUEST_CODE){
            if (resultCode == Activity.RESULT_OK){
                finish()
                var intent = Intent(this,MenuActivity::class.java)
                startActivity(intent)
            }
        }
    }
}
