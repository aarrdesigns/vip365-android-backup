package com.aarr.vip365.views

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.telephony.TelephonyManager
import android.util.Log
import android.widget.*

import com.aarr.vip365.R
import com.aarr.vip365.database.Model.UsersModel
import com.aarr.vip365.util.CountryData
import com.aarr.vip365.util.CountryUtils
import com.google.android.gms.auth.api.Auth

class RequestPhoneActivity : AppCompatActivity() {

    var countryFlag: ImageView? = null
    var countryNumberCode: TextView? = null
    var countryDataContainer: LinearLayout? = null
    var countryCode: String? = null
    var REQUEST_COUNTRY = 1000
    var btnSave: Button? = null
    var txtPhoneNumber: EditText? = null
    var currentUser: UsersModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_request_phone)

        if (intent.hasExtra("User")){
            currentUser = intent.getSerializableExtra("User") as UsersModel
        }

        if (currentUser!=null){
            countryFlag = findViewById<ImageView>(R.id.countryFlag) as ImageView
            countryDataContainer = findViewById<LinearLayout>(R.id.countryDataContainer) as LinearLayout
            txtPhoneNumber = findViewById<EditText>(R.id.txtPhoneNumber) as EditText
            countryDataContainer!!.setOnClickListener {
                var intent = Intent(this,CountryListActivity::class.java)
                startActivityForResult(intent,REQUEST_COUNTRY)
            }
            countryNumberCode = findViewById<TextView>(R.id.countryNumberCode) as TextView
            btnSave = findViewById<Button>(R.id.btnSave) as Button
            btnSave!!.setOnClickListener {
                val phoneNumber = countryNumberCode!!.text.toString()+txtPhoneNumber!!.text.toString()
                currentUser!!.phoneNumber = phoneNumber
                currentUser!!.countryCode = countryCode!!
                var returnIntent = Intent()
                returnIntent.putExtra("User",currentUser)
                setResult(Activity.RESULT_OK,returnIntent)
                finish()
            }
        }

        var tm = getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
        countryCode = tm.simCountryIso
        var country = CountryUtils().getCountryByCode(countryCode!!.replace(" ",""))
        if (country!=null){
            countryFlag!!.setImageResource(country!!.imgFlag)
            countryNumberCode!!.setText(country!!.extention)
        }else{
            Log.v("CountryCode","Country data is null")
        }
        Log.v("CountryCode",countryCode)
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == REQUEST_COUNTRY){
            if (resultCode == Activity.RESULT_OK){
                val result = data!!.getSerializableExtra("CountryData") as CountryData
                if (result!=null){
                    countryCode = result!!.code
                    countryFlag!!.setImageResource(result!!.imgFlag)
                    countryNumberCode!!.setText(result!!.extention)
                }
            }
        }
    }
}
