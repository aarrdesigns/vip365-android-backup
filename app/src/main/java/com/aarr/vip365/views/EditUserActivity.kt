package com.aarr.vip365.views

import android.app.Activity
import android.app.Dialog
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.media.ExifInterface
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.telephony.TelephonyManager
import android.util.Log
import android.view.Window
import android.widget.*
import com.aarr.vip365.database.Dao.UsersDao
import com.aarr.vip365.database.Model.UsersModel

import com.aarr.vip365.R
import com.aarr.vip365.services.RegistroService
import com.aarr.vip365.util.CountryData
import com.aarr.vip365.util.CountryUtils
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.github.siyamed.shapeimageview.CircularImageView
import com.google.android.gms.auth.api.Auth
import java.io.File
import java.io.FileNotFoundException
import java.util.*

class EditUserActivity : AppCompatActivity() {

    var txtName: EditText? = null
    var txtLastName: EditText? = null
    var txtEmail: EditText? = null
    var txtPhoneNumber: EditText? = null
    var btnSave: Button? = null
    var imgProfile: CircularImageView? = null
    var currentUser: UsersModel? = null
    var btnClose: ImageView? = null
    var countryCode: String? = null
    private val PICK_IMAGE = 101
    private val TAKE_PICTURE = 1
    private var destination: File? = null
    private var destinationUri: Uri? = null
    var countryFlag: ImageView? = null
    var countryNumberCode: TextView? = null
    var countryDataContainer: LinearLayout? = null
    var REQUEST_COUNTRY = 1000
    var dialog: ProgressDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_user)

        currentUser = UsersDao().getCurrentUser()

        countryFlag = findViewById<ImageView>(R.id.countryFlag) as ImageView
        countryDataContainer = findViewById<LinearLayout>(R.id.countryDataContainer) as LinearLayout
        countryDataContainer!!.setOnClickListener {
            var intent = Intent(this,CountryListActivity::class.java)
            startActivityForResult(intent,REQUEST_COUNTRY)
        }
        countryNumberCode = findViewById<TextView>(R.id.countryNumberCode) as TextView

        txtName = findViewById<EditText>(R.id.txtName) as EditText
        txtLastName = findViewById<EditText>(R.id.txtLastName) as EditText
        txtEmail = findViewById<EditText>(R.id.txtEmail) as EditText
        txtPhoneNumber = findViewById<EditText>(R.id.txtPhoneNumber) as EditText
        btnSave = findViewById<Button>(R.id.btnSave) as Button
        btnSave!!.setOnClickListener {
            val name = txtName!!.text.toString()
            val lastName = txtLastName!!.text.toString()
            val email = txtEmail!!.text.toString()
            val phoneNumber = txtPhoneNumber!!.text.toString()
            val codeNumber = countryNumberCode!!.text.toString()

//            val password = txtPassword!!.text.toString()
            if (currentUser!=null){
                currentUser!!.name = name
                currentUser!!.lastName = lastName
                currentUser!!.email = email
                currentUser!!.phoneNumber = codeNumber+phoneNumber
                currentUser!!.countryCode = countryCode!!

                showProgressDialog()
                RegistroService(this).handleRegister(currentUser)
            }else{
                Toast.makeText(this, "There is no user information", Toast.LENGTH_SHORT)
                        .show()
            }
        }
        imgProfile = findViewById<CircularImageView>(R.id.imgProfile) as CircularImageView
        btnClose = findViewById<ImageView>(R.id.btnClose) as ImageView
        btnClose!!.setOnClickListener {
            finish()
        }

        if (currentUser!=null){
            txtName!!.setText(currentUser!!.name)
            txtLastName!!.setText(currentUser!!.lastName)
            txtEmail!!.setText(currentUser!!.email)
            countryCode = currentUser!!.countryCode
            var country = CountryUtils().getCountryByCode(countryCode!!.replace(" ",""))
            if (country!=null){
                countryFlag!!.setImageResource(country!!.imgFlag)
                countryNumberCode!!.setText(country!!.extention)
                txtPhoneNumber!!.setText(currentUser!!.phoneNumber.replace(country!!.extention,""))
            }else{
                txtPhoneNumber!!.setText(currentUser!!.phoneNumber)
                Log.v("CountryCode","Country data is null")
            }
            Log.v("CountryCode",countryCode)

            if (currentUser!!.profileImage!=null && !currentUser!!.profileImage.equals("")){
                if (currentUser!!.typeUser==1){
                    var fileAux = File(currentUser!!.profileImage)

                    if (fileAux.exists()){
                        var imageUri = Uri.parse(File(currentUser!!.profileImage).toString())

                        val exif = ExifInterface(imageUri.getPath())
                        val rotation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL)
                        Log.e("ROTATIONVALUE",rotation.toString())

                        val options = RequestOptions()
                        options.diskCacheStrategy(DiskCacheStrategy.NONE)
                        options.skipMemoryCache(true)
                        options.fitCenter()
                        options.dontTransform()
                        options.dontAnimate()
                        Glide
                                .with(this)
                                .load(currentUser!!.profileImage)
                                .apply(options)
                                .into(imgProfile)
                    }
                }else{
                    var imageUri = Uri.parse(File(currentUser!!.profileImage).toString())

                    val exif = ExifInterface(imageUri.getPath())
                    val rotation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL)
                    Log.e("ROTATIONVALUE",rotation.toString())

                    val options = RequestOptions()
                    options.diskCacheStrategy(DiskCacheStrategy.NONE)
                    options.skipMemoryCache(true)
                    options.fitCenter()
                    options.dontTransform()
                    options.dontAnimate()
                    Glide
                            .with(this)
                            .load(currentUser!!.profileImage)
                            .apply(options)
                            .into(imgProfile)
                }
            }
            when (currentUser!!.typeUser){
                1 -> {
                    imgProfile!!.setOnClickListener {
                        showEditImageDialog()
                    }
                }
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        if (destinationUri != null) {
            outState!!.putString("cameraImageUri", destinationUri.toString());
        }
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        super.onRestoreInstanceState(savedInstanceState)
        if (savedInstanceState!!.containsKey("cameraImageUri")) {
            destinationUri = Uri.parse(savedInstanceState.getString("cameraImageUri"))
        }
    }

    fun showEditImageDialog(){
        var dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.dialog_select_media)

        val btnCamera = dialog.findViewById<LinearLayout>(R.id.btnCamera) as LinearLayout
        val btnMedia = dialog.findViewById<LinearLayout>(R.id.btnMedia) as LinearLayout
        val btnClose = dialog.findViewById<ImageView>(R.id.btnClose) as ImageView
        btnClose.setOnClickListener {
            dialog.dismiss()
        }
        btnCamera.setOnClickListener {
            dialog.dismiss()
            val name = getImageName()
            destination = File(Environment.getExternalStorageDirectory(), name)
            try {
                destination!!.createNewFile()
            }catch (e:Exception){
                Log.v("RutaImagen:", "Exception: " + e.toString())
            }
            destinationUri = Uri.fromFile(destination)
            Log.v("RutaImagenDestination:", "AbsolutePath: " + destination!!.absolutePath)

            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            intent.putExtra(MediaStore.EXTRA_OUTPUT,
                    Uri.fromFile(destination))
            startActivityForResult(intent, TAKE_PICTURE)
        }
        btnMedia.setOnClickListener {
            dialog.dismiss()
            val getIntent = Intent(Intent.ACTION_GET_CONTENT)
            getIntent.type = "image/*"

            val pickIntent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
            pickIntent.type = "image/*"

            val chooserIntent = Intent.createChooser(getIntent, "Select Image")
            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, arrayOf(pickIntent))

            startActivityForResult(chooserIntent, PICK_IMAGE)
        }
        dialog.show()
    }

    fun getImageName(): String {
        val c = Calendar.getInstance()
        val DIA = c.get(Calendar.DAY_OF_MONTH).toString()
        val MES = c.get(Calendar.MONTH).toString()
        val ANO = c.get(Calendar.YEAR).toString()
        val HORA = c.get(Calendar.HOUR).toString()
        val MINUTO = c.get(Calendar.MINUTE).toString()
        val SEGUNDO = c.get(Calendar.SECOND).toString()

        val nombre = DIA + MES + ANO + "_" + HORA + MINUTO + SEGUNDO + ".png"
        Log.v("ArchivosAdjuntos", "Nombre imagen: " + nombre)

        return nombre
    }

    private fun getRealPathFromURI(contentURI: Uri): String {
        val result: String
        val cursor = contentResolver.query(contentURI, null, null, null, null)
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.path
        } else {
            cursor.moveToFirst()
            val idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
            result = cursor.getString(idx)
            cursor.close()
        }
        return result
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            TAKE_PICTURE -> if (resultCode == Activity.RESULT_OK) {
                Log.v("TomarImagen", "Resultado recibido: Tomar foto")
//                var photoPath = ""
//                var cursor = getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, arrayOf(MediaStore.Images.Media.DATA, MediaStore.Images.Media.DATE_ADDED, MediaStore.Images.ImageColumns.ORIENTATION), MediaStore.Images.Media.DATE_ADDED, null, "date_added ASC");
//                if(cursor != null && cursor.moveToFirst())
//                {
//                    do {
//                        var uri = Uri.parse(cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA)));
//                        photoPath = uri.toString()
//
//                    }while(cursor.moveToNext())
//                    cursor.close()
//                    Log.v("TomarImagen", "Path: "+photoPath)
//                    currentUser!!.profileImage = photoPath
//                    Glide
//                            .with(this)
//                            .load(photoPath)
//                            .into(imgProfile)
//                }

                var selectedImage = destinationUri
                var selectedImagePath = selectedImage.toString().replace("file://","")
                var file = File(selectedImagePath)
                if (file.exists()){
                    Log.e("Camera", "File exist")
                }
                Log.e("Camera", selectedImage.toString())
                Log.e("Camera", selectedImage.toString().replace("file://",""))
                getContentResolver().notifyChange(selectedImage, null)
                var cr = getContentResolver()
                var bitmap: Bitmap? = null
                try {
                     bitmap = android.provider.MediaStore.Images.Media
                     .getBitmap(cr, selectedImage)

                    currentUser!!.profileImage = selectedImagePath

                    var imageUri = Uri.parse(File(currentUser!!.profileImage).toString())

                    val exif = ExifInterface(imageUri.getPath())
                    val rotation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL)
                    Log.e("ROTATIONVALUE",rotation.toString())

                    val options = RequestOptions()
                    options.diskCacheStrategy(DiskCacheStrategy.NONE)
                    options.skipMemoryCache(true)
                    options.fitCenter()
                    options.dontTransform()
                    options.dontAnimate()
                    Glide
                            .with(this)
                            .load(selectedImagePath)
                            .apply(options)
                            .into(imgProfile)


//                    Toast.makeText(this, selectedImage.toString(),
//                            Toast.LENGTH_LONG).show()
                } catch (e:Exception) {
                    Toast.makeText(this, "Failed to load", Toast.LENGTH_SHORT)
                            .show()
                    Log.e("Camera", e.toString())
                }
//                var ruta = ""
//                if (destination != null && destination!!.exists()) {
//                    ruta = destination!!.getAbsolutePath()
//                    Log.v("RutaImagen:", "AbsolutePath: " + ruta)
//                }
//                Log.v("RutaImagen", ruta)

//                try{
//                    Glide
//                            .with(this)
//                            .load(ruta)
//                            .into(imgProfile)
//                }catch (e:Exception){
//                    Log.w("LoadingImagen","Glide loading image exception: "+e.toString())
//                }

            }
            PICK_IMAGE -> if (resultCode == Activity.RESULT_OK) {
                Log.v("TomarImagen", "Resultado recibido: Foto seleccionada")
                if (data != null) {
                    try {
                        val inputStream = this.contentResolver.openInputStream(data.data)
                        if (inputStream != null) {
                            val file = File(data.data.path)
                            //                        String ruta = new File(data.getData().getPath()).getAbsolutePath();
                            var ruta = ""

                            if (file.exists()) {
                                ruta = File(data.data.path).absolutePath
                                Log.v("RutaImagen:", "AbsolutePath: " + ruta)
                            } else {
                                val uri = data.data
                                val file1 = File(getRealPathFromURI(uri))
                                Log.v("RutaImagen:", "RealPathFromURI" + getRealPathFromURI(uri))
                                if (file1.exists()) {
                                    ruta = getRealPathFromURI(uri)
                                } else {
                                    Log.v("RutaImagen", "imagen no existe")
                                }
                            }
                            Log.v("RutaImagen", ruta)
                            currentUser!!.profileImage = ruta
//                            var fileAux = File(ruta)
//                            if (fileAux.exists()){
//                                var myBitmap = BitmapFactory.decodeFile(fileAux.getAbsolutePath())
//                                imgProfile!!.setImageBitmap(myBitmap)
//                            }
                            try{
                                var imageUri = Uri.parse(File(ruta).toString())

                                val exif = ExifInterface(imageUri.getPath())
                                val rotation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL)
                                Log.e("ROTATIONVALUE",rotation.toString())

                                val options = RequestOptions()
                                options.diskCacheStrategy(DiskCacheStrategy.NONE)
                                options.skipMemoryCache(true)
                                options.fitCenter()
                                options.dontTransform()
                                options.dontAnimate()
                                Glide
                                        .with(this)
                                        .load(ruta)
                                        .apply(options)
                                        .into(imgProfile)
                            }catch (e:Exception){
                                Log.w("LoadingImagen","Glide loading image exception: "+e.toString())
                            }
                        }
                    } catch (e: FileNotFoundException) {
                        e.printStackTrace()
                    }

                }
                //Now you can do whatever you want with your inpustream, save it as file, upload to a server, decode a bitmap...
            }
            REQUEST_COUNTRY -> {
                if (resultCode == Activity.RESULT_OK){
                    val result = data!!.getSerializableExtra("CountryData") as CountryData
                    if (result!=null){
                        countryCode = result!!.code
                        countryFlag!!.setImageResource(result!!.imgFlag)
                        countryNumberCode!!.setText(result!!.extention)
                    }
                }
            }
        }
    }

    fun handleSuccessResponse(user:UsersModel){
        dismissDialog()
        val insert = UsersDao().updateOrCreate(user)
        if (insert>0){
            var returnIntent = Intent()
            setResult(Activity.RESULT_OK,returnIntent)
            finish()
        }
    }

    fun showProgressDialog(){
        dialog = ProgressDialog(this)
        dialog!!.setMessage("Porfavor espere...")
        dialog!!.show()
    }

    fun dismissDialog(){
        if (dialog!=null){
            dialog!!.dismiss()
        }
    }

}
