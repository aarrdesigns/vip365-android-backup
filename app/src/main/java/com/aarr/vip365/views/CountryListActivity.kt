package com.aarr.vip365.views

import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.ImageView

import com.aarr.vip365.R
import com.aarr.vip365.adapters.CountriesAdapter
import com.aarr.vip365.adapters.PaymentAdapter
import com.aarr.vip365.database.Dao.PaymentsMethodsDao
import com.aarr.vip365.util.CountryData
import com.aarr.vip365.util.CountryUtils

class CountryListActivity : AppCompatActivity() {

    var recycler: RecyclerView? = null
    var btnClose: ImageView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_country_list)

        btnClose = findViewById<ImageView>(R.id.btnClose) as ImageView
        btnClose!!.setOnClickListener {
            finish()
        }

        recycler = findViewById<RecyclerView>(R.id.recycler) as RecyclerView
        fillAndResetRecycler()
    }

    fun fillAndResetRecycler(){
        recycler!!.adapter = null
        var adapter = CountriesAdapter(CountryUtils().getCountriesDataES()!!,this)
        val lManager : LinearLayoutManager? = LinearLayoutManager(this)
        recycler!!.layoutManager = lManager
        val dividerItemDecoration = DividerItemDecoration(recycler!!.getContext(),
                lManager!!.getOrientation())
        recycler!!.addItemDecoration(dividerItemDecoration)
        recycler!!.adapter = adapter
    }

    fun returnCountryData(country:CountryData?){
        var returnIntent = Intent()
        if (country!=null)
            returnIntent.putExtra("CountryData",country)
        setResult(Activity.RESULT_OK,returnIntent)
        finish()
    }
}
