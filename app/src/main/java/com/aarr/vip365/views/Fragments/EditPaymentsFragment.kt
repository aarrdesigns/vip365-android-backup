package com.aarr.vip365.views.Fragments


import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView

import com.aarr.vip365.R
import java.util.regex.Pattern


/**
 * A simple [Fragment] subclass.
 */
class EditPaymentsFragment : Fragment() {

    var rootView: View? = null
    var cardNumber: EditText? = null
    var fechaVenc: EditText? = null
    var cvvNumber: EditText? = null
    var cardType: ImageView? = null


    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        rootView = inflater!!.inflate(R.layout.fragment_edit_payments, container, false)

        cardNumber = rootView!!.findViewById<EditText>(R.id.cardNumber) as EditText
        cardNumber!!.addTextChangedListener(object: TextWatcher{
            override fun afterTextChanged(s: Editable?) {
                checkCard(s!!.trim().toString())
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

        })
        fechaVenc = rootView!!.findViewById<EditText>(R.id.fechaVenc) as EditText
        cvvNumber = rootView!!.findViewById<EditText>(R.id.cvvNumber) as EditText
        cardType = rootView!!.findViewById<EditText>(R.id.cardType) as ImageView
        return rootView
    }

    fun checkCard(cardNumber:String?){
        val jcbPattern = Pattern.compile("^(?:2131|1800|35)[0-9]{0,}\$")
        val amexPattern = Pattern.compile("^3[47][0-9]{0,}\$")
        val visaPattern = Pattern.compile("^4[0-9]{0,}\$")
        val mastercardPattern = Pattern.compile("^(5[1-5]|222[1-9]|22[3-9]|2[3-6]|27[01]|2720)[0-9]{0,}\$")
        val maestroPattern = Pattern.compile("^(5[06789]|6)[0-9]{0,}\$")
        val discoverPattern = Pattern.compile("^(6011|65|64[4-9]|62212[6-9]|6221[3-9]|622[2-8]|6229[01]|62292[0-5])[0-9]{0,}\$")

        val jcbFormat = "#### #### #### ####"
        val amexFormat = "#### ###### #####"
        val visaFormat = "#### #### #### ####"
        val mastercardFormat = "#### #### #### ####"
        val maestroFormat = "#### #### #### ####"
        val discoverFormat = "#### #### #### ####"

        val jcbLength = 16
        val amexLength = 15
        val visaLength = 16
        val mastercardLength = 16
        val maestroLength = 16
        val discoverLength = 16

        if (jcbPattern.matcher(cardNumber).matches() || jcbPattern.matcher(getCardNumber(cardNumber)).matches()){
            cardType!!.setImageResource(R.mipmap.jcb_64)
            if (cardNumber!!.length == jcbLength)
                this.cardNumber!!.setText(maskCardNumber(cardNumber!!,jcbFormat))
        }else if (amexPattern.matcher(cardNumber).matches() || amexPattern.matcher(getCardNumber(cardNumber)).matches()){
            cardType!!.setImageResource(R.mipmap.amex_64)
            if (cardNumber!!.length == amexLength)
                this.cardNumber!!.setText(maskCardNumber(cardNumber!!,amexFormat))
        }else if (visaPattern.matcher(cardNumber).matches() || visaPattern.matcher(getCardNumber(cardNumber)).matches()){
            cardType!!.setImageResource(R.mipmap.visa_64)
            if (cardNumber!!.length == visaLength)
                this.cardNumber!!.setText(maskCardNumber(cardNumber!!,visaFormat))
        }else if (mastercardPattern.matcher(cardNumber).matches() || mastercardPattern.matcher(getCardNumber(cardNumber)).matches()){
            cardType!!.setImageResource(R.mipmap.mastercard_64)
            if (cardNumber!!.length == mastercardLength)
                this.cardNumber!!.setText(maskCardNumber(cardNumber!!,mastercardFormat))
        }else if (maestroPattern.matcher(cardNumber).matches() || maestroPattern.matcher(getCardNumber(cardNumber)).matches()){
            if (cardNumber!![0] == '5'){
                cardType!!.setImageResource(R.mipmap.mastercard_64)
                if (cardNumber!!.length == mastercardLength)
                    this.cardNumber!!.setText(maskCardNumber(cardNumber,mastercardFormat))
            }else{
                cardType!!.setImageResource(R.mipmap.maestro_64)
                if (cardNumber!!.length == maestroLength)
                    this.cardNumber!!.setText(maskCardNumber(cardNumber,maestroFormat))
            }
        }
//        else if (discoverPattern.matcher(cardNumber).matches()){
//            cardType!!.setImageResource(R.mipmap.discover_64)
//            if (cardNumber!!.length == discoverLength)
//                this.cardNumber!!.setText(maskCardNumber(cardNumber!!,discoverFormat))
//        }
        else{
            cardType!!.setImageResource(R.mipmap.credit_card64)
        }

    }

    fun getCardNumber(card:String?):String?{
        return card!!.replace("-","")
    }

    fun maskCardNumber(cardNumber: String, mask: String): String {

        // format the number
        var index = 0
        val maskedNumber = StringBuilder()
        for (i in 0..mask.length - 1) {
            val c = mask[i]
            if (c == '#') {
                maskedNumber.append(cardNumber[index])
                index++
            } else if (c == 'x') {
                maskedNumber.append(c)
                index++
            } else {
                maskedNumber.append(c)
            }
        }

        // return the masked number
        return maskedNumber.toString()
    }

}// Required empty public constructor
