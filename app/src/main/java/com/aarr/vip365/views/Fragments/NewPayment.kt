package com.aarr.vip365.views.Fragments


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout

import com.aarr.vip365.R
import com.aarr.vip365.views.PagosActivity


/**
 * A simple [Fragment] subclass.
 */
class NewPayment : Fragment() {

    var rootView:View?=null
    var cardPayment: RelativeLayout? = null
    var paypalPayment: RelativeLayout? = null
    var parentActivity: PagosActivity? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        rootView = inflater!!.inflate(R.layout.fragment_payment_new, container, false)

        parentActivity = activity as PagosActivity

        cardPayment = rootView!!.findViewById<RelativeLayout>(R.id.profileCard) as RelativeLayout
        cardPayment!!.setOnClickListener {
            parentActivity!!.initCardCreation()
        }
        paypalPayment = rootView!!.findViewById<RelativeLayout>(R.id.paypalPayment) as RelativeLayout
        paypalPayment!!.setOnClickListener {
            parentActivity!!.onFuturePaymentPressed()
        }

        return rootView
    }

}// Required empty public constructor
