package com.aarr.vip365.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import com.aarr.vip365.R
import com.aarr.vip365.config.Configuration
import com.aarr.vip365.database.Model.PaymentMethods
import com.aarr.vip365.util.CountryData
import com.aarr.vip365.views.CountryListActivity
import com.aarr.vip365.views.PagosActivity
import java.util.ArrayList

/**
 * Created by andresrodriguez on 9/27/17.
 */

class CountriesAdapter(items: MutableList<CountryData>, adapterContext: Context) : RecyclerView.Adapter<CountriesAdapter.CountriesViewHolder>() {
    var countries: List<CountryData>? = null
        get() {
            if (field == null) {
                this.countries = ArrayList<CountryData>()
            }
            return field
        }
    private val activity: CountryListActivity

    class CountriesViewHolder(v: View) : RecyclerView.ViewHolder(v) {

        var countryFlag: ImageView? = null
        var countryName: TextView? = null
        var countryContainer: RelativeLayout? = null

        init {
            countryFlag = v.findViewById<ImageView>(R.id.countryFlag) as ImageView
            countryName = v.findViewById<TextView>(R.id.countryName) as TextView
            countryContainer = v.findViewById<RelativeLayout>(R.id.countryContainer) as RelativeLayout

        }
    }

    init {
        Log.v("BlacklistSize", items.size.toString())
        countries = items
        activity = adapterContext as CountryListActivity
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CountriesAdapter.CountriesViewHolder {
        val v = LayoutInflater.from(parent.context)
                .inflate(R.layout.country_item, parent, false)
        return CountriesViewHolder(v)
    }

    override fun onBindViewHolder(holder: CountriesAdapter.CountriesViewHolder, position: Int) {
        if (countries != null) {
            if (countries!!.isNotEmpty()) {
                Log.v("countriesMethods", countries!![position].toString())

                holder.countryName!!.setText(countries!![position].name)
                holder.countryFlag!!.setImageResource(countries!![position].imgFlag)
                holder.countryContainer!!.setOnClickListener {
                    activity!!.returnCountryData(countries!![position])
                }

            }
        }

    }

    override fun getItemCount(): Int {
        return countries!!.size
    }
}