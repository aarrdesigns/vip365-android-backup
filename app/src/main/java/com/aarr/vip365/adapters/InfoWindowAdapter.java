package com.aarr.vip365.adapters;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aarr.vip365.R;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

/**
 * Created by andresrodriguez on 10/6/17.
 */

public class InfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

    private static final String TAG = "CustomInfoWindowAdapter";
    private LayoutInflater inflater;


    public InfoWindowAdapter(LayoutInflater inflater){
        this.inflater = inflater;
    }

    @Override
    public View getInfoContents(final Marker m) {
        //Carga layout personalizado.
        View v = inflater.inflate(R.layout.item_marker_info, null);
        String[] info = m.getTitle().split("&");
        Log.v("TitleValue",m.getTitle());
        String nombre = null;
        String duracion = null;
        if (info == null || info.length<=1){
            nombre = m.getTitle();
        }else{
            nombre = info[0];
            duracion = info[1];
        }
        Log.v("Nombre",nombre);
        String url = m.getSnippet();
        ((TextView)v.findViewById(R.id.nombreUbicacion)).setText(nombre);
        if (duracion == null){
            ((LinearLayout)v.findViewById(R.id.duracionContainer)).setVisibility(View.GONE);
        }else{
            ((TextView)v.findViewById(R.id.duracionViaje)).setText(duracion+"");
        }
        return v;
    }

    @Override
    public View getInfoWindow(Marker m) {
        return null;
    }

}