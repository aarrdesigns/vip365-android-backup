package com.aarr.vip365.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import com.aarr.vip365.config.Configuration

import com.aarr.vip365.database.Model.PaymentMethods
import com.aarr.vip365.R
import com.aarr.vip365.views.BuscarRutaActivity
import com.aarr.vip365.views.PagosActivity

import java.util.ArrayList

/**
 * Created by andresrodriguez on 9/27/17.
 */

class PaymentAdapter(items: List<PaymentMethods>, adapterContext: Context) : RecyclerView.Adapter<PaymentAdapter.PaymentViewHolder>() {
    var payments: List<PaymentMethods>? = null
        get() {
            if (field == null) {
                this.payments = ArrayList<PaymentMethods>()
            }
            return field
        }
    private var activity: PagosActivity? = null
    private var activity2: BuscarRutaActivity? = null

    class PaymentViewHolder(v: View) : RecyclerView.ViewHolder(v) {

        var tipoTarjeta: ImageView
        var numeroTarjeta: TextView
        var paymentContainer: RelativeLayout

        init {
            tipoTarjeta = v.findViewById<ImageView>(R.id.tipoTarjeta) as ImageView
            numeroTarjeta = v.findViewById<TextView>(R.id.numeroTarjeta) as TextView
            paymentContainer = v.findViewById<RelativeLayout>(R.id.paymentContainer) as RelativeLayout

        }
    }

    init {
        Log.v("BlacklistSize", items.size.toString())
        payments = items
        if (adapterContext is PagosActivity)
            activity = adapterContext as PagosActivity
        if (adapterContext is BuscarRutaActivity)
            activity2 = adapterContext as BuscarRutaActivity
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PaymentAdapter.PaymentViewHolder {
        val v = LayoutInflater.from(parent.context)
                .inflate(R.layout.payment_item, parent, false)
        return PaymentViewHolder(v)
    }

    override fun onBindViewHolder(holder: PaymentAdapter.PaymentViewHolder, position: Int) {
        if (payments != null) {
            if (payments!!.isNotEmpty()) {
                Log.v("PaymentsMethods",payments!![position].toString())
                //                holder.idField.setText("ID: " + String.valueOf(getBlacklist().get(position).getId()));
                when(payments!![position].paymentType){
                    Configuration().PAYMENT_CREDIT_DEBIT_CARD ->{
                        holder.numeroTarjeta.setText(getMaskedNumber(payments!![position].cardNumber!!))
                        when (payments!![position].cardType) {
                            "Visa" -> holder.tipoTarjeta.setImageResource(R.mipmap.visa_64)
                            "American Express" -> holder.tipoTarjeta.setImageResource(R.mipmap.amex_64)
                            "Master" -> holder.tipoTarjeta.setImageResource(R.mipmap.mastercard_64)
                            "Discover" -> holder.tipoTarjeta.setImageResource(R.mipmap.discover_64)
                            "JCB" -> holder.tipoTarjeta.setImageResource(R.mipmap.jcb_64)
                            "Maestro" -> holder.tipoTarjeta.setImageResource(R.mipmap.maestro_64)
                        }
                    }
                    Configuration().PAYMENT_PAYPAL ->{
                        holder.numeroTarjeta.setText("PayPal")
                        holder.tipoTarjeta.setImageResource(R.mipmap.paypal_64)
                    }
                }


                holder.paymentContainer.setOnClickListener {
                    if (activity!=null){
                        if (payments!![position].paymentType != Configuration().PAYMENT_CASH){

                        }
                    }else if (activity2!=null){
                        activity2!!.selectPaymentMethod(payments!![position])
                    }
                }


            }
        }

    }

    fun getMaskedNumber(cardNumber:String):String{
        var lastFour = cardNumber.substring(cardNumber.length-4,cardNumber.length)
        val cardValue = "**** "+lastFour
        return cardValue
    }

    override fun getItemCount(): Int {
        return payments!!.size
    }
}
