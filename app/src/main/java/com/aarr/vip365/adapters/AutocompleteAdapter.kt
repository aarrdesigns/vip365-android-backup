package com.aarr.vip365.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.aarr.vip365.R
import com.aarr.vip365.services.PlaceAutocomplete.Prediction
import com.aarr.vip365.views.BuscarRutaActivity
import java.util.ArrayList

/**
 * Created by andresrodriguez on 9/27/17.
 */

class AutocompleteAdapter(items: List<Prediction>, adapterContext: Context, tipo: Int) : RecyclerView.Adapter<AutocompleteAdapter.AutocompleteViewHolder>() {
    var predictions: List<Prediction>? = null
        get() {
            if (field == null) {
                this.predictions = ArrayList<Prediction>()
            }
            return field
        }
    private val activity: BuscarRutaActivity
    var tipoUbicacion = -1
    val ORIGEN = 1
    val DESTINO = 2
    val DIRECCIONES = 3

    class AutocompleteViewHolder(v: View) : RecyclerView.ViewHolder(v) {

        var mainText: TextView? = null
        var secondText: TextView? = null
        var container: LinearLayout? = null

        init {
            mainText = v.findViewById<TextView>(R.id.mainText) as TextView
            secondText = v.findViewById<TextView>(R.id.secondText) as TextView
            container = v.findViewById<LinearLayout>(R.id.container) as LinearLayout

        }
    }

    init {
        Log.v("PredictionsSize", items.size.toString())
        predictions = items
        tipoUbicacion = tipo
        activity = adapterContext as BuscarRutaActivity
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AutocompleteAdapter.AutocompleteViewHolder {
        val v = LayoutInflater.from(parent.context)
                .inflate(R.layout.autocomplete_item, parent, false)
        return AutocompleteViewHolder(v)
    }

    override fun onBindViewHolder(holder: AutocompleteAdapter.AutocompleteViewHolder, position: Int) {
        if (predictions != null) {
            if (predictions!!.isNotEmpty()) {
                Log.v("AutoComplete",predictions!!.get(position).structuredFormatting.mainText)
                holder.mainText!!.setText(predictions!!.get(position).structuredFormatting.mainText.trim())
                try{
                    holder.secondText!!.setText(predictions!!.get(position)!!.structuredFormatting!!.secondaryText!!.trim())
                }catch (e:Exception){
                    holder.secondText!!.setText("")
                    Log.e("TextException",e.toString())
                }

                holder.container!!.setOnClickListener{
                    Log.e("ClickAdapter",predictions!!.get(position).id)
                    if (predictions!!.get(position).id.equals("-100")){
                        activity!!.selectLocationInMap(tipoUbicacion)
                    }else{
                        activity!!.getLocationData(tipoUbicacion,predictions!!.get(position))
                    }
                }
            }
        }

    }

    override fun getItemCount(): Int {
        return predictions!!.size
    }
}